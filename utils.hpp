#pragma once
#include <iostream>
#include <boost/spirit/include/qi_parse.hpp>
#include <boost/spirit/include/support_istream_iterator.hpp>
#include <boost/spirit/home/qi/char/char_class.hpp>

template <typename T>
class has_size {
private:
    using Yes = char;
    using No = Yes[2];

    template<typename C>
    static auto Test(void*) -> decltype(size_t{std::declval<C const>().size()}, Yes{});

    template<typename> static No& Test(...);
public:
    static bool const value = sizeof(Test<T>(0)) == sizeof(Yes);
};

template<typename T, typename std::enable_if<!has_size<T>::value>::type* = nullptr>
void print(T value) {
    std::cout << value << std::endl;
}

template<typename T, typename std::enable_if<has_size<T>::value>::type* = nullptr>
void print(T seq) {
    for(auto &value : seq) {
        std::cout << value << std::endl;
    }
}

template <typename P, typename T>
void test_phrase_parser_attr(
    std::istream &input, P const& parser, T& attr, bool full_match=true)
{
    using boost::spirit::qi::phrase_parse;
    using boost::spirit::qi::standard::space;
    using boost::spirit::istream_iterator;

    istream_iterator begin{input};
    istream_iterator end;

    if (phrase_parse(begin, end, parser, space, attr) && (!full_match || (begin == end)))
        std::cout << "ok" << std::endl;
    else
        std::cout << "fail" << std::endl;
}


template <typename P, typename T>
bool test_phrase_parser_attr(
    std::string const& input, P const& parser, T& attr, bool full_match=true)
{
    using boost::spirit::qi::phrase_parse;
    using boost::spirit::qi::standard::space;

    auto begin = std::begin(input);
    auto end = std::end(input);

    if (phrase_parse(begin, end, parser, space, attr) && (!full_match || (begin == end))) {
        return true;
    }
    return false;
}
