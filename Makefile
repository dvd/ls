BUILD_PATH ?= ./build
CXX ?= g++-4.9
CXXFLAGS += -std=c++1y -fdiagnostics-color=auto -Wall -Wextra
CXXFLAGS += -I .

# recursive wildcard -- http://blog.jgc.org/2011/07/gnu-make-recursive-wildcard-function.html
rwildcard=$(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2) $(filter $(subst *,%,$2),$d))

LS_SRCS = $(call rwildcard,ls/,*.cpp)
TESTS_SRCS := $(wildcard test/*.cpp)
BIN_SRCS := bin/tmp_script2.cpp

LS_OBJECTS = $(LS_SRCS:%.cpp=$(BUILD_PATH)/%.o)
TESTS_BINS := $(addprefix test_, $(subst .cpp,,$(notdir $(TESTS_SRCS))))
BIN_BINS := $(addprefix build/, $(subst .cpp,,$(notdir $(BIN_SRCS))))

SRCS := $(sort $(LS_SRCS) $(TESTS_SRCS))
OBJECTS = $(SRCS:%.cpp=$(BUILD_PATH)/%.o)
DEPS = $(OBJECTS:%.o=%.d)

.PHONY: test
.SECONDARY: $(addprefix $(BUILD_PATH)/, $(TESTS_BINS))

bin: $(BIN_BINS);

test: $(TESTS_BINS);

test_%: $(BUILD_PATH)/test_%
	$(BUILD_PATH)/$@

$(BUILD_PATH)/test_%: $(BUILD_PATH)/test/%.o $(LS_OBJECTS)
	$(CXX) -o $@ $^ $(LDFLAGS)

$(BUILD_PATH)/%: $(BUILD_PATH)/bin/%.o $(LS_OBJECTS)
	$(CXX) -o $@ $^ $(LDFLAGS)

-include $(DEPS)

$(BUILD_PATH)/%.o: %.cpp
	@echo "Compiling: $< -> $@"
	@mkdir -p $(dir $@)
	$(CXX) $(CXXFLAGS) -MP -MMD -c $< -o $@
