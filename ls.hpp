#pragma once
#include <string>
#include <vector>
#include <ostream>

namespace ls {
    using std::string;
    using std::vector;

    struct Selector {
        Selector() = default;
        Selector(string s) : value{std::move(s)} {}
        string value;
    };

    std::ostream& operator<<(std::ostream&, Selector const&);

    struct Ruleset;

    struct Block {
        Block() = default;
        Block(vector<string> d, vector<Ruleset> r)
            : declarations{std::move(d)},
              rulesets{std::move(r)} {}
        vector<string> declarations;
        vector<Ruleset> rulesets;
    };

    std::ostream& operator<<(std::ostream&, Block const&);

    struct Ruleset {
        Ruleset() = default;
        Ruleset(vector<Selector> sel, Block b)
            : selectors{std::move(sel)},
              block{std::move(b)} {}

        vector<Selector> selectors;
        Block block;
    };

    std::ostream& operator<<(std::ostream&, Ruleset const&);
};

