#pragma once
#define BOOST_RESULT_OF_USE_DECLTYPE
#define BOOST_SPIRIT_USE_PHOENIX_V3

#include "ls.hpp"
#include <string>
#include <tuple>
#include <vector>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/adapted/std_tuple.hpp>

BOOST_FUSION_ADAPT_STRUCT(
    ls::Ruleset,
    (std::vector<ls::Selector>, selectors)
    (ls::Block, block)
)

namespace ls { namespace parser { namespace grammars {
    namespace qi = boost::spirit::qi;
    namespace phx = boost::phoenix;
    using std::string;
    using std::vector;
    using std::tuple;

    template <typename Iterator, typename Skipper=qi::space_type>
    struct G1 : qi::grammar<Iterator, void(), Skipper> {

        template <typename T>
        using rule = qi::rule<Iterator, T, Skipper>;

        rule<void()> start;
        rule<ls::Ruleset()> ruleset;

        rule<ls::Selector()> selector;
        rule<vector<ls::Selector>()> selectors;

        rule<ls::Block()> block;
        rule<string()> declaration;

        G1() : G1::base_type(start, "G1") {
            selector = qi::as_string[+(qi::char_ - qi::char_("{,"))];
            selectors = selector % ',';

            declaration = qi::as_string[+(qi::char_ - (qi::eol | ';' | '{' | '}')) >> (qi::lit(';') | qi::eol)];

            {
                using attr_signature = vector<boost::variant<string, ls::Ruleset>>;
                auto handler = [](attr_signature const& elements) -> ls::Block {
                    vector<string> declarations;
                    vector<ls::Ruleset> rulesets;
                    for(auto& e : elements) {
                        if (auto *s = boost::get<string>(&e)) {
                            declarations.push_back(*s);
                        }
                        else if(auto *s = boost::get<ls::Ruleset>(&e)) {
                            rulesets.push_back(*s);
                        }
                    }
                    return {declarations, rulesets};
                };
                block = (qi::lit('{')
                    >> *(declaration|ruleset)
                    >> '}')[qi::_val = phx::bind(handler, qi::_1)];
            }
            ruleset = selectors >> block;

            ruleset.name("ruleset");
            block.name("block");
            declaration.name("declaration");
            selectors.name("selectors");
            selector.name("selector");
            /*
            qi::debug(ruleset);
            qi::debug(block);
            qi::debug(declaration);
            qi::debug(selectors);
            qi::debug(selector);
            */
            /*
            start = ruleset;
            */
        }
    };
}}};

