#include "ls.hpp"

namespace ls {

std::ostream& operator<<(std::ostream& out, Selector const& sel) {
    out << sel.value;
    return out;
}

std::ostream& operator<<(std::ostream& out, Block const& block) {
    out << "{" << std::endl;
    for(auto const& v : block.declarations) {
        out << v << std::endl;
    }
    for(auto const& v : block.rulesets) {
        out << v << std::endl;
    }
    out << "}" << std::endl;
    return out;
}

std::ostream& operator<<(std::ostream& out, Ruleset const& rule) {
    for (size_t ix=0; ix<rule.selectors.size()-1; ix++) {
        out << rule.selectors[ix] << ",";
    }
    out << rule.selectors.back() << " " << rule.block;
    return out;
}

};
