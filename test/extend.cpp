#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <cassert>
#include <string>
#include <ls/sass/sass.hpp>
#include <ls/sass/directives/extend_impl.hpp>
#include <ls/goodies/infix_ostream_iterator.hpp>

namespace {
    using std::string;
    using namespace ls::sass::css3;

    string extend_selector(
        string const& raw_base, string const& raw_selector, string const& raw_match_selector) {

        auto G1 = split(Selector::fromString(raw_base), Combinator::whitespace);
        auto G2 = split(Selector::fromString(raw_selector), Combinator::whitespace);
        auto EX = SimpleSelector::fromString(raw_match_selector);

        ls::sass::directives::details::ExtendImplementation impl;
        auto x = impl.find_selector(G1, EX);
        assert(x);

        // limit the domain of the test
        assert(G1.size() == 1);
        assert(G2.size() == 1);

        auto result = impl.extend_selector(
            x->selector, x->match,
            G2.back(), G2.back().selector(-1),
            EX);

        std::ostringstream output;
        Renderer r{&result, true};
        output << r;
        return output.str();
    }

    string extend(
        string const& raw_base, string const& raw_selector, string const& raw_match_selector) {
        ls::sass::directives::details::ExtendImplementation impl;

        auto result = impl.extend(
            Selector::fromString(raw_base),
            Selector::fromString(raw_selector),
            SimpleSelector::fromString(raw_match_selector));

        std::ostringstream output;
        auto it = ls::infix_ostream_iterator<Renderer>(output, ",");
        for (auto& x : result) {
            Renderer r{&x, true};
            *it = r;
        }
        return output.str();
    }
}
TEST_CASE("@extend private implementation", "[private][extend]") {
    using namespace ls::sass::css3;
    using ls::sass::directives::details::ExtendImplementation;

    SECTION("match_group function") {
        SECTION("given '{a b.foo}+{c.bar.baz} {d.foo[name]}' as the reference selector") {
            ExtendImplementation impl;
            auto s = Selector::fromString("a b.foo + c.bar.baz d.foo[name]");
            auto groups = split(s, Combinator::whitespace);

            SECTION("searching for {a} must find a match in the first group") {
                auto match = impl.find_selector(groups, SimpleSelector::fromString("a"));
                REQUIRE(!!match == true);

                auto& group = match->selector;
                REQUIRE(&group == &groups[0]);

                auto& sel = match->match;
                REQUIRE(&sel == &(groups[0].selector(0)));
            }

            SECTION("searching for {c.bar} must find a match in the second group") {
                auto match = impl.find_selector(groups, SimpleSelector::fromString("c.bar"));
                REQUIRE(!!match == true);

                auto& group = match->selector;
                REQUIRE(&group == &groups[1]);

                auto& sel = match->match;
                REQUIRE(&sel == &(groups[1].selector(1)));
            }

            SECTION("searching for {d[name]} must find a match in the third group") {
                auto match = impl.find_selector(groups, SimpleSelector::fromString("d[name]"));
                REQUIRE(!!match == true);

                auto& group = match->selector;
                REQUIRE(&group == &groups[2]);

                auto& sel = match->match;
                REQUIRE(&sel == &(groups[2].selector(0)));
            }

            SECTION("searching for {z} must not find a match") {
                auto match = impl.find_selector(groups, SimpleSelector::fromString("z"));
                REQUIRE(!!match == false);
            }
        }
    }
    SECTION("extend_selector function") {
        SECTION("base={.foo} derived={a} match={.foo} -> {a}") {
            auto result = extend_selector(".foo", "a", ".foo");
            REQUIRE(result == "{a}");
        }
        SECTION("base={.foo.bar} derived={a} match={.foo} -> {a.bar}") {
            auto result = extend_selector(".foo.bar", "a", ".foo");
            REQUIRE(result == "{a.bar}");
        }
        SECTION("base={p}+{.foo} derived={a} match={.foo} -> {p}+{a}") {
            auto result = extend_selector("p + .foo", "a", ".foo");
            REQUIRE(result == "{p}+{a}");
        }
        SECTION("base={p}+{.foo[title]} derived=b>{a} match={.foo} -> {p} + {b} > {a[title]}") {
            auto result = extend_selector("p + .foo[title]", "b > a", ".foo");
            REQUIRE(result == "{p}+{b}>{a[title]}");
        }
        SECTION("base={p}+{.foo[title]} derived=b>{a} match={.foo[title]} -> {p} + {b} > {a}") {
            auto result = extend_selector("p + .foo[title]", "b > a", ".foo[title]");
            REQUIRE(result == "{p}+{b}>{a}");
        }
    }
    SECTION("extend function") {
        SECTION("base={.foo} derived={a} match={.foo} -> {a}") {
            auto result = extend(".foo", "a", ".foo");
            REQUIRE(result == "{*.foo},{a}");
        }
        SECTION("base={.foo.bar} derived={a} match={.foo} -> {a.bar}") {
            auto result = extend_selector(".foo.bar", "a", ".foo");
            REQUIRE(result == "{a.bar}");
        }
        SECTION("base={p}+{.foo} derived={a} match={.foo} -> {p}+{a}") {
            auto result = extend_selector("p + .foo", "a", ".foo");
            REQUIRE(result == "{p}+{a}");
        }
        SECTION("base={p}+{.foo[title]} derived=b>{a} match={.foo} -> {p} + {b} > {a[title]}") {
            auto result = extend_selector("p + .foo[title]", "b > a", ".foo");
            REQUIRE(result == "{p}+{b}>{a[title]}");
        }
        SECTION("base={p}+{.foo[title]} derived=b>{a} match={.foo[title]} -> {p} + {b} > {a}") {
            auto result = extend_selector("p + .foo[title]", "b > a", ".foo[title]");
            REQUIRE(result == "{p}+{b}>{a}");
        }
    }
}
