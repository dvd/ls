#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <sstream>
#include <string>
#include <ls/sass/sass.hpp>

using std::string;
using std::vector;
namespace css3 = ls::sass::css3;

string render(css3::Selector const& s) {
    css3::Renderer r{&s, true};
    std::ostringstream output;
    output << r;
    return output.str();
}
string render(css3::AuxSelector const& s) {
    std::ostringstream output;
    output << s;
    return output.str();
}
string render(vector<css3::AuxSelector> const& s) {
    std::ostringstream output;
    for (auto x : s) {
        output << x;
    }
    return output.str();
}
string parse(string const& input) {
    auto result = css3::Selector::fromString(input);
    return render(result);
}

int fail_parse(string const& input) {
    try {
        css3::Selector::fromString(input);
    }
    catch(ls::parsing_error& e) {
        return e.error_position;
    }
    return -1;
}

TEST_CASE("Single Type Element", "[parsing][selector]") {
    REQUIRE(parse("a") == "{a}");

    SECTION("+ id") {
        REQUIRE(parse("a#foo") == "{a#foo}");
    }

    SECTION("+ class") {
        REQUIRE(parse("a.foo") == "{a.foo}");
    }

    SECTION("+ multiple classes") {
        REQUIRE(parse("a.foo.bar") == "{a.foo.bar}");
    }

    SECTION("+ attribute") {
        REQUIRE(parse("a[href]") == "{a[href]}");
        REQUIRE(parse("a[name=home]") == "{a[name=home]}");
        SECTION("quoted string are normalized") {
            REQUIRE(parse(R"(a[href="http://"])") == R"({a[href="http://"]})");
            REQUIRE(parse(R"(a[href='http://'])") == R"({a[href="http://"]})");
            REQUIRE(
                parse(R"(a[name='"quoted" and \'quoted\''])")
                == R"({a[name="\"quoted\" and 'quoted'"]})");
        }
    }
    SECTION("+ multiple attributes") {
        REQUIRE(parse("a[href][title=hello]") == "{a[href][title=hello]}");
    }

    SECTION("+ pseudo-class") {
        REQUIRE(parse("a:first-child") == "{a:first-child}");
        REQUIRE(parse("a:nth-child(even)") == "{a:nth-child(even)}");
        REQUIRE(parse("a:nth-child(2n+1)") == "{a:nth-child(odd)}");
        REQUIRE(parse("a:nth-child(+2)") == "{a:nth-child(+2)}");
    }
    SECTION("+ multiple pseudo-classes") {
        REQUIRE(parse("a:hover:first-child") == "{a:hover:first-child}");
    }

    SECTION("all together") {
        REQUIRE(parse("a.foo#foo") == "{a.foo#foo}");
        REQUIRE(parse("a[name]:first-child") == "{a[name]:first-child}");
        REQUIRE(parse("a.foo#foo[name]:first-child") == "{a.foo#foo[name]:first-child}");
        REQUIRE(parse("a.foo:first-child[name].bar:hover") == "{a.foo:first-child[name].bar:hover}");
    }
}

TEST_CASE("Explicit Universal Element", "[parsing][selector]") {
    REQUIRE(parse("*") == "{*}");

    SECTION("+ class") {
        REQUIRE(parse("*.foo") == "{*.foo}");
    }

    SECTION("+ id") {
        REQUIRE(parse("*#foo") == "{*#foo}");
    }

    SECTION("+ attribute") {
        REQUIRE(parse("*[href]") == "{*[href]}");
    }

    SECTION("+ pseudo-class") {
        REQUIRE(parse("*:first-child") == "{*:first-child}");
    }
}

TEST_CASE("Implicit Universal Element", "[parsing][selector]") {
    SECTION("+ class") {
        REQUIRE(parse(".foo") == "{*.foo}");
    }

    SECTION("+ id") {
        REQUIRE(parse("#foo") == "{*#foo}");
    }

    SECTION("+ attribute") {
        REQUIRE(parse("[href]") == "{*[href]}");
    }

    SECTION("+ pseudo-class") {
        REQUIRE(parse(":first-child") == "{*:first-child}");
    }
}

TEST_CASE("Multiple Simple Selectors", "[parsing][selector]") {
    SECTION("whitespace combinator") {
        REQUIRE(parse("div a") == "{div} {a}");
    }
    SECTION("plus combinator") {
        REQUIRE(parse("div + a") == "{div}+{a}");
    }
    SECTION("tilde combinator") {
        REQUIRE(parse("div ~ a") == "{div}~{a}");
    }
    SECTION("greaterThan combinator") {
        REQUIRE(parse("div > a") == "{div}>{a}");
    }
    SECTION("multiple combinators") {
        REQUIRE(parse("div > a span ~ i") == "{div}>{a} {span}~{i}");
    }
    SECTION("superflous whitespaces") {
        REQUIRE(parse("div   >    a    span    ~   i") == "{div}>{a} {span}~{i}");
    }
    SECTION("no superflous whitespaces") {
        REQUIRE(parse("div>a span~i") == "{div}>{a} {span}~{i}");
    }
}

SCENARIO("Invalid input strings must be reported correctly", "[parse][selector]") {
    GIVEN("An input string with an incomplete class") {
        string input = "a.";
        THEN("The parser should report the error position") {
            REQUIRE(fail_parse(input) == 1);
        }
    }
    GIVEN("An input string with an incomplete ID") {
        string input = "a#";
        THEN("The parser should report the error position") {
            REQUIRE(fail_parse(input) == 1);
        }
    }
    GIVEN("An input string with an incomplete attribute selector") {
        string input = "a[x";
        THEN("The parser should report the error position") {
            REQUIRE(fail_parse(input) == 1);
        }
    }
    GIVEN("An input string with an empty attribute selector") {
        string input = "a[]";
        THEN("The parser should report the error position") {
            REQUIRE(fail_parse(input) == 1);
        }
    }
    GIVEN("An input string with an unexpected ]") {
        string input = "a]";
        THEN("The parser should report the error position") {
            REQUIRE(fail_parse(input) == 1);
        }
    }
    GIVEN("An input string with an incomplete pseudo class selector") {
        string input = "a:";
        THEN("The parser should report the error position") {
            REQUIRE(fail_parse(input) == 1);
        }
    }
    GIVEN("An input string with an unclosed function in a pseudo class selector") {
        string input = "a:f(q";
        THEN("The parser should report the error position") {
            REQUIRE(fail_parse(input) == 3);
        }
    }
}

SCENARIO("css3 basic selectors can be compared using &&", "[match][selector]") {
    GIVEN("Two universal selectors") {
        css3::Universal ref{}, sel{};

        WHEN("compared") {
            THEN("the two selectors are compatible") {
                REQUIRE((ref && sel) == true);
            }
        }
    }
    GIVEN("One universal selector `a` and one type selector `b`") {
        css3::Universal a{};
        css3::Type b{"div"};

        WHEN("`a` is the reference and `b` is the selector") {
            THEN("the match is not compatible") {
                REQUIRE((a && b) == false);
            }
        }
        WHEN("`b` is the reference and `a` is the selector") {
            THEN("the match is compatible") {
                REQUIRE((b && a) == true);
            }
        }
    }
    GIVEN("Two type selectors") {
        WHEN("`a` and `b` refer to different tags") {
            css3::Type a{"div"};
            css3::Type b{"span"};
            THEN("the match is not compatible") {
                REQUIRE((a && b) == false);
            }
        }
        WHEN("both refer to the same tag") {
            css3::Type a{"div"};
            css3::Type b{"div"};
            THEN("the match is compatible") {
                REQUIRE((a && b) == true);
            }
        }
    }
}

SCENARIO("css3 simple selectors can be compared using &&", "[match][selector]") {
    using namespace ls::sass::css3;
    GIVEN("div.foo and div.foo") {
        auto ref = SimpleSelector::fromString("div.foo");
        auto sel = SimpleSelector::fromString("div.foo");

        WHEN("compared") {
            THEN("the two selectors are compatible") {
                REQUIRE((ref && sel) == true);
            }
        }
    }
    GIVEN("div.foo.bar and div.foo") {
        auto ref = SimpleSelector::fromString("div.foo.bar");
        auto sel = SimpleSelector::fromString("div.foo");

        WHEN("compared") {
            THEN("the two selectors are compatible") {
                REQUIRE((ref && sel) == true);
            }
        }
    }
    GIVEN("div.foo and span.foo") {
        auto ref = SimpleSelector::fromString("div.foo");
        auto sel = SimpleSelector::fromString("span.foo");

        WHEN("compared") {
            THEN("the two selectors are not compatible") {
                REQUIRE((ref && sel) == false);
            }
        }
    }
    GIVEN("div.foo and *.foo") {
        auto ref = SimpleSelector::fromString("div.foo");
        auto sel = SimpleSelector::fromString(".foo");

        WHEN("compared") {
            THEN("the two selectors are compatible") {
                REQUIRE((ref && sel) == true);
            }
        }
    }
    GIVEN("div.foo:hover and div.foo") {
        auto ref = SimpleSelector::fromString("div.foo:hover");
        auto sel = SimpleSelector::fromString("div.foo");

        WHEN("compared") {
            THEN("the two selectors are compatible") {
                REQUIRE((ref && sel) == true);
            }
        }
    }
    GIVEN("div.foo:hover and div:hover") {
        auto ref = SimpleSelector::fromString("div.foo:hover");
        auto sel = SimpleSelector::fromString("div:hover");

        WHEN("compared") {
            THEN("the two selectors are compatible") {
                REQUIRE((ref && sel) == true);
            }
        }
    }
    GIVEN("*.foo and div.foo") {
        auto ref = SimpleSelector::fromString("*.foo");
        auto sel = SimpleSelector::fromString("div.foo");

        WHEN("compared") {
            THEN("the two selectors are not compatible") {
                REQUIRE((ref && sel) == false);
            }
        }
    }
    GIVEN("div.foo.bar and div.foo") {
        auto ref = SimpleSelector::fromString("div.foo.bar");
        auto sel = SimpleSelector::fromString("div.foo");

        WHEN("compared") {
            THEN("the two selectors are compatible") {
                REQUIRE((ref && sel) == true);
            }
        }
    }
    GIVEN("div.foo[name] and div[name]") {
        auto ref = SimpleSelector::fromString("div.foo[name]");
        auto sel = SimpleSelector::fromString("div[name]");

        WHEN("compared") {
            THEN("the two selectors are compatible") {
                REQUIRE((ref && sel) == true);
            }
        }
    }
    GIVEN("div[name=x] and div[name]") {
        auto ref = SimpleSelector::fromString("div[name=x]");
        auto sel = SimpleSelector::fromString("div[name]");

        WHEN("compared") {
            THEN("the two selectors are not compatible") {
                REQUIRE((ref && sel) == false);
            }
        }
    }
    GIVEN("div[name][title=\"a\"] and div[title=\"a\"]") {
        auto ref = SimpleSelector::fromString("div[name][title=\"a\"]");
        auto sel = SimpleSelector::fromString("div[title=\"a\"]");

        WHEN("compared") {
            THEN("the two selectors are compatible") {
                REQUIRE((ref && sel) == true);
            }
        }
    }
    GIVEN("div:hover:first-child and div:first-child") {
        auto ref = SimpleSelector::fromString("div:hover:first-child");
        auto sel = SimpleSelector::fromString("div:first-child");

        WHEN("compared") {
            THEN("the two selectors are compatible") {
                REQUIRE((ref && sel) == true);
            }
        }
    }
}

SCENARIO("css3 selectors can be searched for a matching simple selectors using the find_match function", "[match][selector]") {
    using namespace ls::sass::css3;
    GIVEN("{div.foo} and {.foo}") {
        auto ref = Selector::fromString("div.foo");
        auto sel = SimpleSelector::fromString(".foo");
        WHEN("compared") {
            THEN("{div.foo} is the found match") {
                auto result = find_match(ref, sel);
                REQUIRE(!!result == true);
                REQUIRE(&(*result) == &ref.selector(0));
            }
        }
    }
    GIVEN("{div.foo} and {.bar}") {
        auto ref = Selector::fromString("div.foo");
        auto sel = SimpleSelector::fromString(".bar");
        WHEN("compared") {
            THEN("no match will be found") {
                auto result = find_match(ref, sel);
                REQUIRE(!!result == false);
            }
        }
    }
    GIVEN("{div.foo p i[title]} and {i[title]}") {
        auto ref = Selector::fromString("div.foo p i[title]");
        auto sel = SimpleSelector::fromString("i[title]");
        WHEN("compared") {
            THEN("{i[title]} is the found match") {
                auto result = find_match(ref, sel);
                REQUIRE(!!result == true);
                REQUIRE(&(*result) == &ref.selector(2));
            }
        }
    }
}

SCENARIO("SimpleSelectors can be compared", "[compare][selector]") {
    using namespace ls::sass::css3;
    GIVEN("Two simple selectors with the same type and attributes") {
        WHEN("the attributes are in the same order") {
            auto a = SimpleSelector::fromString("div.foo[title]");
            auto b = SimpleSelector::fromString("div.foo[title]");

            THEN("the two selectors are equals") {
                REQUIRE(a == b);
            }
        }
        WHEN("the attributes are not in the same order") {
            auto a = SimpleSelector::fromString("div.foo[title]");
            auto b = SimpleSelector::fromString("div[title].foo");

            THEN("the two selectors are equals") {
                REQUIRE(a == b);
            }
        }
        WHEN("the attributes are in the same order (even if there is more than an attribute of the same type)") {
            auto a = SimpleSelector::fromString("div.foo.bar[title]");
            auto b = SimpleSelector::fromString("div.foo.bar[title]");

            THEN("the two selectors are equals") {
                REQUIRE(a == b);
            }
        }
    }
    GIVEN("Two simple selectors that differ only for the type") {
        auto a = SimpleSelector::fromString("div.foo");
        auto b = SimpleSelector::fromString("p.foo");
        WHEN("compared") {
            THEN("the two are different") {
                REQUIRE(a != b);
            }
        }
    }
    GIVEN("Two simple selectors that differ only for one attribute type") {
        auto a = SimpleSelector::fromString("div#a.foo[title]");
        auto b = SimpleSelector::fromString("div#a.foo[name]");
        WHEN("compared") {
            THEN("the two are different") {
                REQUIRE(a != b);
            }
        }
    }
}

SCENARIO("css3 aux selectors can be compared using operator==", "[match][selector]") {
    using std::make_tuple;
    GIVEN("two ID selectors") {
        WHEN("they refers to the same id") {
            css3::ID a{"foo"}, b{"foo"};
            THEN("the two are considered equals") {
                REQUIRE(a == b);
            }
        }
        WHEN("they refers to different ids") {
            css3::ID a{"foo"}, b{"bar"};
            THEN("the two are not considered equals") {
                REQUIRE(a != b);
            }
        }
    }
    GIVEN("two class selectors") {
        WHEN("they refers to the same class") {
            css3::Class a{"foo"}, b{"foo"};
            THEN("the two are considered equals") {
                REQUIRE(a == b);
            }
        }
        WHEN("they refers to different ids") {
            css3::Class a{"foo"}, b{"bar"};
            THEN("the two are not considered equals") {
                REQUIRE(a != b);
            }
        }
    }
    GIVEN("two attribute selectors") {
        WHEN("they refers to the same triplet name/op/value") {
            css3::Attribute a1{"name", make_tuple("=", "value")}, b1{"name", make_tuple("=", "value")};
            css3::Attribute a2{"name", make_tuple("~=", "value")}, b2{"name", make_tuple("~=", "value")};
            css3::Attribute a3{"name"}, b3{"name"};
            THEN("the two are considered equals") {
                REQUIRE(a1 == b1);
                REQUIRE(a2 == b2);
                REQUIRE(a3 == b3);
            }
        }
        WHEN("they differs in at least one detail") {
            css3::Attribute a1{"name", make_tuple("=", "value")}, b1{"name", make_tuple("~=", "value")};
            css3::Attribute a2{"name", make_tuple("~=", "value")}, b2{"name", make_tuple("~=", "val")};
            css3::Attribute a3{"name"}, b3{"eman"};
            THEN("the two are not considered equals") {
                REQUIRE(a1 != b1);
                REQUIRE(a2 != b2);
                REQUIRE(a3 != b3);
            }
        }
        WHEN("they differs only for the quotes of the value") {
            css3::Attribute a1{"name", make_tuple("=", R"("Mr. 'x'")")}, b1{"name", make_tuple("=", R"('Mr. \'x\'')")};
            css3::Attribute a2{"name", make_tuple("=", R"("Mr. \"x\"")")}, b2{"name", make_tuple("=", R"('Mr. "x"')")};
            THEN("the two are considered equals") {
                REQUIRE(a1 == b1);
                REQUIRE(a2 == b2);
            }
        }
    }
    GIVEN("two pseudo-class selectors") {
        WHEN("they refers to the same class/function") {
            css3::PseudoClass a1{"hover"}, b1{"hover"};
            css3::PseudoClass a2{"nth-child(2)"}, b2{"nth-child(2)"};
            THEN("the two are considered equals") {
                REQUIRE(a1 == b1);
                REQUIRE(a2 == b2);
            }
        }
        WHEN("they refers to differents classes/functions") {
            css3::PseudoClass a1{"hover"}, b1{"before"};
            css3::PseudoClass a2{"nth-child(2)"}, b2{"nth-child(3)"};
            THEN("the two are not considered equals") {
                REQUIRE(a1 != b1);
                REQUIRE(a2 != b2);
            }
        }
        WHEN("they use and alias for nth-child(even) or nth-child(odd)") {
            css3::PseudoClass a1{"nth-child(even)"}, b1{"nth-child(2n+0)"};
            css3::PseudoClass a2{"nth-child(even)"}, b2{"nth-child(2n)"};
            css3::PseudoClass a3{"nth-child(even)"}, b3{"nth-child(even)"};
            css3::PseudoClass a4{"nth-child(odd)"}, b4{"nth-child(2n+1)"};
            css3::PseudoClass a5{"nth-child(odd)"}, b5{"nth-child(odd)"};
            THEN("the two are considered equals") {
                REQUIRE(a1 == b1);
                REQUIRE(a2 == b2);
                REQUIRE(a3 == b3);
                REQUIRE(a4 == b4);
                REQUIRE(a5 == b5);
            }
        }
    }
}

TEST_CASE("SimpleSelectors in a Selector can be replaced", "[selector]") {
    using namespace ls::sass::css3;
    SECTION("replace by index") {
        auto s = Selector::fromString("a b");
        s.replace_selector(1, SimpleSelector::fromString("c"));
        REQUIRE(render(s) == "{a} {c}");
    }
    SECTION("replace by negative index") {
        auto s = Selector::fromString("a b");
        s.replace_selector(-1, SimpleSelector::fromString("c"));
        REQUIRE(render(s) == "{a} {c}");
    }
    SECTION("replace by simple selector") {
        auto s = Selector::fromString("a b");
        s.replace_selector(
            SimpleSelector::fromString("b"),
            SimpleSelector::fromString("c"));
        REQUIRE(render(s) == "{a} {c}");
    }
    SECTION("replace by invalid index") {
        auto s = Selector::fromString("a b");
        REQUIRE_THROWS_AS(
            s.replace_selector(10, SimpleSelector::fromString("c")),
            std::out_of_range);
    }
    SECTION("replace by invalid simple selector") {
        auto s = Selector::fromString("a b");
        REQUIRE_THROWS_AS(
            s.replace_selector(
                SimpleSelector::fromString("q"),
                SimpleSelector::fromString("c")),
            std::out_of_range);
    }
}

SCENARIO("calculation of the difference between two vectors of AuxSelector", "[private][selector]") {
    using namespace ls::sass::css3;

    auto make = [](string const& input) -> vector<AuxSelector> {
        return SimpleSelector::fromString(input).tail;
    };

    GIVEN("Two identical vectors") {
        auto a = make(".foo");
        auto b = make(".foo");
        WHEN("the difference is computed") {
            auto r = a - b;
            THEN("the resultant vector is empty") {
                REQUIRE(r.size() == 0);
            }
        }
    }

    GIVEN("Two identical vectors (with different types of attributes") {
        auto a = make("#x.foo[name]:hover.bar");
        auto b = make("#x.foo[name]:hover.bar");
        WHEN("the difference is computed") {
            auto r = a - b;
            THEN("the resultant vector is empty") {
                REQUIRE(r.size() == 0);
            }
        }
    }

    GIVEN("Two equivalent vectors (same attributes, different order)") {
        auto a = make("#x.foo");
        auto b = make(".foo#x");
        WHEN("the difference is computed") {
            auto r = a - b;
            THEN("the resultant vector is empty") {
                REQUIRE(r.size() == 0);
            }
        }
    }

    GIVEN("The first vector completely contained in the second") {
        auto a = make(".foo");
        auto b = make(".foo:hover");
        WHEN("the difference is computed") {
            auto r = a - b;
            THEN("the resultant vector is empty") {
                REQUIRE(r.size() == 0);
            }
        }
    }

    GIVEN("The second vector completely contained in the first") {
        auto a = make(".foo:hover");
        auto b = make(".foo");
        WHEN("the difference is computed") {
            auto r = a - b;
            THEN("the resultant vector contains the difference between the two") {
                REQUIRE(r.size() == 1);
                REQUIRE(render(r[0]) == ":hover");
            }
        }
    }

    GIVEN("Two vectors with some common attributes") {
        auto a = make(".foo.bar:hover");
        auto b = make("#x.baz.foo:hover");
        WHEN("the difference is computed") {
            auto r1 = a - b;
            auto r2 = b - a;
            THEN("the resultant vector contains the difference between the first and the second") {
                REQUIRE(r1.size() == 1);
                REQUIRE(render(r1[0]) == ".bar");
                REQUIRE(r2.size() == 2);
                REQUIRE(render(r2[0]) == "#x");
                REQUIRE(render(r2[1]) == ".baz");
            }
        }
    }

    GIVEN("Two vectors with no attributes in common") {
        auto a = make(".foo");
        auto b = make(".bar");
        WHEN("the difference is computed") {
            auto r1 = a - b;
            auto r2 = b - a;
            THEN("the resultant vector is equal to the first") {
                REQUIRE(r1.size() == 1);
                REQUIRE(render(r1[0]) == ".foo");
                REQUIRE(r2.size() == 1);
                REQUIRE(render(r2[0]) == ".bar");
            }
        }
    }
}

SCENARIO("calculation of the union between two vectors of AuxSelector", "[private][selector]") {
    using namespace ls::sass::css3;

    auto make = [](string const& input) -> vector<AuxSelector> {
        return SimpleSelector::fromString(input).tail;
    };

    GIVEN("Two identical vectors") {
        auto a = make(".foo");
        auto b = make(".foo");
        WHEN("the union is computed") {
            auto r = a | b;
            THEN("the resultant vector contains no duplicates") {
                REQUIRE(r.size() == 1);
                REQUIRE(render(r[0]) == ".foo");
            }
        }
    }

    GIVEN("Two vectors with some common attributes") {
        auto a = make(".foo.bar");
        auto b = make(".foo.baz:hover");
        WHEN("the union is computed") {
            auto r = a | b;
            THEN("the resultant vector contains no duplicates") {
                REQUIRE(r.size() == 4);
                REQUIRE(render(r) == ".foo.bar.baz:hover");
            }
        }
    }

    GIVEN("Two vectors with no common attributes") {
        auto a = make(".foo.bar");
        auto b = make(".baz");
        WHEN("the union is computed") {
            auto r = a | b;
            THEN("the resultant vector contains all the attributes") {
                REQUIRE(r.size() == 3);
                REQUIRE(render(r) == ".foo.bar.baz");
            }
        }
    }

    GIVEN("Two vectors of which one is empty") {
        auto a = make(".foo.bar");
        auto b = make(".baz") - make(".baz");

        REQUIRE(b.size() == 0);
        WHEN("the union is computed") {
            auto r = a | b;
            THEN("the resultant vector contains all the attributes") {
                REQUIRE(r.size() == 2);
                REQUIRE(render(r) == ".foo.bar");
            }
        }
    }
}
