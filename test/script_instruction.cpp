#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <ls/sass_script/expressions.hpp>
#include <ls/sass_script/instructions.hpp>

namespace {
    using std::vector;
    using std::string;
    using ls::sass_script::data::IScope;
    using ls::sass_script::instructions::_Program;

    vector<string> execute(_Program const& p, IScope& scope) {
        vector<string> output;
        auto runner = p.runner(scope);
        while (auto s = runner->step()) {
            output.push_back(*s);
        }
        return output;
    }
}

SCENARIO("`For` instructions can loop over a program", "[sass_script][instruction]") {
    using std::vector;
    using std::string;
    using ls::sass_script::data::Scope;
    using ls::sass_script::expression::LoadVariable;
    using namespace ls::sass_script::instructions;

    GIVEN("A `For` instruction from 1 to 3") {
        auto f = For("index", 1, 3, false);
        f.emplace_back<Echo>(std::make_unique<LoadVariable>("index"));

        WHEN("Executed on an empty Scope") {
            auto scope = Scope();
            auto result = execute(f, scope);
            THEN ("The inner program is executed exactly 2 times") {
                auto expected = vector<string>{"1", "2"};
                REQUIRE(result == expected);
            }
        }
    }

    GIVEN("A `For` instruction from 3 to 1") {
        auto f = For("index", 3, 1, false);
        f.emplace_back<Echo>(std::make_unique<LoadVariable>("index"));

        WHEN("Executed on an empty Scope") {
            auto scope = Scope();
            auto result = execute(f, scope);
            THEN ("The inner program is executed exactly 2 times (in the declared order)") {
                auto expected = vector<string>{"3", "2"};
                REQUIRE(result == expected);
            }
        }
    }

    GIVEN("A `For` instruction from 1 to 3 (included)") {
        auto f = For("index", 1, 3, true);
        f.emplace_back<Echo>(std::make_unique<LoadVariable>("index"));

        WHEN("Executed on an empty Scope") {
            auto scope = Scope();
            auto result = execute(f, scope);
            THEN ("The inner program is executed exactly 3 times") {
                auto expected = vector<string>{"1", "2", "3"};
                REQUIRE(result == expected);
            }
        }
    }

    GIVEN("A `For` instruction from 1 to 1") {
        auto f = For("index", 1, 1, false);
        f.emplace_back<Echo>(std::make_unique<LoadVariable>("index"));

        WHEN("Executed on an empty Scope") {
            auto scope = Scope();
            auto result = execute(f, scope);
            THEN ("The inner program is executed exactly 0 times") {
                auto expected = vector<string>{};
                REQUIRE(result == expected);
            }
        }
    }

    GIVEN("A `For` instruction from 1 to 1 (included)") {
        auto f = For("index", 1, 1, true);
        f.emplace_back<Echo>(std::make_unique<LoadVariable>("index"));

        WHEN("Executed on an empty Scope") {
            auto scope = Scope();
            auto result = execute(f, scope);
            THEN ("The inner program is executed exactly 1 times") {
                auto expected = vector<string>{"1"};
                REQUIRE(result == expected);
            }
        }
    }
}
