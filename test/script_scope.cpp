#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <ls/sass_script/data.hpp>

using namespace ls::sass_script::data;
using std::make_unique;

struct XDataType : DataType {
    std::string s{""};
    std::string repr() const override { return "XDataType{}"; }
    std::string const& as_string() const override { return s; }
};

SCENARIO("A variable can be constructed directly inside a scope", "[sass_script][scope]") {
    GIVEN("An empty scope") {
        Scope a;
        WHEN("A variable is constructed") {
            String* s = a.emplace<String>("a", "hello world");
            THEN("The returned pointer is the same that is stored in the scope") {
                REQUIRE(s == a.find("a"));
            }
        }
    }
}

SCENARIO("An existing pointer can be moved to a scope", "[sass_script][scope]") {
    GIVEN("An empty scope and a Variable instance") {
        Scope a;
        String* s = new String{"hello world"};
        WHEN("The variable is moved to the scope") {
            a.set("a", std::unique_ptr<String>(s));
            THEN("The pointer stored into the scope is the same of the original variable") {
                REQUIRE(a.find("a") == s);
            }
        }
    }
}

SCENARIO("A scope can be used to access the variables by name", "[sass_script][scope]") {
    GIVEN("A scope with a variable stored") {
        Scope a;
        auto s = a.emplace<String>("a", "hello world");
        WHEN("The scope is used to check the existence of a variable") {
            auto x = a.find("a");
            THEN("A pointer to the original variable is returned") {
                REQUIRE(x == s);
            }
        }
        WHEN("The scope is used to get a variable") {
            auto& x = a.var<String>("a");
            THEN("The original variable is returned") {
                REQUIRE(&x == s);
            }
        }
        WHEN("The scope is used to check the existence of a not present variable") {
            auto x = a.find("b");
            THEN("Null is returned") {
                REQUIRE(x == nullptr);
            }
        }
        WHEN("The scope is used to get a non existent variable") {
            THEN("An exception is thrown") {
                REQUIRE_THROWS_AS(a.get("b"), variable_not_found);
                REQUIRE_THROWS_AS(a.var<String>("b"), variable_not_found);
            }
        }
        WHEN("The wrong case is used to access the variable") {
            auto x = a.find("A");
            THEN("The variable is not found") {
                REQUIRE(x == nullptr);
            }
        }
        WHEN("The wrong type is used to retrieve the variable") {
            auto x = a.emplace<XDataType>("b");
            REQUIRE(a.find("b") == x);
            THEN("An exception is thrown") {
                REQUIRE_THROWS_AS(a.var<String>("b"), type_error);
            }
        }
    }
}

SCENARIO("Variable can be replaced but not changed", "[sass_script][scope]") {
    GIVEN("A scope with a variable stored") {
        Scope a;
        auto s = a.emplace<String>("a", "hello world");
        WHEN("A new variable is set for the same name") {
            auto s2 = new String{"have a nice day"};
            a.set("a", std::unique_ptr<String>{s2});
            THEN("The new variable takes the place of the old one, is not copied over") {
                REQUIRE(a.find("a") == s2);
            }
        }
        WHEN("A new variable is constructed over the same name") {
            auto s2 = a.emplace<String>("a", "have a nice day");
            REQUIRE(s2 != s);
            THEN("The new variable takes the place of the old one, is not copied over") {
                REQUIRE(a.find("a") == s2);
            }
        }
        WHEN("A new variable is set for the same name") {
            auto s2 = new String{"have a nice day"};
            auto alive = a.set("a", std::unique_ptr<String>{s2});
            THEN("The hold variable is still alive until a reference to the unique_ptr is kept") {
                REQUIRE(alive.get() == s);
            }
        }
        WHEN("A new variable of different type replace the old name") {
            auto x = new XDataType{};
            auto alive = a.set("a", std::unique_ptr<XDataType>{x});
            THEN("All works as expected") {
                REQUIRE(a.find("a") == x);
                REQUIRE(&(a.var<XDataType>("a")) == x);
                REQUIRE(alive.get() == s);
            }
        }
    }
}

SCENARIO("Scopes can be connected to each other") {
    GIVEN("Two scopes chained together") {
        Scope a;
        Scope b;
        ChainedScope x{a, b};

        auto aa = a.emplace<String>("a", "Hello world");
        auto ab = a.emplace<String>("b", "have a nice day");
        auto bb = b.emplace<String>("b", "have a good day");

        WHEN("The resulting scope is used to get a variable that exists only in the first scope") {
            auto p = x.find("a");
            THEN("A pointer to the variable in the first scope is returned") {
                REQUIRE(p == aa);
            }
        }

        WHEN("The resulting scope is used to get a variable that exists in both scopes") {
            auto p = x.find("b");
            THEN("The variable in the second scope takes precedence") {
                REQUIRE(p != ab);
                REQUIRE(p == bb);
            }
        }

        WHEN("The resulting scope is used to add a new variable") {
            auto c = x.emplace<String>("c", "42");
            THEN("The new variable is added to the second scope") {
                REQUIRE(c == b.find("c"));
            }
        }

        WHEN("The resulting scope is used to replace a variable that exists only in the first scope") {
            auto c = x.emplace<String>("a", "42");
            THEN("Only the first scope changes") {
                REQUIRE(c == a.find("a"));
                REQUIRE(b.find("a") == nullptr);
            }
        }

        WHEN("The resulting scope is used to replace a variable that exists in both scopes") {
            auto c = x.emplace<String>("b", "42");
            THEN("Only the second scope changes") {
                REQUIRE(c == b.find("b"));
                REQUIRE(ab == a.find("b"));
            }
        }
    }
}
SCENARIO("A scopes graph is traversed from the right to the left") {
    GIVEN("Two `ChainedScope`s linkd together") {
        Scope a;
        Scope b;
        ChainedScope x{a, b};

        Scope c;
        Scope d;
        ChainedScope y{c, d};

        ChainedScope z{x, y};

        auto aa = a.emplace<String>("a", "");
        auto bb = b.emplace<String>("b", "");
        auto cc = c.emplace<String>("c", "");
        auto dd = d.emplace<String>("d", "");

        WHEN("The resulting scope is queried for variables") {
            auto za = z.find("a");
            auto zb = z.find("b");
            auto zc = z.find("c");
            auto zd = z.find("d");
            THEN("The object graph is correctly traversed") {
                REQUIRE(za == aa);
                REQUIRE(zb == bb);
                REQUIRE(zc == cc);
                REQUIRE(zd == dd);
            }
        }

        WHEN("The resulting scope is queried for variables that have a duplicate") {
            a.emplace<String>("d", "");
            auto p = z.find("d");
            THEN("The variable in the rightmost scope is returned") {
                REQUIRE(p == dd);
            }
        }

        WHEN("The resulting scope is used to add a new variables") {
            auto p = z.emplace<String>("x", "");
            THEN("Only the rightmost scope changes") {
                REQUIRE(p == d.find("x"));
            }
        }

        WHEN("The resulting scope is used to replace a variable") {
            b.emplace<String>("c", "");

            auto p = new String{""};
            z.set("c", std::unique_ptr<String>(p));
            THEN("Among the scopes in which the variable is found, only the rightmost changes") {
                REQUIRE(p == c.find("c"));
            }
        }
    }
}
