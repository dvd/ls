#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <ls/sass/directives/block.hpp>
#include <ls/sass/exception.hpp>
#include <ls/strings.hpp>

using namespace ls::sass::directives;
using namespace std;

namespace {
    struct Counter {
        size_t* count = nullptr;

        Counter(size_t* p) : count{p} {}

        template <typename T>
        bool operator()(T const&) const {
            (*count)++;
            return false;
        }
    };
    size_t count_directives(Block const& b) {
        size_t n=0;
        b.directives(Counter{&n});
        return n;
    }
    template<typename T, typename... Args>
    std::unique_ptr<T> make_unique(Args&&... args) {
        return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
    }
}

SCENARIO("A Block can be constructed programmatically", "[block]") {
    GIVEN("an ampty block") {
        Block b{};

        REQUIRE( b.parent() == nullptr );
        REQUIRE( b.selectors().size() == 0 );
        REQUIRE( count_directives(b) == 0 );

        WHEN("an Extend is constructed inplace") {
            Extend* e = b.emplace_back<Extend>();
            REQUIRE(e->parent() == &b);

            THEN("the number of directives change") {
                REQUIRE(count_directives(b) == 1);

                auto v = b.directives<Extend>();
                REQUIRE(v.size() == 1);
            }
        }

        WHEN("an Extend is added") {
            Extend* e = new Extend{};

            auto p = std::unique_ptr<Extend>{e};
            REQUIRE(p->parent() == nullptr);

            b.push_back(std::move(p));
            REQUIRE(!p);
            REQUIRE(e->parent() == &b);

            THEN("the number of directives change") {
                REQUIRE(count_directives(b) == 1);

                auto v = b.directives<Extend>();
                REQUIRE(v.size() == 1);
                REQUIRE(v[0] == e);
            }
        }

        WHEN("a Block is added") {
            Block* child = new Block{};
            b.push_back(std::unique_ptr<Block>{child});

            REQUIRE(child->parent() == &b);

            THEN("the number of directives change") {
                REQUIRE(count_directives(b) == 1);

                auto v = b.directives<Block>();
                REQUIRE(v.size() == 1);
                REQUIRE(v[0] == child);
            }
        }
    };
    GIVEN("A Block with a child") {
        Block b{};
        Block* child = b.emplace_back<Block>();

        WHEN("The child is removed") {
            std::unique_ptr<Directive> alive{b.remove(*child)};

            THEN("The number of directives change to zero") {
                REQUIRE(count_directives(b) == 0);
            }
            THEN("The child has the parent set to null") {
                REQUIRE(child->parent() == nullptr);
            }
            THEN("The child is kept alive until the unique_ptr is not destructed") {
                REQUIRE(alive);
                REQUIRE(alive.get() == child);
            }
            THEN("The child can be added to another Block") {
                Block b2{};
                b2.push_back(std::move(alive));

                REQUIRE(child->parent() == &b2);
            }
        }
    };
    GIVEN("A Block and a directive without parent") {
        Block b{};
        Extend* e = new Extend{};

        WHEN("The directive is moved on the block") {
            THEN("An exception is raised") {
                REQUIRE_THROWS_AS(b.move(*e), ls::owner_error);
            }
        }
        WHEN("The directive is assigned to the block") {
            b.push_back(std::unique_ptr<Extend>{e});

            WHEN("The directive is moved on the same block") {
                THEN("An exception is raised") {
                    REQUIRE_THROWS_AS(b.move(*e), ls::owner_error);
                }
            }
        }
        WHEN("The directive is assigned to another block") {
            Block b2{};
            b2.push_back(std::unique_ptr<Extend>{e});

            REQUIRE(e->parent() == &b2);

            WHEN("The directive is moved on the first block") {
                b.move(*e);

                THEN("The directive change parent") {
                    REQUIRE(e->parent() == &b);
                }

                THEN("The first block now as one directive") {
                    REQUIRE(count_directives(b) == 1);
                }

                THEN("The new block now as zero directives") {
                    REQUIRE(count_directives(b2) == 0);
                }
            }
        }
    }
    GIVEN("A Block with a sub-tree") {
        Block b{};
        Extend* e1 = b.emplace_back<Extend>();
        Block* child = b.emplace_back<Block>();
        Extend* e2 = child->emplace_back<Extend>();
        Extend* e3 = child->emplace_back<Extend>();

        WHEN("The sub-block is moved in a new block") {
            Block b2{};
            b2.move(*child);

            THEN("The entire sub-tree is moved") {
                REQUIRE(child->parent() == &b2);
                REQUIRE(e2->parent() == child);
                REQUIRE(e2->parent()->parent() == &b2);
                REQUIRE(e3->parent() == child);
                REQUIRE(e3->parent()->parent() == &b2);
                REQUIRE(e1->parent() == &b);

                REQUIRE(count_directives(b) == 1);
                REQUIRE(count_directives(b2) == 1);
                REQUIRE(count_directives(*child) == 2);
            }
        };
    };
    GIVEN("A Block with a child and a directive without parent") {
        Block b{};
        Extend* e = b.emplace_back<Extend>();
        Block* child = new Block{};

        WHEN("The child is replaced with the directive") {
            auto alive = b.replace(*e, *child);

            THEN("The child parent is set to null") {
                REQUIRE(e->parent() == nullptr);
            }

            THEN("The child is kept alive until the unique_ptr is not destructed") {
                REQUIRE(alive);
                REQUIRE(alive.get() == e);
            }

            THEN("The new directive is associated with the parent") {
                REQUIRE(child->parent() == &b);
            }
        }
        WHEN("The directive is assigned to the block") {
            b.push_back(std::unique_ptr<Block>{child});

            WHEN("The child is replaced with the directive") {
                THEN("An exception is raised") {
                    REQUIRE_THROWS_AS(b.replace(*e, *child), ls::owner_error);
                }
            }
        }
        WHEN("The directive is assigned to a new block") {
            Block b2{};
            b2.push_back(std::unique_ptr<Block>{child});
            REQUIRE(count_directives(b2) == 1);

            WHEN("The child is replaced with the directive") {
                b.replace(*e, *child);

                THEN("The new directive is moved into the parent") {
                    REQUIRE(child->parent() == &b);
                    REQUIRE(count_directives(b2) == 0);
                }
            }
        }
    };
    GIVEN("A Block with a child") {
        Block b{};
        Extend* child = b.emplace_back<Extend>();

        WHEN("The child is removed two times") {
            auto alive = b.remove(*child);
            THEN("An exception is raised") {
                REQUIRE_THROWS_AS(b.remove(*child), ls::owner_error);
            }
        }
        WHEN("The child is copied") {
            Extend e2 = *child;
            WHEN("The copied child is removed") {
                THEN("An exception is raised") {
                    REQUIRE_THROWS_AS(b.remove(e2), ls::owner_error);
                }
            }
            WHEN("The right child is removed") {
                b.remove(*child);
                WHEN("The copied child is removed") {
                    THEN("An exception is raised") {
                        REQUIRE_THROWS_AS(b.remove(e2), ls::owner_error);
                    }
                }
            }
        }
        WHEN("The child is removed from another block") {
            Block b2{};

            THEN("An exception is raised") {
                REQUIRE_THROWS_AS(b2.remove(*child), ls::owner_error);
            }
        }
    }
};
namespace {
    struct V {
        int* total;
        int* extends;
        int* blocks;
        V(int* total, int* extends, int* blocks)
            : total{total}, extends{extends}, blocks{blocks} {}

        template <typename T>
        bool operator()(T const &) const {
            (*total)++;
            return false;
        }

        bool operator()(Extend const&) const {
            (*total)++;
            *extends += 1 << *total;
            return false;
        }

        bool operator()(Block const&) const {
            (*total)++;
            *blocks += 1 << *total;
            return false;
        }
    };
}
SCENARIO("Directives in a block can be visited", "[block]") {
    GIVEN("A block with directives of two types") {
        Block b{};
        auto e1 = b.emplace_back<Extend>();
        auto b1 = b.emplace_back<Block>();
        auto e2 = b.emplace_back<Extend>();
        auto b2 = b.emplace_back<Block>();

        WHEN("The directives is visited") {
            int total=0;
            int extends=0;
            int blocks=0;

            b.directives(V{&total, &extends, &blocks});

            THEN("The visitor is called for every directive") {
                REQUIRE(total == 4);
            }
            THEN("The directives are visited in the correct order") {
                REQUIRE(extends == 10);
                REQUIRE(blocks == 20);
            }
        }
        WHEN("The directives are filtered by type") {
            auto ve = b.directives<Extend>();
            auto vb = b.directives<Block>();

            THEN("The directives are returned in order") {
                REQUIRE(ve.size() == 2);
                REQUIRE(ve[0] == e1);
                REQUIRE(ve[1] == e2);

                REQUIRE(vb.size() == 2);
                REQUIRE(vb[0] == b1);
                REQUIRE(vb[1] == b2);
            }
        }
    };
};
SCENARIO("A tree of blocks can be visited using an iterator", "[block]") {
    GIVEN("A tree of directives") {
        Block b{};
        auto e1 = b.emplace_back<Extend>();

        auto b2 = b.emplace_back<Block>();
        auto b2e1 = b2->emplace_back<Extend>();
        auto b2b1 = b2->emplace_back<Block>();

        auto e2 = b.emplace_back<Extend>();
        auto b3 = b.emplace_back<Block>();

        WHEN("An iterator for the Block type is build") {
            auto it = b.begin<Block>();
            auto end = b.end<Block>();

            REQUIRE(it != end);

            THEN("All the blocks are visited in a depth-first order") {
                REQUIRE(&(*it++) == &b);
                REQUIRE(it != end);
                REQUIRE(&(*it++) == b2);
                REQUIRE(it != end);
                REQUIRE(&(*it++) == b2b1);
                REQUIRE(it != end);
                REQUIRE(&(*it++) == b3);
                REQUIRE(it == end);
            }
        }

        WHEN("An iterator for the Extend type is build") {
            auto it = b.begin<Extend>();
            auto end = b.end<Extend>();

            REQUIRE(it != end);

            THEN("All the `extend`s are visited in a depth-first order") {
                REQUIRE(&(*it++) == e1);
                REQUIRE(it != end);
                REQUIRE(&(*it++) == b2e1);
                REQUIRE(it != end);
                REQUIRE(&(*it++) == e2);
                REQUIRE(it == end);
            }
        }
        WHEN("An iterator is build") {
            auto it = b.begin<Block>();
            auto end = b.end<Block>();

            THEN("It can be used with a standard algorithm") {
                auto found = find_if(it, end, [b2b1](Block const& x) { return &x == b2b1;});
                REQUIRE(found != end);
                REQUIRE(&(*found) == b2b1);
            }
        }
    };
    GIVEN("An iterator to the end") {
        Block b{};
        auto it = b.end<Block>();

        WHEN("Incremented") {
            it++;
            THEN("The iterator is still equal to an end iterator") {
                REQUIRE(it == b.end<Block>());
            }
        }
    }
};
SCENARIO("A block can be transplanted to the root", "[block]") {
    using namespace ls::sass::css3;
    GIVEN("A tree of blocks") {
        Block root{};
        auto b1 = root.emplace_back<Block>(vector<Selector>{
            Selector{SimpleSelector{Type{"a"}}}
        });
        auto b2 = b1->emplace_back<Block>(vector<Selector>{
            Selector{SimpleSelector{Type{"b"}}}
        });
        WHEN("The inner child is transplanted") {
            transplant(*b2);
            THEN("The child is moved to the root but the selectors are updated to reflect the previous location") {
                REQUIRE(b1->parent() == &root);
                REQUIRE(b2->parent() == &root);
                REQUIRE(supplant("%", b1->selectors()) == "a");
                REQUIRE(supplant("%", b2->selectors()) == "a b");
            }
        }
        WHEN("The outer child is transplanted") {
            transplant(*b1);
            THEN("Nothing happens") {
                REQUIRE(b1->parent() == &root);
                REQUIRE(b2->parent() == b1);
                REQUIRE(supplant("%", b1->selectors()) == "a");
                REQUIRE(supplant("%", b2->selectors()) == "b");
            }
        }
        WHEN("The root is transplanted") {
            THEN("An exception is raised") {
                REQUIRE_THROWS_AS(transplant(root), ls::owner_error);
            }
        }
    }
    GIVEN("A tree of blocks with multiple selectors") {
        Block root{};
        auto b1 = root.emplace_back<Block>(vector<Selector>{
            Selector{SimpleSelector{Type{"a"}}},
            Selector{SimpleSelector{Type{"c"}}},
        });
        auto b2 = b1->emplace_back<Block>(vector<Selector>{
            Selector{SimpleSelector{Type{"b"}}},
            Selector{SimpleSelector{Type{"d"}}},
        });
        WHEN("The inner child is transplanted") {
            transplant(*b2);
            THEN("The child is moved to the root but the selectors are updated to reflect the previous location") {
                REQUIRE(b1->parent() == &root);
                REQUIRE(b2->parent() == &root);
                REQUIRE(supplant("%", b1->selectors()) == "a, c");
                REQUIRE(supplant("%", b2->selectors()) == "a b, a d, c b, c d");
            }
        }
    }
};
