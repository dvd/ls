#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <map>
#include <string>
#include <ls/sass/directives/block.hpp>

namespace {
    using std::map;
    using std::string;
    using namespace ls::sass::directives;

    struct Walk : TreeVisitor {
        mutable map<string, int> ins;
        mutable map<string, int> outs;
        string stop_at = "";

        Walk() {}
        Walk(string stop_at) : stop_at{stop_at} {}

        using TreeVisitor::enter;
        using TreeVisitor::leave;

        bool enter(Block const&) const {
            ins["block"] += 1;
            return stop_at == "block";
        }
        bool leave(Block const&) const {
            outs["block"] += 1;
            return false;
        }
        bool enter(Property const&) const {
            ins["property"] += 1;
            return stop_at == "property";
        }
        bool leave(Property const&) const {
            outs["property"] += 1;
            return false;
        }
        bool enter(Extend const&) const {
            ins["extend"] += 1;
            return stop_at == "extend";
        }
        bool leave(Extend const&) const {
            outs["extend"] += 1;
            return false;
        }
    };
};

SCENARIO("block structure can be traversed", "[traversal]") {
    using namespace ls;
    using namespace ls::sass::directives;

    GIVEN("a root block with no children") {
        Block root{};

        WHEN("the block is traversed") {
            Walk walker;
            traverse_tree(root, walker);

            THEN("the visitor must have seen every element in the structure") {
                REQUIRE(walker.ins["block"] == 1);
                REQUIRE(walker.outs["block"] == 1);
            }
        }
    }

    GIVEN("a root block with two properties") {
        Block root{};
        root.emplace_back<Property>("color", "black");
        root.emplace_back<Property>("border", "none");

        WHEN("the block is traversed") {
            Walk walker;
            traverse_tree(root, walker);

            THEN("the visitor must have seen every element in the structure") {
                REQUIRE(walker.ins["block"] == 1);
                REQUIRE(walker.outs["block"] == 1);
                REQUIRE(walker.ins["property"] == 2);
                REQUIRE(walker.outs["property"] == 2);
            }
        }
        WHEN("the traversal ends at the first property") {
            Walk walker{"property"};
            traverse_tree(root, walker);

            THEN("the visitor must have seen the root block and the first property (both partially)") {
                REQUIRE(walker.ins["block"] == 1);
                REQUIRE(walker.outs["block"] == 0);
                REQUIRE(walker.ins["property"] == 1);
                REQUIRE(walker.outs["property"] == 0);
            }
        }
    }
    GIVEN("a root block with another block") {
        Block root{};
        root.emplace_back<Property>("color", "black");
        auto child = root.emplace_back<Block>();
        child->emplace_back<Property>("color", "blue");

        WHEN("the block is traversed") {
            Walk walker;
            traverse_tree(root, walker);

            THEN("the visitor must have seen every element in the structure") {
                REQUIRE(walker.ins["block"] == 2);
                REQUIRE(walker.outs["block"] == 2);
                REQUIRE(walker.ins["property"] == 2);
                REQUIRE(walker.outs["property"] == 2);
            }
        }
    }
}
