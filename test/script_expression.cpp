#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <ls/sass_script/data.hpp>
#include <ls/sass_script/expressions.hpp>
#include <ls/strings.hpp>

using namespace ls::sass_script::expression;
using ls::repr;
using std::make_unique;
namespace data = ls::sass_script::data;

struct Ping : Function<0> {
    using Function<0>::operator();

    std::unique_ptr<data::DataType> operator()() const {
        return make_unique<data::String>("ping");
    }

    Signature const& signature() const override {
        static Signature sig{};
        return sig;
    }
};

struct Add : Function<2> {
    using Function<2>::operator();

    std::unique_ptr<data::DataType> operator()(data::String const& a, data::String const& b) const {
        return make_unique<data::String>(supplant("%+%", a.as_string(), b.as_string()));
    }

    Signature const& signature() const override {
        static Signature sig;
        if (sig.size() == 0) {
            sig.push_back("op1", required);
            sig.push_back("op2", required);
        };
        return sig;
    }
};

auto PingF = wrap_function(Ping{});
auto AddF = wrap_function(Add{});

SCENARIO("LoadVariable must be used to retrieve a variable by name") {
    GIVEN("A scope with a variable") {
        data::Scope scope;
        scope.emplace<data::String>("a", "hello world");

        WHEN("LoadVariable is used with the correct name") {
            LoadVariable expr{"a"};
            THEN("The variable is retrieved correctly") {
                auto r = expr.evaluate(scope);
                REQUIRE(repr(r) == "String{hello world}");
            }
        }
        WHEN("LoadVariable is used with a variable that doesn't exist") {
            LoadVariable expr{"b"};
            THEN("An exception is raised") {
                REQUIRE_THROWS_AS(expr.evaluate(scope), data::variable_not_found);
            }
        }
    }
}

SCENARIO("LoadConst must be used to wrap a constant value in an Expression") {
    GIVEN("A DataType wrapped in a LoadConst expression") {
        LoadConst expr{make_unique<data::String>("hello world")};

        THEN("The expression can be used to retrieve the orginal datatype") {
            data::Scope scope;
            auto r = expr.evaluate(scope);
            REQUIRE(repr(r) == "String{hello world}");
        }
    }
}

SCENARIO("A WrappedFunction must be used to call a Function using a vector of DataType") {
    GIVEN("A function that accepts zero arguments") {
        auto args = std::vector<data::DataType const*>{};

        WHEN("The function is called") {
            THEN("The result is what expected") {
                REQUIRE(repr(*PingF(args)) == "String{ping}");
            }
        }
    }
    GIVEN("A function that accepts two arguments") {
        auto args = std::vector<data::DataType const*>{
            new data::String{"A"},
            new data::String{"B"}
        };

        WHEN("The function is called") {
            THEN("The correct overload is choosen and the arguments vector is completly consumed") {
                REQUIRE(repr(*AddF(args)) == "String{A+B}");
                REQUIRE(args.size() == 0);
            }
        }


        WHEN("The function is called with arguments of unexpected type") {
            args[0] = new data::Color{};
            THEN("An execption (with the offending signature) is raised") {
                try {
                    AddF(args);
                }
                catch(invalid_signature& e) {
                    REQUIRE(std::string{e.what()} == "ls::sass_script::data::Color,ls::sass_script::data::String");
                }
            }
        }

        WHEN("The function is called with more arguments than needed") {
            args.push_back(new data::String{"C"});
            THEN("An execption is raised") {
                REQUIRE_THROWS_AS(AddF(args), invalid_signature);
            }
        }

        WHEN("The function is called with fewer arguments than needed") {
            args.erase(args.begin());
            THEN("An execption is raised") {
                REQUIRE_THROWS_AS(AddF(args), invalid_signature);
            }
        }
    }
}

SCENARIO("A function Signature holds both the name of the arguments that the default values") {
    GIVEN("A signature with a required argument and a optional one") {
        Signature sig;
        sig.push_back("a", required);
        sig.push_back("b", make_unique<data::Color>());
        WHEN("repr is called") {
            THEN("A human readable version of the signature is returned") {
                REQUIRE(repr(sig) == "a,[b=Color{0,0,0,0}]");
            }
        }
    }
}

SCENARIO("A FunctionInstance can be used to call a function from a set of expressions") {
    data::Scope scope;
    scope.emplace<data::String>("a", "hello");
    scope.emplace<data::String>("b", "world");
    scope.emplace<data::Color>("c");

    GIVEN("A function that accepts two arguments") {
        FunctionInstance f{AddF};

        WHEN("The function is called with the correct number of arguments and the expressions resolve to the correct types") {
            f.push_back(make_unique<LoadVariable>("a"));
            f.push_back(make_unique<LoadVariable>("b"));

            THEN("The result is what expected") {
                REQUIRE(repr(*f.call(scope)) == "String{hello+world}");
            }
        }

        WHEN("The function is called with too few arguments") {
            f.push_back(make_unique<LoadVariable>("a"));

            THEN("An exception is raised") {
                REQUIRE_THROWS_AS(f.call(scope), required_argument);
            }
        }

        WHEN("The function is called using name arguments") {
            f.push_back("op1", make_unique<LoadVariable>("a"));
            f.push_back("op2", make_unique<LoadVariable>("b"));

            THEN("The result is what expected") {
                REQUIRE(repr(*f.call(scope)) == "String{hello+world}");
            }
        }

        WHEN("An unknown named argument is used") {
            THEN("An exception is raised") {
                REQUIRE_THROWS_AS(
                    f.push_back("foo", make_unique<LoadVariable>("a")),
                    unknown_argument);
            }
        }

        WHEN("Too many arguments are used") {
            THEN("An exception is raised") {
                f.push_back(make_unique<LoadVariable>("a"));
                f.push_back(make_unique<LoadVariable>("b"));
                REQUIRE_THROWS_AS(
                    f.push_back(make_unique<LoadVariable>("c")),
                    argument_out_of_range);
            }
        }

        WHEN("A positional argument is used after a named one") {
            f.push_back("op1", make_unique<LoadVariable>("a"));
            THEN("An exception is raised") {
                REQUIRE_THROWS_AS(
                    f.push_back(make_unique<LoadVariable>("b")),
                    positional_argument_now_allowed);
            }
        }

        WHEN("An argument is defined twice") {
            f.push_back(make_unique<LoadVariable>("a"));
            THEN("An exception is raised") {
                REQUIRE_THROWS_AS(
                    f.push_back("op1", make_unique<LoadVariable>("b")),
                    argument_redefined);
            }
        }
    }

    GIVEN("A function that accepts two arguments but the second is optional") {
        struct AddX : Function<2> {
            using Function<2>::operator();

            std::unique_ptr<data::DataType> operator()(data::String const& a, data::String const& b) const {
                return make_unique<data::String>(supplant("%+%", a.as_string(), b.as_string()));
            }

            Signature const& signature() const override {
                static Signature sig;
                if (sig.size() == 0) {
                    sig.push_back("op1", required);
                    sig.push_back("op2", make_unique<data::String>("foo"));
                };
                return sig;
            }
        };

        auto AddXF = wrap_function(AddX{});
        FunctionInstance f{AddXF};

        WHEN("The optional argument is not specified") {
            f.push_back(make_unique<LoadVariable>("a"));

            THEN("The default value is used") {
                REQUIRE(repr(*f.call(scope)) == "String{hello+foo}");
            }
        }

        WHEN("The optional argument is specified") {
            f.push_back(make_unique<LoadVariable>("a"));
            f.push_back(make_unique<LoadVariable>("b"));

            THEN("The new value is used") {
                REQUIRE(repr(*f.call(scope)) == "String{hello+world}");
            }
        }

    }
}

SCENARIO("CallFunction must be used to call a function") {

    GIVEN("A function and a scope") {

        data::Scope scope;
        scope.emplace<data::String>("a", "hello");
        scope.emplace<data::String>("b", "world");

        CallFunction f{AddF};
        WHEN("Number e type of the arguments are correct") {
            auto& inst = f.instance();
            inst.push_back(make_unique<LoadVariable>("a"));
            inst.push_back(make_unique<LoadConst>(make_unique<data::String>("!")));

            THEN("The function evaluate to the expected result") {
                REQUIRE(repr(f.evaluate(scope)) == "String{hello+!}");
            }
        }
    }
}
