#include "compiler.hpp"
#include <boost/spirit/include/support_istream_iterator.hpp>

namespace ls { namespace sass_script { namespace compiler {
using std::unique_ptr;

unique_ptr<si::Program> compile(Context const& ctx, std::string const& input) {
    return _parse(ctx, std::begin(input), std::end(input));
}

unique_ptr<si::Program> compile(Context const& ctx, std::istream& input) {
    using boost::spirit::istream_iterator;
    istream_iterator start{input};
    istream_iterator end;
    return _parse(ctx, istream_iterator{input}, istream_iterator{});
}

}}}
