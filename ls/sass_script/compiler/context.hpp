#pragma once
#include <string>
#include <unordered_map>
#include <ls/sass_script/expressions/function.hpp>

namespace ls { namespace sass_script { namespace compiler {
    namespace se = ls::sass_script::expression;

    class Context {
        public:
            using registry_t = std::unordered_map<std::string, se::WrappedFunction>;
        private:
            registry_t _registry;
        public:
            se::WrappedFunction const& function(std::string) const;
            Context& function(std::string, se::WrappedFunction);

            registry_t const& registry() const;
            Context& registry(registry_t const&);
            Context& registry(registry_t&&);
    };

    Context default_context();
}}}
