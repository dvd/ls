#pragma once
#include <istream>
#include <memory>
#include <ls/sass/exception.hpp>
#include <ls/sass_script/instructions.hpp>
#include <ls/sass_script/grammars.hpp>

namespace ls { namespace sass_script { namespace compiler {
    namespace si = ls::sass_script::instructions;

    class Context;

    template <typename T>
    struct has_begin {
        template <typename U>
        static constexpr std::true_type f(decltype(std::begin(std::declval<U const>()))*);

        template <typename>
        static constexpr std::false_type f(...);

        static constexpr bool value = decltype(f<T>(nullptr))::value;
    };

    template <typename Iterator>
    std::unique_ptr<si::Program> _parse(Context const& ctx, Iterator start, Iterator end) {
        namespace qi = boost::spirit::qi;

        Iterator it{start};
        grammars::Program<decltype(start)> parser;
        si::Program* result{nullptr};

        if (qi::phrase_parse(it, end, parser(&ctx), qi::space, result)) {
            if (it == end) {
                return std::unique_ptr<si::Program>{result};
            }
            delete result;
        }
        throw ls::parsing_error{std::distance(start, it)};
    }

    template <typename T, typename std::enable_if_t<has_begin<T>::value>* = nullptr>
    std::unique_ptr<si::Program> compile(Context const& ctx, T const& input) {
        return _parse(ctx, std::begin(input), std::end(input));
    }

    template <size_t N>
    std::unique_ptr<si::Program> compile(Context const& ctx, char const (&input)[N]) {
        return _parse(ctx, input, input+N-1);
    }

    std::unique_ptr<si::Program> compile(Context const&, std::string const&);
    std::unique_ptr<si::Program> compile(Context const&, std::istream&);
}}}
