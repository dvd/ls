#include "streams.hpp"
#include <ls/sass_script/instructions/base.hpp>

namespace ls { namespace sass_script { namespace compiler {

using instructions::InstructionRunner;

CharStream::CharStream() : _eof{true} {}
CharStream::CharStream(InstructionRunner* r) : _runner{r} {
    increment();
}

void CharStream::increment() {
    if (_eof) {
        return;
    }
    if (!_buffer || _position >= _buffer->size()) {
        _buffer = _runner->step();
        _position = 0;
    }
    if (_buffer) {
        _c = (*_buffer)[_position++];
    }
    else {
        _eof = true;
    }
}

bool CharStream::equal(CharStream const& other) const {
    if (_eof) {
        return other._eof;
    }
    return _runner == other._runner && _buffer == other._buffer && _position == other._position;
}

char CharStream::dereference() const {
    return _c;
}

}}}
