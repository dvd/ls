#include "context.hpp"
#include <ls/sass_script/library.hpp>

namespace ls { namespace sass_script { namespace compiler {

using std::string;
using registry_t = Context::registry_t;

se::WrappedFunction const& Context::function(string name) const {
    return _registry.at(name);
}

Context& Context::function(string name, se::WrappedFunction f) {
    _registry.emplace(name, se::WrappedFunction{f});
    return *this;
}

registry_t const& Context::registry() const {
    return _registry;
}

Context& Context::registry(registry_t const& r) {
    _registry = r;
    return *this;
}

Context& Context::registry(registry_t&& r) {
    _registry = std::move(r);
    return *this;
}

Context default_context() {
    Context ctx;

    ctx.registry(library::registry());
    return ctx;
}

}}}
