#pragma once
#define BOOST_RESULT_OF_USE_DECLTYPE
#define BOOST_SPIRIT_USE_PHOENIX_V3
#include <memory>
#include <string>
#include <boost/iterator/iterator_facade.hpp>
#include <boost/spirit/home/support.hpp>
#include <boost/spirit/home/support/multi_pass.hpp>

namespace ls { namespace sass_script { namespace instructions {
    class InstructionRunner;
}}}

namespace ls { namespace sass_script { namespace compiler {

    /*
     * CharStream is a `Single Pass Iterator` that consumes an
     * InstructionRunner a char at time.
     */
    class CharStream : public boost::iterator_facade<
        CharStream, char, boost::single_pass_traversal_tag, char> {
        public:
            CharStream();
            explicit CharStream(instructions::InstructionRunner*);

        private:
            instructions::InstructionRunner* _runner{nullptr};
            std::string const* _buffer{nullptr};
            size_t _position{0};
            char _c;
            bool _eof{false};
        private:
            friend class boost::iterator_core_access;
            void increment();
            bool equal(CharStream const& other) const;
            char dereference() const;
    };

    /*
     * Wraps a CharStream so it is be ready to be consumed by spirit
     */
    using CharStreamMultipass = boost::spirit::multi_pass<
        CharStream,
        boost::spirit::iterator_policies::default_policy<
            boost::spirit::iterator_policies::ref_counted,
            boost::spirit::iterator_policies::no_check,
            boost::spirit::iterator_policies::buffering_input_iterator,
            boost::spirit::iterator_policies::split_std_deque>>;
}}}
