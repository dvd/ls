#pragma once
#include "base.hpp"
#include "data.hpp"
#include <ls/sass_script/expressions.hpp>
#include <ls/goodies/qi_introspection.hpp>

namespace ls { namespace sass_script { namespace grammars {
    namespace se = ls::sass_script::expression;

    template <typename Iterator, typename Skipper=qi::space_type>
    struct LoadConst : qi::grammar<Iterator, se::LoadConst*(), Skipper> {

        qi::rule<Iterator, se::LoadConst*(), Skipper> start;

        DataType<Iterator, Skipper> data_type;

        LoadConst() : LoadConst::base_type(start, "load_const") {
            using namespace qi;
            auto handler = [](sd::DataType* d) {
                return new se::LoadConst(std::unique_ptr<sd::DataType>{d});
            };
            start = data_type[_val = phx::bind(handler, _1)];
        }
    };

    template <typename Iterator, typename Skipper=qi::space_type>
    struct LoadVariable : qi::grammar<Iterator, se::LoadVariable*(), Skipper> {

        qi::rule<Iterator, se::LoadVariable*(), Skipper> start;

        VariableName<Iterator> name;

        LoadVariable() : LoadVariable::base_type(start, "load_variable") {
            using namespace qi;
            auto handler = [](std::string name) {
                return new se::LoadVariable(name);
            };
            start = name[_val = phx::bind(handler, _1)];
        }
    };

    // a#{$q}
    template <typename, typename>
    struct Expression;

    template <typename Iterator, typename Skipper=qi::space_type>
    struct Interpolation : qi::grammar<Iterator, se::Expression*(), Skipper> {

        qi::rule<Iterator, se::Expression*(), Skipper> start;

        //Expression<Iterator, Skipper> expression;

        Interpolation() : Interpolation::base_type(start, "interpolation") {
            using namespace qi;
            auto handler = [](auto) {
                return new se::LoadConst(std::make_unique<sd::String>("foo"));
            };
            start = (
                lit('#')
                >> '{'
                >> char_('a')
                >> '}')[_val = phx::bind(handler, _1)];
        }
    };

    template <typename Iterator, typename Skipper=qi::space_type>
    struct InterpolatedUnquotedString : qi::grammar<Iterator, se::Expression*(compiler::Context const*), Skipper> {

        qi::rule<Iterator, se::Expression*(compiler::Context const*), Skipper> start;

        qi::rule<Iterator, se::Expression*(), Skipper> string;
        Interpolation<Iterator, Skipper> interpolation;
        qi::rule<Iterator, se::Expression*(), Skipper> expr;


        InterpolatedUnquotedString() : InterpolatedUnquotedString::base_type(start, "interpolated_unquoted_string") {
            using namespace qi;

            auto handle_string = [](std::string s) {
                return new se::LoadConst(std::make_unique<sd::String>(s));
            };
            string = as_string[
                +(char_ - char_("$\"'{}';:") - "#{")
            ][_val = phx::bind(handle_string, _1)];

            expr = (string | interpolation);

            auto handle_expressions = [](compiler::Context const* ctx, std::vector<se::Expression*> exprs) {
                if (exprs.size() == 1) {
                    return exprs[0];
                }

                using ptr = std::unique_ptr<se::Expression>;

                auto& add = ctx->function("_add");

                se::Expression* expr0 = exprs[0];
                for (size_t ix=1; ix<exprs.size(); ix++) {
                    auto exprN = new se::CallFunction(add);

                    auto& inst = exprN->instance();
                    inst.push_back(ptr{expr0});
                    inst.push_back(ptr{exprs[ix]});

                    expr0 = exprN;
                }

                return expr0;
            };
            start = (+expr)[_val = phx::bind(handle_expressions, _r1, _1)];
        }
    };

    template <typename Iterator, typename Skipper=qi::space_type>
    struct Expression : qi::grammar<Iterator, se::Expression*(compiler::Context const*), Skipper> {

        qi::rule<Iterator, se::Expression*(compiler::Context const*), Skipper> start;

        LoadConst<Iterator, Skipper> load_const;
        LoadVariable<Iterator, Skipper> load_variable;
        InterpolatedUnquotedString<Iterator, Skipper> interpolated_string;

        Expression() : Expression::base_type(start, "expression") {
            start = interpolated_string(qi::_r1)
                | load_variable
                | load_const;
        }
    };
}}}
