#pragma once
#define BOOST_RESULT_OF_USE_DECLTYPE
#define BOOST_SPIRIT_USE_PHOENIX_V3
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include <ls/sass_script/compiler/context.hpp>

namespace ls { namespace sass_script { namespace grammars {
    namespace qi = boost::spirit::qi;
    namespace phx = boost::phoenix;

    template <typename Iterator>
    struct Symbol : qi::grammar<Iterator, std::string()> {

        qi::rule<Iterator, std::string()> start;

        Symbol() : Symbol::base_type(start, "symbol") {
            start = qi::alpha >> *qi::alnum;
            start.name("symbol");
        }
    };

    template <typename Iterator>
    struct VariableName : qi::grammar<Iterator, std::string()> {

        qi::rule<Iterator, std::string()> start;
        Symbol<Iterator> symbol;

        VariableName() : VariableName::base_type(start, "variable_name") {
            start = qi::lit('$') >> symbol;
            start.name("variable_name");
        }
    };
}}}

