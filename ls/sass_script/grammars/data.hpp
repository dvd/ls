#pragma once
#include "base.hpp"
#include <ls/sass_script/data.hpp>

namespace ls { namespace sass_script { namespace grammars {

    namespace sd = ls::sass_script::data;

    template <typename Iterator, typename Skipper=qi::space_type>
    struct UnquotedString : qi::grammar<Iterator, sd::String*(), Skipper> {

        template <typename T>
        using rule_t = qi::rule<Iterator, T, Skipper>;

        rule_t<sd::String*()> start;

        UnquotedString() : UnquotedString::base_type(start, "unquoted_string") {
            using namespace qi;
            start = as_string[
                lexeme[
                    +(char_ - char_("\"'{};"))
                ]
            ][_val = phx::new_<sd::String>(_1)];
        }
    };

    template <typename Iterator, typename Skipper=qi::space_type>
    struct QuotedString : qi::grammar<Iterator, sd::String*(), Skipper> {

        template <typename T>
        using rule_t = qi::rule<Iterator, T, Skipper>;

        rule_t<sd::String*()> start;

        QuotedString() : QuotedString::base_type(start, "quoted_string") {
            using namespace qi;
            start = as_string[
                lexeme[
                    char_('"') >> *(char_ - char_('"')) >> char_('"')
                ]
            ][_val = phx::new_<sd::String>(_1)];
        }
    };

    template <typename Iterator, typename Skipper=qi::space_type>
    struct DataType : qi::grammar<Iterator, sd::DataType*(), Skipper> {

        qi::rule<Iterator, sd::DataType*(), Skipper> start;

        QuotedString<Iterator, Skipper> quoted_string;
        UnquotedString<Iterator, Skipper> unquoted_string;

        DataType() : DataType::base_type(start, "data_type") {
            start = unquoted_string
                | quoted_string;
        }
    };

}}}
