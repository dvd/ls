#pragma once
#include "base.hpp"
#include "data.hpp"
#include "expressions.hpp"
#include <ls/sass_script/instructions.hpp>
#include <ls/goodies/qi_introspection.hpp>

namespace ls { namespace sass_script { namespace grammars {
    namespace si = ls::sass_script::instructions;

    template <typename Iterator, typename Skipper>
    using PropertyName = InterpolatedUnquotedString<Iterator, Skipper>;

    template <typename Iterator, typename Skipper=qi::space_type>
    struct StoreVariable : qi::grammar<Iterator, si::StoreVariable*(compiler::Context const*), Skipper> {

        qi::rule<Iterator, si::StoreVariable*(compiler::Context const*), Skipper> start;

        VariableName<Iterator> variable_name;
        Expression<Iterator, Skipper> expression;

        StoreVariable() : StoreVariable::base_type(start, "store_variable") {
            using namespace qi;
            auto handler = [](std::string const& var, se::Expression* expr) {
                return new si::StoreVariable(var, std::unique_ptr<se::Expression>{expr});
            };
            start = (variable_name
                >> ':'
                >> expression(_r1)
                >> ';')[_val = phx::bind(handler, _1, _2)];
        }
    };

    template <typename Iterator, typename Skipper=qi::space_type>
    struct EmitProperty : qi::grammar<Iterator, si::EmitProperty*(compiler::Context const*), Skipper> {

        qi::rule<Iterator, si::EmitProperty*(compiler::Context const*), Skipper> start;

        PropertyName<Iterator, Skipper> property_name;
        Expression<Iterator, Skipper> property_value;

        EmitProperty() : EmitProperty::base_type(start, "emit_property") {
            using namespace qi;
            auto handler = [](se::Expression* name, se::Expression* value) {
                using uptr = std::unique_ptr<se::Expression>;
                return new si::EmitProperty(uptr{name}, uptr{value});
            };
            start = (property_name(_r1)
                >> ':'
                >> property_value(_r1)
                >> ';')[_val = phx::bind(handler, _1, _2)];
        }
    };

    template <typename Iterator, typename Skipper=qi::space_type>
    struct For : qi::grammar<Iterator, si::Instruction*(compiler::Context const*), Skipper> {

        qi::rule<Iterator, si::Instruction*(compiler::Context const*), Skipper> start;
        qi::rule<Iterator, si::Instruction*(compiler::Context const*), Skipper, qi::locals<bool>> for_;
        VariableName<Iterator> variable_name;

        template <typename Grammar>
        For(Grammar sub_grammar) : For::base_type(start, "for") {
            using namespace qi;
            auto handler = [](std::string const& name, int from, int to, bool closed, std::vector<si::Instruction*> seq) -> si::Instruction* {
                using uptr = std::unique_ptr<si::Instruction>;
                auto p = new si::For(name, from, to, closed);

                for (auto i : seq) {
                    p->push_back(uptr{i});
                }

                return p;
            };
            for_ = (lit("@for")
                >> variable_name
                >> "from"
                >> int_
                >> (
                    lit("to")[_a = false]
                    | lit("through")[_a = true])
                >> int_
                >> '{'
                >> +sub_grammar
                >> '}')[qi::_val = phx::bind(handler, _1, _2, _3, _a, _4)];
            for_.name("for");

            start = for_(_r1);
        }
    };

    template <typename Iterator, typename Skipper=qi::space_type>
    struct AllowedInstructionInBlock : qi::grammar<Iterator, si::Instruction*(compiler::Context const*), Skipper> {

        qi::rule<Iterator, si::Instruction*(compiler::Context const*), Skipper> start;

        StoreVariable<Iterator, Skipper> store_variable;
        EmitProperty<Iterator, Skipper> emit_property;

        AllowedInstructionInBlock() : AllowedInstructionInBlock::base_type(start, "allowed_instruction_in_block") {
            using namespace qi;
            start = (
                store_variable(_r1)
                | emit_property(_r1));
        }
    };

    template <typename Iterator, typename Skipper=qi::space_type>
    struct EmitBlock : qi::grammar<Iterator, si::Instruction*(compiler::Context const*), Skipper> {

        qi::rule<Iterator, si::Instruction*(compiler::Context const*), Skipper> start;

        PropertyName<Iterator, Skipper> block_name;
        AllowedInstructionInBlock<Iterator, Skipper> instruction;
        For<Iterator, Skipper> for_;

        EmitBlock() : EmitBlock::base_type(start, "emit_block"), for_{instruction(qi::_r1) | start(qi::_r1)} {
            using namespace qi;
            auto handler = [](se::Expression* name, std::vector<si::Instruction*> seq) {
                using uptr = std::unique_ptr<si::Instruction>;
                auto p = new si::EmitBlock(std::unique_ptr<se::Expression>{name});

                for (auto i : seq) {
                    p->push_back(uptr{i});
                }

                return p;
            };
            start = (block_name(_r1)
                >> '{'
                >> *(
                    instruction(_r1)
                    | start(_r1)
                    | for_(_r1)
                    )
                >> '}')[_val = phx::bind(handler, _1, _2)];
        }
    };


    template <typename Iterator, typename Skipper=qi::space_type>
    struct TopLevelInstruction : qi::grammar<Iterator, si::Instruction*(compiler::Context const*), Skipper> {

        qi::rule<Iterator, si::Instruction*(compiler::Context const*), Skipper> start;

        StoreVariable<Iterator, Skipper> store_variable;
        EmitBlock<Iterator, Skipper> emit_block;

        TopLevelInstruction() : TopLevelInstruction::base_type(start, "top_level_instruction") {
            using namespace qi;
            start = store_variable(_r1)
                | emit_block(_r1);
        }
    };


    template <typename Iterator, typename Skipper=qi::space_type>
    struct Program : qi::grammar<Iterator, si::Program*(compiler::Context const*), Skipper> {

        qi::rule<Iterator, si::Program*(compiler::Context const*), Skipper> start;

        TopLevelInstruction<Iterator, Skipper> instruction;

        Program() : Program::base_type(start, "program") {
            using namespace qi;

            auto handler = [](std::vector<si::Instruction*> seq) {
                using ptr = std::unique_ptr<si::Instruction>;

                auto p = new si::Program();
                for (auto& i : seq) {
                    p->push_back(ptr{i});
                }

                return p;
            };
            start = (*instruction(_r1))[_val = phx::bind(handler, _1)];
        }
    };
}}}

