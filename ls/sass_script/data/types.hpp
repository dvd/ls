#pragma once
#include "color.hpp"
#include "string.hpp"
#include <ls/meta.hpp>
#include <typeinfo>

namespace ls { namespace sass_script { namespace data {

    using Types = metatype::List<Color, String>;

    /*
     * Calls the visitor with a reference to the DataType casted to the correct
     * type.
     *
     * The DataType is a const reference and consequently the visitor
     * must accept a const reference to the casted type.
     *
     * resolve(p, [](auto const& t) {
     *      // here decltype(t) is one of the subclasses of DataType
     * });
     */
    template <typename Func>
    void resolve(data::DataType const& v, Func const& visitor) {
        metatype::resolve<Types>(v, visitor);
    }
}}}
