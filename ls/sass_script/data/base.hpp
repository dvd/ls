#pragma once
#include <ls/repr.hpp>
namespace ls { namespace sass_script { namespace data {
    class DataType : public Repr {
        public:
            virtual ~DataType() = default;
            virtual std::string const& as_string() const = 0;
    };

}}}
