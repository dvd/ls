#include "string.hpp"
#include <ls/strings.hpp>

namespace ls { namespace sass_script { namespace data {

using std::string;

String::String(string s) : _data{std::move(s)} {}

string const& String::as_string() const {
    return _data;
}

string String::repr() const {
    return supplant("String{%}", _data);
}

String String::operator+(String const& other) const {
    return String{_data + other._data};
}

}}}
