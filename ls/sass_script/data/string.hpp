#pragma once
#include <string>
#include "base.hpp"

namespace ls { namespace sass_script { namespace data {

    class String : public DataType {
        std::string _data;
        public:
            String(std::string s);

            std::string repr() const override;
            std::string const& as_string() const override;

            String operator+(String const&) const;
    };

}}}
