#pragma once
#include <stdexcept>
#include <string>

namespace ls { namespace sass_script { namespace data {
    class variable_not_found : public std::runtime_error {
        using std::runtime_error::runtime_error;
    };

    class type_error : public std::runtime_error {
        using std::runtime_error::runtime_error;
    };
}}}
