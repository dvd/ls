#include "scope.hpp"
#include <algorithm>

namespace ls { namespace sass_script { namespace data {
    using std::string;
    using std::unique_ptr;

    DataType& IScope::get(string const& name) const {
        DataType* v = find(name);
        if (v == nullptr) {
            throw variable_not_found{name};
        }
        return *v;
    }

    Scope::entry const* Scope::find_variable(string const& name) const {
        auto end = storage.end();
        auto it = std::find_if(
            storage.begin(), end,
            [&name](Scope::entry const& e) { return std::get<0>(e) == name; });

        return it == end ? nullptr : &(*it);
    }

    Scope::entry* Scope::find_variable(string const& name) {
        return const_cast<entry*>(
            static_cast<Scope const&>(*this).find_variable(name));
    }

    DataType* Scope::find(string const& name) const {
        auto found = find_variable(name);
        return found ? std::get<1>(*found).get() : nullptr;
    }

    unique_ptr<DataType> Scope::set(string const& name, unique_ptr<DataType> v) {
        auto found = find_variable(name);
        if (found) {
            std::swap(std::get<1>(*found), v);
        }
        else {
            storage.push_back(entry{name, move(v)});
        }
        return v;
    }

    DataType* Scope::add(unique_ptr<DataType> v) {
        auto raw = v.get();
        anonymous_storage.push_back(move(v));
        return raw;
    }

    void Scope::clear() {
        storage.clear();
        anonymous_storage.clear();
    }

    ChainedScope::ChainedScope(IScope& parent, IScope& scope)
        : parent{parent}, scope{scope} {}

    DataType* ChainedScope::find(string const& name) const {
        auto pointer = scope.find(name);
        if (!pointer) {
            pointer = parent.find(name);
        }
        return pointer;
    }

    unique_ptr<DataType> ChainedScope::set(string const& name, unique_ptr<DataType> v) {
        if (scope.find(name) || !parent.find(name)) {
            return scope.set(name, move(v));
        }
        else {
            return parent.set(name, move(v));
        }
    }

    DataType* ChainedScope::add(unique_ptr<DataType> v) {
        return scope.add(move(v));
    }
}}}
