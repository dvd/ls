#pragma once
#include "base.hpp"
#include <cstdint>

namespace ls { namespace sass_script { namespace data {

    struct Color : DataType {
        uint8_t const red{0};
        uint8_t const green{0};
        uint8_t const blue{0};
        uint8_t const alpha{0};

        std::string _value;

        std::string repr() const override;
        std::string const& as_string() const override;
    };

}}}
