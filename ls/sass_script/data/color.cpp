#include "color.hpp"
#include <ls/strings.hpp>

namespace ls { namespace sass_script { namespace data {

using std::string;

string Color::repr() const {
    return supplant(
        "Color{%,%,%,%}",
        static_cast<int>(red),
        static_cast<int>(green),
        static_cast<int>(blue),
        static_cast<int>(alpha));
}

string const& Color::as_string() const {
    return _value;
}

}}}

