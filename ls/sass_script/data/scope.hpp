#pragma once
#include "base.hpp"
#include "exceptions.hpp"
#include <memory>
#include <string>
#include <tuple>
#include <vector>

namespace ls { namespace sass_script { namespace data {
    class ChainedScope;

    /*
     * IScope is the interface for the scope concept.
     *
     * A scope is used to store and retrieve variable by name, the scope takes
     * the ownership of the variables it handles.
     */
    class IScope {
        public:
            // Adds a new variables to this scope, returns a unique_ptr to the
            // previous DataType associated with the same name (if any, an
            // empty unique_ptr otherwhise).
            virtual std::unique_ptr<DataType> set(std::string const&, std::unique_ptr<DataType>) = 0;

            // Adds a new anonymous variable to this scope. The variable cannot
            // be retrieved by name and can be only used through the pointer
            // returned by this method.
            virtual DataType* add(std::unique_ptr<DataType>) = 0;

            // check if a variable exists, returns nullptr if not found.
            virtual DataType* find(std::string const& name) const = 0;

            // returns a variable by name or raise an exception if not found.
            DataType& get(std::string const& name) const;

            // construct a new variable directly into the scope.
            // This method is a shortcut for when you need to build a DataType
            // just to add it to this scope
            template <typename T, typename... Args>
            T* emplace(std::string const& name, Args&&... args) {
                auto p = std::make_unique<T>(args...);
                T* raw = p.get();
                set(name, std::move(p));
                return raw;
            }

            // Another helper function.
            //
            // Search a variable by name and perform a dynamic_cast on the
            // found pointer to the desired type.
            //
            // Returns a reference to the variable.
            //
            // An exception is thrown if the variable is not found or if it
            // cannot be casted to the desired type.
            template <typename T>
            T& var(std::string const& name) const {
                DataType& v = get(name);
                try {
                    return dynamic_cast<T&>(v);
                } catch(std::bad_cast) {
                    throw type_error{name};
                }
            }
    };

    class Scope : public IScope {
        using entry = std::tuple<std::string, std::unique_ptr<DataType>>;
        std::vector<entry> storage;
        std::vector<std::unique_ptr<DataType>> anonymous_storage;

        protected:
            entry const* find_variable(std::string const&) const;
            entry* find_variable(std::string const&);

        public:
            std::unique_ptr<DataType> set(std::string const&, std::unique_ptr<DataType>) override;
            DataType* add(std::unique_ptr<DataType>) override;
            DataType* find(std::string const& name) const override;
            void clear();
    };

    /*
     * Links two scopes together and implements the rules of visibility between
     * them.
     */
    class ChainedScope : public IScope {
        IScope& parent;
        IScope& scope;

        public:
            ChainedScope(IScope&, IScope&);
            std::unique_ptr<DataType> set(std::string const&, std::unique_ptr<DataType>) override;
            DataType* add(std::unique_ptr<DataType>) override;
            DataType* find(std::string const& name) const override;
    };
}}}
