#pragma once
#include <string>
#include <ls/sass_script/instructions/instructions.hpp>
#include <ls/sass/directives/block/traverse.hpp>

namespace ls { namespace sass_script { namespace serializers {
    namespace si = ls::sass_script::instructions;

    template <typename Stream>
    class AsciiTree : public sass::directives::TreeVisitor {
        Stream& stream;
        std::string indent = "";

        template <typename U>
        Stream& write(U o) {
            stream << indent << o << std::endl;
            return stream;
        }

        public:
            AsciiTree(Stream& stream) : stream{stream} {};

            template <typename I>
            void dump(I const& i) {
                instructions::traverse_tree(i, *this);
            }

        public:
            using sass::directives::TreeVisitor::leave;

            template <typename T, typename std::enable_if_t<si::has_instructions<T>::value>* = nullptr>
            bool enter(T const& i) {
                write("+ " + i.repr());
                indent.append("  ");
                return false;
            }

            template <typename T, typename std::enable_if_t<si::has_instructions<T>::value>* = nullptr>
            bool leave(T const&) {
                indent.resize(indent.size() - 2);
                return false;
            }

            template <typename T, typename std::enable_if_t<!si::has_instructions<T>::value>* = nullptr>
            bool enter(T const& i) {
                write("- " + i.repr());
                return false;
            }
    };
}}}

