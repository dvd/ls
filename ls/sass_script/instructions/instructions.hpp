#pragma once
#include "echo.hpp"
#include "emit_block.hpp"
#include "emit_property.hpp"
#include "for.hpp"
#include "program.hpp"
#include "store_variable.hpp"
#include <ls/meta.hpp>

namespace ls { namespace sass_script { namespace instructions {

    using Instructions = metatype::List<Echo, EmitBlock, EmitProperty, StoreVariable, Program, For>;

    template <typename Func>
    void resolve(Instruction const& v, Func const& visitor) {
        metatype::resolve<Instructions>(v, visitor);
    }

    template <typename T>
    struct has_instructions {
        template <typename U>
        static constexpr std::true_type f(decltype(std::declval<U const>().instructions())*);

        template <typename>
        static constexpr std::false_type f(...);

        static constexpr bool value = decltype(f<T>(nullptr))::value;
    };

}}}

