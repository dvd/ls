#include "echo.hpp"
#include <ls/sass_script/data/base.hpp>
#include <ls/sass_script/expressions/expression.hpp>
#include <ls/strings.hpp>

namespace ls { namespace sass_script { namespace instructions {
    using std::string;
    using std::unique_ptr;

    Echo::Echo(unique_ptr<expression::Expression> e) : _expr{std::move(e)} {}
    Echo::Echo(data::DataType const* v) : Echo(v->as_string()) {}
    Echo::Echo(string v) : _expr{nullptr}, _data{std::move(v)} {}

    unique_ptr<InstructionRunner> Echo::runner(data::IScope& scope) const {
        string const* p;
        if (_expr) {
            p = &_expr->evaluate(scope)->as_string();
        }
        else {
            p = &_data;
        }
        return std::make_unique<runners::Echo>(p);
    }

    string Echo::repr() const {
        return "I:Echo()";
    }

    namespace runners {
        Echo::Echo(string const* s) : _pointer{s} {}
        Echo::Echo(string d) : _data{std::move(d)}, _pointer{&_data} {}
        Echo::Echo(data::DataType const* d) : _data{d->as_string()}, _pointer{&_data} {}

        string const* Echo::step() {
            string const* output = nullptr;
            if (_pointer) {
                std::swap(output, _pointer);
            }
            return output;
        }
    }

}}}
