#include <iostream>
#include "for.hpp"
#include <ls/sass_script/data/string.hpp>
#include <ls/strings.hpp>

using namespace std;

namespace ls { namespace sass_script { namespace instructions {

using std::string;
using std::unique_ptr;

For::For(string name, int from, int to, bool closed)
    : _name{name}, _from{from}, _to{to}, _closed_range{closed} {}

string For::repr() const {
    string range;
    if (_closed_range) {
        range = supplant("[%:%]", _from, _to);
    }
    else {
        range = supplant("[%:%)", _from, _to);
    }
    return supplant("I:For(% -> %)", _name, range);
}

namespace {
    struct Runner : InstructionRunner {
        string variable;
        int from;
        int to;
        int loop_step;
        data::IScope& root_scope;
        std::vector<unique_ptr<Instruction>> const& instructions;

        unique_ptr<InstructionRunner> child_runner{nullptr};
        data::Scope inernal_scope;
        data::ChainedScope child_scope;

        Runner(string variable, int from, int to, int step, data::IScope& scope, std::vector<unique_ptr<Instruction>> const& instructions)
            : variable{variable}, from{from}, to{to+step}, loop_step{step}, root_scope{scope}, instructions{instructions}, child_scope{scope, inernal_scope} {
        }

        string const* step() override {
            string const* value = nullptr;
            while (!value && from != to) {
                if (child_runner) {
                    value = child_runner->step();
                }

                if (!value) {
                    inernal_scope.clear();
                    // XXX switch to a data::Number ASAP
                    inernal_scope.emplace<data::String>(variable, std::to_string(from));
                    child_runner = std::make_unique<runners::ProgramRunner>(child_scope, instructions);
                    from += loop_step;
                }
            }

            return value;
        }
    };
}

unique_ptr<InstructionRunner> For::runner(data::IScope& scope) const {
    int start{_from};
    int end{_to};
    int step = start < end ? 1 : -1;
    if (_closed_range) {
        end += step;
    }
    return std::make_unique<Runner>(_name, start, end, step, scope, _instructions);
}

}}}
