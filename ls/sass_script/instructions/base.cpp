#include <iostream>
#include "base.hpp"
#include "echo.hpp"

namespace ls { namespace sass_script { namespace instructions {
    using std::string;

    Instruction::~Instruction() = default;
    InstructionRunner::~InstructionRunner() = default;

    namespace runners {
        using std::vector;
        using std::unique_ptr;

        string const* NoOp::step() {
            return nullptr;
        }

        LinkedRunners::LinkedRunners() {}
        LinkedRunners::LinkedRunners(vector<unique_ptr<InstructionRunner>> r) : _runners{std::move(r)} {}

        void LinkedRunners::push_back(std::unique_ptr<InstructionRunner> r) {
            _runners.push_back(std::move(r));
        }

        void LinkedRunners::push_back(data::DataType const* d) {
            push_back(std::make_unique<runners::Echo>(d));
        }

        void LinkedRunners::push_back(string s) {
            push_back(std::make_unique<runners::Echo>(std::move(s)));
        }

        string const* LinkedRunners::step() {
            string const* output = nullptr;
            while (!output && ip < _runners.size()) {
                if (!_current) {
                    _current = _runners[ip].get();
                }
                output = _current->step();
                if (!output) {
                    _current = nullptr;
                    ++ip;
                }
            }
            return output;
        }
    }
}}}

