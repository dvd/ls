#pragma once
#include "program.hpp"

namespace ls { namespace sass_script { namespace instructions {
    class EmitBlock : public _Program {
        std::unique_ptr<expression::Expression> _name;

        public:
            EmitBlock(std::unique_ptr<expression::Expression>);

            std::unique_ptr<InstructionRunner> runner(data::IScope&) const override;
            std::string repr() const override;
    };

}}}


