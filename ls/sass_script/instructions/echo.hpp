#pragma once
#include "base.hpp"
#include <string>

namespace ls { namespace sass_script { namespace instructions {
    class Echo : public Instruction {
        std::unique_ptr<expression::Expression> _expr;
        std::string _data{""};

        public:
            Echo(std::unique_ptr<expression::Expression>);
            Echo(data::DataType const*);
            Echo(std::string);

            std::unique_ptr<InstructionRunner> runner(data::IScope&) const override;
            std::string repr() const override;
    };

    namespace runners {
        class Echo : public InstructionRunner {
            std::string _data;
            std::string const* _pointer;
            public:
                Echo(std::string const*);
                Echo(std::string);
                Echo(data::DataType const*);
                std::string const* step() override;
        };
    }
}}}
