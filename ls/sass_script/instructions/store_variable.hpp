#pragma once
#include "base.hpp"
#include <string>

namespace ls { namespace sass_script { namespace instructions {
    class StoreVariable : public Instruction {
        std::string _name;
        std::unique_ptr<expression::Expression> _expr;

        public:
            StoreVariable(std::string, std::unique_ptr<expression::Expression>);

            std::unique_ptr<InstructionRunner> runner(data::IScope&) const override;
            std::string repr() const override;
    };

}}}

