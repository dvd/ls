#include "store_variable.hpp"
#include <ls/sass_script/data/scope.hpp>
#include <ls/sass_script/expressions/expression.hpp>
#include <ls/sass_script/library/internal.hpp>
#include <ls/strings.hpp>

namespace ls { namespace sass_script { namespace instructions {
    using std::move;
    using std::unique_ptr;
    using std::string;

    StoreVariable::StoreVariable(string n, unique_ptr<expression::Expression> e)
        : _name{move(n)}, _expr{move(e)} {}

    unique_ptr<InstructionRunner> StoreVariable::runner(data::IScope& scope) const {
        // TODO: if it's possible to detect if the DataType returned from
        // _expr->evaluate is an anonymous one, we can skip the copy.
        auto value = library::copy(*(_expr->evaluate(scope)));
        scope.set(_name, move(value));
        return std::make_unique<runners::NoOp>();
    }

    std::string StoreVariable::repr() const {
        return supplant("I:StoreVariable(%, %)", _name, _expr->repr());
    }
}}}
