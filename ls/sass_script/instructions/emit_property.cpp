#include "emit_property.hpp"
#include "echo.hpp"
#include <ls/sass_script/expressions/expression.hpp>
#include <ls/strings.hpp>

namespace ls { namespace sass_script { namespace instructions {
    using std::unique_ptr;
    using expression::Expression;

    EmitProperty::EmitProperty(unique_ptr<Expression> n, unique_ptr<Expression> v)
        : _name{std::move(n)}, _value{std::move(v)} {}

    unique_ptr<InstructionRunner> EmitProperty::runner(data::IScope& scope) const {
        auto r = std::make_unique<runners::LinkedRunners>();
        r->push_back(_name->evaluate(scope));
        r->push_back(":");
        r->push_back(_value->evaluate(scope));
        r->push_back(";");
        return std::move(r);
    }

    std::string EmitProperty::repr() const {
        return supplant("I:EmitProperty(%, %)", _name->repr(), _value->repr());
    }

}}}
