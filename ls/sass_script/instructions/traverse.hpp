#include "instructions.hpp"
#include <ls/types.hpp>

namespace ls { namespace sass_script { namespace instructions {

    template <typename UserVisitor>
    class InstructionTreeTraversal {
        UserVisitor& _visitor;

        public:
            InstructionTreeTraversal(UserVisitor& v) : _visitor{v} {}

            template <typename T>
            using has_instructions = instructions::has_instructions<T>;

            template <typename T, typename std::enable_if_t<has_instructions<T>::value>* = nullptr>
            bool operator()(T const& i) const {
                if (_visitor.enter(i)) {
                    return true;
                }
                auto f = [this](auto& i){ return this->operator()(i); };
                for (auto ptr : i.instructions()) {
                    instructions::resolve(*ptr, f);
                }
                return _visitor.leave(i);
            }

            template <typename T, typename std::enable_if_t<!has_instructions<T>::value>* = nullptr>
            bool operator()(T const& i) const {
                return _visitor.enter(i) || _visitor.leave(i);
            }
    };

    template<typename Instruction, typename UserVisitor>
    bool traverse_tree(Instruction const& i, UserVisitor& visitor) {
        return InstructionTreeTraversal<UserVisitor>(visitor)(i);
    }
}}}
