#pragma once
#include "base.hpp"

namespace ls { namespace sass_script { namespace instructions {
    class EmitProperty : public Instruction {
        std::unique_ptr<expression::Expression> _name;
        std::unique_ptr<expression::Expression> _value;

        public:
            EmitProperty(std::unique_ptr<expression::Expression>, std::unique_ptr<expression::Expression>);

            std::unique_ptr<InstructionRunner> runner(data::IScope&) const override;
            std::string repr() const override;
    };

}}}



