#pragma once
#include <string>
#include <memory>
#include <vector>
#include <ls/repr.hpp>

namespace ls { namespace sass_script { namespace data {
    class IScope;
    class DataType;
}}}

namespace ls { namespace sass_script { namespace expression {
    class Expression;
}}}

namespace ls { namespace sass_script { namespace instructions {
    class InstructionRunner;

    class Instruction : public ls::Repr {
        public:
            virtual ~Instruction();
            virtual std::unique_ptr<InstructionRunner> runner(data::IScope&) const = 0;
    };

    class InstructionRunner {
        public:
            virtual ~InstructionRunner();
            virtual std::string const* step() = 0;
    };

    namespace runners {
        class NoOp : public InstructionRunner {
            public:
                std::string const* step() override;
        };

        class LinkedRunners : public InstructionRunner {
            size_t ip{0};
            std::vector<std::unique_ptr<InstructionRunner>> _runners;
            InstructionRunner* _current{nullptr};
            public:
                LinkedRunners();
                LinkedRunners(std::vector<std::unique_ptr<InstructionRunner>>);

                void push_back(std::unique_ptr<InstructionRunner>);
                void push_back(data::DataType const*);
                void push_back(std::string);

                std::string const* step() override;
        };
    }
}}}
