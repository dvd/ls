#pragma once
#include "program.hpp"
#include <string>

namespace ls { namespace sass_script { namespace instructions {
    class For : public _Program {
        std::string _name;
        int _from;
        int _to;
        bool _closed_range;
        public:
            /*
             * Construct a new `For` instruction; the string is the name of the
             * varible that will keep track of the current index, the two ints
             * are the extremes of the range and the bool controls if the range
             * is opened (false) or closed (true)
             */
            For(std::string, int, int, bool);

            std::string repr() const override;

            std::unique_ptr<InstructionRunner> runner(data::IScope&) const override;
    };
}}}
