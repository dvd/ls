#include "emit_block.hpp"
#include <ls/sass_script/expressions/expression.hpp>
#include <ls/strings.hpp>

namespace ls { namespace sass_script { namespace instructions {
    using std::unique_ptr;

    EmitBlock::EmitBlock(unique_ptr<expression::Expression> n) : _name{std::move(n)} {}

    unique_ptr<InstructionRunner> EmitBlock::runner(data::IScope& scope) const {
        auto r = std::make_unique<runners::LinkedRunners>();
        r->push_back(_name->evaluate(scope));
        r->push_back("{");
        r->push_back(_Program::runner(scope));
        r->push_back("}");
        return std::move(r);
    }

    std::string EmitBlock::repr() const {
        return supplant("I:EmitBlock(%)", _name->repr());
    }

}}}



