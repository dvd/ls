#include "program.hpp"
#include <ls/sass_script/expressions/expression.hpp>

namespace ls { namespace sass_script { namespace instructions {
    using std::unique_ptr;
    using std::vector;
    using std::string;

    void _Program::push_back(unique_ptr<Instruction> i) {
        _instructions.push_back(std::move(i));
    }

    vector<Instruction*> _Program::instructions() const {
        vector<Instruction*> output;
        output.reserve(_instructions.size());
        for(auto& i : _instructions) {
            output.push_back(i.get());
        }
        return output;
    }

    unique_ptr<InstructionRunner> _Program::runner(data::IScope& scope) const {
        return std::make_unique<runners::ProgramRunner>(scope, _instructions);
    }

    std::string Program::repr() const {
        return "I:Program()";
    }

    namespace runners {
        ProgramRunner::ProgramRunner(data::IScope& parent_scope, vector<unique_ptr<Instruction>> const& i)
            : scope{parent_scope, child_scope}, instructions{i} {}

        string const* ProgramRunner::step() {
            string const* value = nullptr;

            while (!value && ip < instructions.size()) {
                if (!runner) {
                    runner = instructions[ip]->runner(scope);
                }
                value = runner->step();
                if (!value) {
                    runner = nullptr;
                    ++ip;
                }
            }
            return value;
        }
    }
}}}
