#pragma once
#include "base.hpp"
#include <memory>
#include <vector>
#include <ls/sass_script/data/scope.hpp>

namespace ls { namespace sass_script { namespace instructions {
    class _Program : public Instruction {
        protected:
            std::vector<std::unique_ptr<Instruction>> _instructions;

        public:
            void push_back(std::unique_ptr<Instruction>);

            std::vector<Instruction*> instructions() const;

            template <typename T, typename... Args>
            T* emplace_back(Args&&... args) {
                auto p = std::make_unique<T>(std::forward<Args...>(args...));
                auto raw = p.get();
                push_back(std::move(p));
                return raw;
            }

            std::unique_ptr<InstructionRunner> runner(data::IScope&) const override;
    };
    class Program : public _Program {
        public:
            std::string repr() const override;
    };
    namespace runners {
        class ProgramRunner : public InstructionRunner {
            size_t ip = 0;
            data::Scope child_scope;
            data::ChainedScope scope;
            std::vector<std::unique_ptr<Instruction>> const& instructions;
            std::unique_ptr<InstructionRunner> runner;

            public:
                ProgramRunner(data::IScope&, std::vector<std::unique_ptr<Instruction>> const&);

                std::string const* step() override;
        };
    }
}}}
