#pragma once
#include <string>
#include <unordered_map>
#include <ls/sass_script/expressions/function.hpp>

namespace ls { namespace sass_script { namespace library {

    std::unordered_map<std::string, expression::WrappedFunction> registry();

}}}

