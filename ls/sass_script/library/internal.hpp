#pragma once
#include <memory>
#include <string>

#include <ls/sass_script/data/types.hpp>
#include <ls/sass_script/expressions/function.hpp>

namespace ls { namespace sass_script { namespace library {
    std::unique_ptr<data::DataType> copy(data::DataType const&);

    class Add : public expression::Function<2> {
        public:
            using Function<2>::operator();
            std::unique_ptr<data::DataType> operator()(data::String const&, data::String const&) const;

            expression::Signature const& signature() const override;
    };
}}}
