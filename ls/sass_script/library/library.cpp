#include "library.hpp"
#include "internal.hpp"

namespace ls { namespace sass_script { namespace library {

using std::unordered_map;
using std::string;
using expression::WrappedFunction;

unordered_map<string, WrappedFunction> registry() {
    unordered_map<string, WrappedFunction> reg;

    reg.emplace("_add", wrap_function(Add{}));

    return reg;
}

}}}
