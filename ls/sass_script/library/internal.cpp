#include "internal.hpp"
#include <ls/meta.hpp>

namespace ls { namespace sass_script { namespace library {

using std::unique_ptr;
using expression::Signature;
using expression::required;

unique_ptr<data::DataType> copy(data::DataType const& src) {
    unique_ptr<data::DataType> output;

    data::resolve(src, [&output](auto const& t) {
        using T = std::remove_const_t<std::remove_reference_t<decltype(t)>>;
        output.reset(new T(t));
    });

    return std::move(output);
}

unique_ptr<data::DataType> Add::operator()(data::String const& a, data::String const& b) const {
    return std::make_unique<data::String>(a + b);
}

Signature const& Add::signature() const {
    static Signature sig;
    if (sig.size() == 0) {
        sig.push_back("op1", required);
        sig.push_back("op2", required);
    };
    return sig;
}

}}}
