#pragma once
#include <memory>
#include <string>
#include <vector>
#include "signature.hpp"
#include <ls/repr.hpp>

namespace ls { namespace sass_script { namespace data {
    class IScope;
    class DataType;
}}}

namespace ls { namespace sass_script { namespace expression {
    class Expression : public ls::Repr {
        public:
            virtual ~Expression();
            /*
             * Evaluates this expression against the passed scope and returns
             * a const pointer to a DataType (the result of this expression).
             */
            virtual data::DataType const* evaluate(data::IScope&) const = 0;
    };

    class LoadVariable : public Expression {
        public:
            LoadVariable(std::string);
            data::DataType const* evaluate(data::IScope&) const override;
            std::string repr() const override;
        private:
            std::string name;
    };

    class LoadConst : public Expression {
        public:
            LoadConst(std::unique_ptr<data::DataType>);
            data::DataType const* evaluate(data::IScope&) const override;
            std::string repr() const override;
        private:
            std::unique_ptr<data::DataType> data;
    };

    class WrappedFunction;

    class CallFunction : public Expression {
        public:
            using argument_t = std::unique_ptr<Expression>;

            CallFunction(WrappedFunction const&);
            CallFunction(WrappedFunction const&, std::vector<argument_t>);

            FunctionInstance& instance();
            FunctionInstance const& instance() const;

            data::DataType const* evaluate(data::IScope&) const override;
            std::string repr() const override;
        private:
            WrappedFunction const& function;
            FunctionInstance inst;
    };
}}}
