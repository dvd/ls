#pragma once
#include <memory>
#include <string>
#include <ls/flat_map.hpp>
#include <boost/variant.hpp>

namespace ls { namespace sass_script { namespace data {
    class IScope;
    class DataType;
}}}
namespace ls { namespace sass_script { namespace expression {

    // A type to expose the concept of "required argument"; An instance of this
    // type called "required" is automatically created.
    class required_t {
    };

    extern required_t required;

    // Two type to describe a function signature.
    //
    // A parameter can be required or have a default value (the default value
    // cannot be nullptr)
    //
    // A signature is a container that map a parameter name with its state; if
    // it is required or not and in this case with its default value.
    using Parameter = boost::variant<required_t, std::unique_ptr<data::DataType>>;
    using Signature = FlatMap<std::string, Parameter>;

    /*
     * This class helps collecting the function arguments.
     *
     * Function arguments can be added by name or appended to the end of the
     * list.
     *
     * Arguments are checked against the signature used to construct the class,
     * and a  exception is raised if something doesn't match the expectations.
     */
    class Expression;
    class WrappedFunction;
    class FunctionInstance {
        public:
            using argument_t = std::unique_ptr<Expression>;

            FunctionInstance(WrappedFunction const&);
            FunctionInstance(WrappedFunction const&, std::vector<argument_t>);

            void push_back(std::string const&, argument_t);
            void push_back(argument_t);

            std::vector<data::DataType const*> arguments(data::IScope&) const;
            std::unique_ptr<data::DataType> call(data::IScope&) const;
        private:
            void set(size_t, argument_t);

            WrappedFunction const& function;
            std::vector<argument_t> data;
            int last_index{0};
            bool positional{true};
    };

}}}

namespace ls {
    std::string repr(sass_script::expression::Signature const& sig);
}
