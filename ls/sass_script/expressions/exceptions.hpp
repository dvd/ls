#pragma once
#include <stdexcept>
#include <ls/sass_script/data/exceptions.hpp>

namespace ls { namespace sass_script { namespace expression {

struct invalid_signature : data::type_error {
    using data::type_error::type_error;
};

struct argument_error : std::runtime_error {
    using std::runtime_error::runtime_error;
};

// raised when an argument name is not found in the signature
struct unknown_argument : argument_error {
    using argument_error::argument_error;
};

// raised when more arguments than the signature size are used
struct argument_out_of_range : argument_error {
    argument_out_of_range(size_t index) : argument_error{std::to_string(index)} {}
};

// raised when an argument is passed twice (both as a name and a positional
// argument)
struct argument_redefined : argument_error {
    using argument_error::argument_error;
    argument_redefined(size_t index) : argument_error{std::to_string(index)} {}
};

// raised when a required argument is missing
struct required_argument : argument_error {
    using argument_error::argument_error;
    required_argument(size_t index) : required_argument{std::to_string(index)} {}
};

// raised when a null value is used
struct null_argument : argument_error {
    using argument_error::argument_error;
    null_argument(size_t index) : argument_error{std::to_string(index)} {}
};

// raised when a positional argument is used after a named one
struct positional_argument_now_allowed : argument_error {
    positional_argument_now_allowed() : argument_error{""} {}
};

}}}
