#pragma once
#include "exceptions.hpp"
#include "signature.hpp"
#include <vector>
#include <ls/sass_script/data/types.hpp>
#include <ls/sass_script/data/exceptions.hpp>
#include <ls/meta.hpp>
#include <ls/types.hpp>
#include <ls/strings.hpp>

namespace ls { namespace sass_script { namespace expression {
    /*
     * Base class for a function with a given arity.
     *
     * New function must derive this class, implements the signature method and
     * one or more operator().
     *
     * A Function must return an unique pointer to a new DataType, or in other
     * words, it cannot return a pointer to an already existent instance.
     *
     * For example:
     *
     * class Add : public Function<2> {
     *  public:
     *      using Function<2>::operator();
     *      std::unique_ptr<DataType> operator()(A const&, A const&) const {...}
     *      std::unique_ptr<DataType> operator()(B const&, B const&) const {...}
     *
     *      Signature const& signature() const override {
     *          static Signature sig;
     *          if (sig.size() == 0) {
     *              sig.push_back("op1", required);
     *              sig.push_back("op2", required);
     *          };
     *          return sig;
     *      }
     * }
     */

    class Expression;

    template<size_t nargs>
    class Function {
        public:
            static size_t const arity{nargs};
            using result_t = std::unique_ptr<data::DataType>;

            template <typename... Args>
            result_t operator()(Args...) const {
                throw invalid_signature{type_names<Args...>()};
            }

            virtual Signature const& signature() const = 0;
    };

    class WrappedFunction  {
        public:
            using arguments_t = std::vector<data::DataType const*>;
            using func_t = std::function<std::unique_ptr<data::DataType>(arguments_t&)>;

        private:
            func_t _function;
            Signature const* _signature;

        public:
            WrappedFunction(WrappedFunction const& other) : _function{other._function}, _signature{other._signature} {}
            WrappedFunction(func_t f, Signature const* s) : _function{f}, _signature{s} {}

            Signature const& signature() const {
                return *_signature;
            }

            std::unique_ptr<data::DataType> operator()(arguments_t& args) const {
                return _function(args);
            }
    };

    template<size_t count, typename F>
    struct CallWrappedFunction {
        template<typename... Args>
        std::unique_ptr<data::DataType> call(F func, std::vector<data::DataType const*>& args, Args... func_args) {
            std::unique_ptr<data::DataType> output;

            data::resolve(*args[0], [&](auto& value) {
                args.erase(args.begin());
                output = CallWrappedFunction<count-1, F>{}.call(func, args, func_args..., value);
            });
            return output;
        }
    };

    template<typename F>
    struct CallWrappedFunction<0, F> {
        template<typename... Args>
        std::unique_ptr<data::DataType> call(F func, std::vector<data::DataType const*>&, Args... func_args) {
            return func(func_args...);
        }
    };

    // convert a function (a Function subclass) in a callable that accepts a
    // vector of DataType* and returns a DataType*
    template<typename F>
    auto wrap_function(F func) {
        return WrappedFunction{
            [=](std::vector<data::DataType const*>& args) {
                if (args.size() != F::arity) {
                    throw invalid_signature{""};
                }
                return CallWrappedFunction<F::arity, F>{}.call(func, args);
            },
            &func.signature()
        };
    };
}}}
