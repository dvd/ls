#include "expression.hpp"
#include "function.hpp"
#include <ls/sass_script/data/scope.hpp>
#include <ls/strings.hpp>

namespace ls { namespace sass_script { namespace expression {

using std::move;
using std::string;

Expression::~Expression() = default;

LoadVariable::LoadVariable(std::string n) : name{std::move(n)} {}

data::DataType const* LoadVariable::evaluate(data::IScope& scope) const {
    return &(scope.get(name));
}

string LoadVariable::repr() const {
    return supplant("E:LoadVariable(%)", name);
}

LoadConst::LoadConst(std::unique_ptr<data::DataType> p) : data{std::move(p)} {}

data::DataType const* LoadConst::evaluate(data::IScope&) const {
    return data.get();
}

string LoadConst::repr() const {
    return supplant("E:LoadConst(%)", data->repr());
}

CallFunction::CallFunction(WrappedFunction const& f) : function{f}, inst{f} {}
CallFunction::CallFunction(WrappedFunction const& f, std::vector<argument_t> args)
    : function{f}, inst{f, move(args)} {}

FunctionInstance const& CallFunction::instance() const {
    return inst;
}

FunctionInstance& CallFunction::instance() {
    return const_cast<FunctionInstance&>(
        static_cast<CallFunction const&>(*this).instance());
}

data::DataType const* CallFunction::evaluate(data::IScope& scope) const {
    return scope.add(inst.call(scope));
}

string CallFunction::repr() const {
    return supplant("E:CallFunction()");
}

}}}
