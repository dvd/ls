#include "expression.hpp"
#include "function.hpp"
#include "exceptions.hpp"
#include <ls/sass_script/data/scope.hpp>
#include <ls/goodies/infix_ostream_iterator.hpp>
#include <ls/strings.hpp>

namespace ls { namespace sass_script { namespace expression {

required_t required;

using std::move;

FunctionInstance::FunctionInstance(WrappedFunction const& f)
    : function{f} {

    auto& sig = function.signature();
    data.reserve(sig.size());
    for (size_t ix=0; ix<sig.size(); ix++) {
        data.push_back(argument_t{});
    }
}

FunctionInstance::FunctionInstance(WrappedFunction const& f, std::vector<argument_t> src)
    : function{f}, data{move(src)} {}

void FunctionInstance::set(size_t index, argument_t value) {
    if (index >= data.size()) {
        throw argument_out_of_range{index};
    }
    if (!value) {
        throw null_argument{index};
    }
    if (data[index]) {
        throw argument_redefined{index};
    }
    data[index] = move(value);
}

void FunctionInstance::push_back(std::string const& arg, argument_t value) {
    size_t index;
    try {
        index = function.signature().index(arg);
    }
    catch(std::out_of_range&) {
        throw unknown_argument{arg};
    }
    if (!value) {
        throw null_argument{arg};
    }
    set(index, move(value));
    positional = false;
}

void FunctionInstance::push_back(argument_t value) {
    if (!positional) {
        throw positional_argument_now_allowed{};
    }
    set(last_index++, move(value));
}

std::vector<data::DataType const*> FunctionInstance::arguments(data::IScope& scope) const {
    using std::unique_ptr;
    using data::DataType;

    std::vector<DataType const*> output;
    output.reserve(data.size());

    for (size_t ix=0; ix<data.size(); ix++) {
        if (!data[ix]) {
            auto& default_value = function.signature().at(ix);
            if (auto p = boost::get<unique_ptr<DataType>>(&default_value)) {
                output.push_back(p->get());
            }
            else {
                throw required_argument{ix};
            }
        }
        else {
            output.push_back(data[ix]->evaluate(scope));
        }
    }
    return move(output);
}

std::unique_ptr<data::DataType> FunctionInstance::call(data::IScope& scope) const {
    auto args = arguments(scope);
    return function(args);
}

}}}

namespace ls {
    using std::string;
    using sass_script::expression::Signature;

    string repr(Signature const& sig) {
        using std::unique_ptr;
        using sass_script::data::DataType;

        std::ostringstream output;
        auto it = ls::infix_ostream_iterator<string>(output, ",");

        for (size_t ix=0; ix<sig.size(); ix++) {
            auto& e = sig.at(ix);
            if (auto p = boost::get<unique_ptr<DataType>>(&e)) {
                *it = supplant("[%=%]", sig.key(ix), repr(**p));
            }
            else {
                *it = sig.key(ix);
            }
        }
        return output.str();
    }
}
