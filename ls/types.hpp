#pragma once
#include <string>
#include <typeinfo>

namespace ls {
    std::string demangle(char const*);

    template <typename T>
    std::string type_name() {
        auto mangled = typeid(T).name();
        auto name = demangle(mangled);
        if (name == "") {
            name = mangled;
        }
        return std::move(name);
    }

    template <typename...> struct type_names_t;

    template <typename... Args>
    std::string type_names() {
        return type_names_t<Args...>{}();
    }

    template <typename T, typename... Args>
    struct type_names_t<T, Args...> {
        std::string operator()() {
            return type_name<T>() + "," + type_names<Args...>();
        }
    };

    template <typename T>
    struct type_names_t<T> {
        std::string operator()() {
            return type_name<T>();
        }
    };
}
