#pragma once
#include <ostream>
#include <string>

namespace ls {
    using std::move;
    using std::ostream;
    using std::string;

    // represents a "normalized" quoted string
    //
    // you can assign to this class a string enclosed by any delimiter but
    // internally it is rewritten to be delimited by the chosen character.
    //
    // quotes_normalized<'"'>{R"("hello")"} -> "hello"
    // quotes_normalized<'"'>{R"('hello mr. "x"')"} -> "hello mr. \"x\""
    // quotes_normalized<'"'>{R"('hello mr. \'x\'')"} -> "hello mr. 'x'"
    template <char delimiter>
    class quotes_normalized {
            string data;
        public:
            quotes_normalized(string);

            quotes_normalized<delimiter>& operator=(string);
            bool operator==(quotes_normalized<delimiter> const&) const;
            operator string() const;

            template <char d>
            friend ostream& operator<<(ostream&, quotes_normalized<d>);

            string normalize(string) const;
    };

    template <char delimiter>
    quotes_normalized<delimiter>::quotes_normalized(string input)
        : data{normalize(move(input))} {
    }

    template <char delimiter>
    quotes_normalized<delimiter>& quotes_normalized<delimiter>::operator=(string value) {
        data = move(normalize(value));
        return *this;
    }

    template <char delimiter>
    bool quotes_normalized<delimiter>::operator==(quotes_normalized<delimiter> const& other) const {
        return data == other.data;
    }

    template <char delimiter>
    quotes_normalized<delimiter>::operator string() const {
        return data;
    }

    template <char delimiter>
    string quotes_normalized<delimiter>::normalize(string value) const {
        auto delim = string{value[0]};
        if (delimiter != delim[0]) {
            auto old_escape = string{"\\"} + delim;
            auto new_escape = string{"\\"} + delimiter;

            while (true) {
                auto found = value.find(old_escape);
                if (found == string::npos) {
                    break;
                }
                value.replace(found, 2, delim);
            }

            size_t start = 0;
            while (true) {
                auto found = value.find(delimiter, start);
                if (found == string::npos) {
                    break;
                }
                start = found+1;
                if (found > 0 && value[found-1] == '\\') {
                    continue;
                }
                value.replace(found, 1, new_escape);
            }

            value[0] = delimiter;
            value[value.size()-1] = delimiter;
        }
        return move(value);
    }

    template <char delimiter>
    ostream& operator<<(ostream& out, quotes_normalized<delimiter> q) {
        out << q.data;
        return out;
    }
}
