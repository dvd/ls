#pragma once
#include <stdexcept>
#include <vector>
#include <utility>

namespace ls {
    /*
     * A map Key -> T that stores its values in a vector (hence the "flat"
     * attribute).
     *
     * It exposes a very limited subset (due the laziness) of the map
     * interface.
     *
     * Supports accessing the elements both by key that by index.
     */
    template <typename Key, typename T>
    class FlatMap {
        public:
            using key_type = Key;
            using mapped_type = T;
            using value_type = std::pair<Key, T>;
            using size_type = std::size_t;

        public:
            FlatMap() {}
            FlatMap(std::initializer_list<std::pair<Key, T>> l) {
                data.reserve(l.size());
                std::copy(std::begin(l), std::end(l), std::back_inserter(data));
            }

            size_t size() const {
                return data.size();
            }

            size_t index(Key const& key) const {
                for (size_t ix=0; ix<data.size(); ix++) {
                    if (std::get<0>(data[ix]) == key) {
                        return ix;
                    }
                }
                throw std::out_of_range{""};
            }

            T const& at(size_type index) const {
                if (index >= data.size()) {
                    throw std::out_of_range{""};
                }
                return std::get<1>(data[index]);
            }
            T const& at(Key const& key) const {
                return std::get<1>(data[index(key)]);
            }
            T& at(size_type index) {
                return const_cast<T&>(
                    static_cast<FlatMap const&>(*this).at(index));
            }
            T& at(Key const& key) {
                return const_cast<T&>(
                    static_cast<FlatMap const&>(*this).at(key));
            }
            Key const& key(size_type index) const {
                if (index >= data.size()) {
                    throw std::out_of_range{""};
                }
                return std::get<0>(data[index]);
            }
            void push_back(Key const& name, T&& value) {
                data.push_back(std::make_pair(name, std::forward<T>(value)));
            }
        private:
            std::vector<std::pair<Key, T>> data;
    };
}
