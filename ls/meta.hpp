#pragma once
#include <typeinfo>

namespace ls { namespace metatype {
    struct Nil {};

    template <typename Head, typename... Tail>
    struct List {
        using type = List<Head, Tail...>;
    };

    template <>
    struct List<Nil> {
        using type = Nil;
    };

    template <typename ...> struct concat;

    template <template <typename ...> class List, typename T, typename ...Types>
    struct concat<List<Types...>, T> {
        using type = List<Types..., T>;
    };

    template <template <typename ...> class List, typename T, typename ...Types>
    struct concat<T, List<Types...>> {
        using type = List<T, Types...>;
    };

    template <typename ...> struct head;

    template <template <typename ...> class List, typename T, typename ...Types>
    struct head<List<T, Types...>> {
        using type = T;
    };

    template <>
    struct head<Nil> {
        using type = Nil;
    };

    template <typename ...> struct tail;

    template <template <typename ...> class List, typename T, typename ...Types>
    struct tail<List<T, Types...>> {
        using type = List<Types...>;
    };

    template <template <typename ...> class List, typename T>
    struct tail<List<T>> {
        using type = Nil;

    };

    template <>
    struct tail<Nil> {
        using type = Nil;
    };

    template <typename V, typename List>
    struct for_each_caller {
        static void call(V const& visitor) {
            using T = typename head<List>::type;
            visitor(static_cast<T*>(nullptr));
            for_each_caller<V, typename tail<List>::type>::call(visitor);
        }
    };

    template <typename V>
    struct for_each_caller<V, Nil> {
        static void call(V const&) {
        }
    };

    template<typename List, typename V>
    void for_each(V const& visitor) {
        for_each_caller<V, List>::call(visitor);
    }

    /*
     * Calls the visitor with a reference to `v` casted to the correct type.
     *
     * `v` is a const reference and consequently the visitor must accept a
     * const reference to the casted type.
     *
     * using X = List<A, B>;
     *
     * resolve<X>(v, [](auto const& t) {
     *      // here decltype(t) is one of the types in X
     * });
     */
    template <typename Types, typename T, typename Func>
    void resolve(T const& v, Func const& visitor) {
        for_each<Types>([&v,&visitor](auto* t) {
            using DerivedT = std::remove_pointer_t<decltype(t)>;
            try {
                visitor(dynamic_cast<DerivedT const&>(v));
            } catch(std::bad_cast&) {
            }
        });
    }
}}
