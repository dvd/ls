#pragma once
#include <stdexcept>
#include <string>

namespace ls {
    struct parsing_error : std::runtime_error {
        parsing_error(long int);

        long int error_position;
    };

    struct owner_error : std::logic_error {
        using logic_error::logic_error;
    };
};
