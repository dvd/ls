#pragma once
#define BOOST_RESULT_OF_USE_DECLTYPE
#define BOOST_SPIRIT_USE_PHOENIX_V3
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include <ls/sass/css3.hpp>
#include <ls/sass/directives.hpp>

namespace ls { namespace sass {
    namespace grammars {
        namespace qi = boost::spirit::qi;
        namespace phx = boost::phoenix;
        namespace fusion = boost::fusion;
        namespace d = ls::sass::directives;
        namespace css3 = ls::sass::css3;
    }
}}
