#pragma once
#include <string>
#include "base.hpp"
#include "css3.hpp"
#include <iostream>

namespace ls { namespace sass { namespace grammars {
    template <typename Iterator, typename Skipper=qi::space_type>
    struct Property : qi::grammar<Iterator, d::Property*(d::Block*), Skipper> {

        qi::rule<Iterator, d::Property*(d::Block*), Skipper> start;
        CssIdent<Iterator> name;
        qi::rule<Iterator, std::string(), Skipper> value;

        Property() : Property::base_type(start, "property") {
            using std::string;

            value = +(qi::char_ - ";");

            auto construct = [](d::Block* parent, string name, string value) {
                return parent->emplace_back<d::Property>(name, value);
            };
            start = (
                name
                >> ':'
                >> value
                >> ';')[qi::_val = phx::bind(construct, qi::_r1, qi::_1, qi::_2)];
        }
    };
}}}

