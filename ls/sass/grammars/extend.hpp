#pragma once
#include "base.hpp"
#include "css3.hpp"

namespace ls { namespace sass { namespace grammars {
    template <typename Iterator, typename Skipper=qi::space_type>
    struct Extend : qi::grammar<Iterator, d::Extend*(d::Block*), Skipper> {
        qi::rule<Iterator, d::Extend*(d::Block*), Skipper> start;
        SimpleSelector<Iterator, Skipper> simple_selector;

        Extend() : Extend::base_type(start, "extend") {
            auto construct = [](d::Block* parent, css3::SimpleSelector const& s) {
                return parent->emplace_back<d::Extend>(s);
            };

            start = (
                qi::lit("@extend")
                >> simple_selector
                >> ';')[qi::_val = phx::bind(construct, qi::_r1, qi::_1)];
        }
    };
}}}
