#pragma once
#include "base.hpp"
#include <string>
#include <tuple>

namespace ls { namespace sass { namespace grammars {
    /*
     * Selector grammar; based on http://www.w3.org/TR/CSS21/grammar.html
     */

    template <typename Iterator>
    struct CssIdent : qi::grammar<Iterator, std::string()> {
        qi::rule<Iterator, std::string()> nl;
        qi::rule<Iterator, std::string()> unicode;
        qi::rule<Iterator, std::string()> escape;
        qi::rule<Iterator, std::string()> nonascii;
        qi::rule<Iterator, std::string()> nmchar;
        qi::rule<Iterator, std::string()> nmstart;
        qi::rule<Iterator, std::string()> name;
        qi::rule<Iterator, std::string()> start;

        CssIdent() : CssIdent::base_type(start, "CSS Identifier") {
            nl.name("nl");
            nl = qi::char_('\n')
                | qi::string("\r\n")
                | qi::char_('\r')
                | qi::char_('\f');

            unicode.name("unicode");
            unicode = qi::no_case[
                qi::char_('\\')
                >> qi::repeat(1, 6)[qi::char_("0-9a-f")]
                >> -qi::omit[ "\r\n" | qi::char_(" \n\r\t\f")]];

            escape.name("escape");
            escape = unicode
                | qi::no_case['\\' >> ~qi::char_("\n\r\f0-9a-f")];

            nonascii.name("nonascii");
            nonascii = ~qi::char_('\x00', '\x7F');

            nmchar.name("nmchar");
            nmchar = qi::no_case[qi::char_("_a-z0-9-")]
                | nonascii
                | escape;

            nmstart.name("nmstart");
            nmstart = qi::no_case[qi::char_("_a-z")]
                | nonascii
                | escape;

            name.name("name");
            name = +nmchar;

            start.name("ident");
            start = -qi::char_('-')
                >> nmstart
                >> *nmchar;
        }
    };

    template <typename Iterator, typename Skipper=qi::space_type>
    struct SimpleSelector : qi::grammar<Iterator, css3::SimpleSelector()> {
        template <typename T>
        using rule = qi::rule<Iterator, T, Skipper>;

        template <typename T>
        using nsrule = qi::rule<Iterator, T>;

        nsrule<std::string()> nl;
        nsrule<std::string()> unicode;
        nsrule<std::string()> escape;
        nsrule<std::string()> nonascii;
        nsrule<std::string()> nmchar;
        nsrule<std::string()> nmstart;
        nsrule<std::string()> name;
        nsrule<std::string()> ident;
        nsrule<std::string()> extended_ident;
        qi::rule<Iterator, std::string(), qi::locals<char>> quoted_string;
        nsrule<css3::ID()> hash;
        nsrule<css3::Class()> class_;
        nsrule<css3::BasicSelector()> element_name;
        nsrule<css3::Attribute()> attrib;
        nsrule<css3::PseudoClass()> pseudo;
        rule<std::string()> function;
        // _[explicit|implicit]_simple_selector are helper rules
        // to simplify the attribute propagation in `simple_selector`
        nsrule<css3::SimpleSelector()> _explicit_simple_selector;
        nsrule<css3::SimpleSelector()> _implicit_simple_selector;
        nsrule<css3::SimpleSelector()> start;

        SimpleSelector() : SimpleSelector::base_type(start, "SimpleSelector") {
            using qi::_1;
            using qi::_a;
            using std::string;
            using boost::optional;

            nl.name("nl");
            nl = qi::char_('\n')
                | qi::string("\r\n")
                | qi::char_('\r')
                | qi::char_('\f');

            unicode.name("unicode");
            unicode = qi::no_case[
                qi::char_('\\')
                >> qi::repeat(1, 6)[qi::char_("0-9a-f")]
                >> -qi::omit[ "\r\n" | qi::char_(" \n\r\t\f")]];

            escape.name("escape");
            escape = unicode
                | qi::no_case['\\' >> ~qi::char_("\n\r\f0-9a-f")];

            nonascii.name("nonascii");
            nonascii = ~qi::char_('\x00', '\x7F');

            nmchar.name("nmchar");
            nmchar = qi::no_case[qi::char_("_a-z0-9-")]
                | nonascii
                | escape;

            nmstart.name("nmstart");
            nmstart = qi::no_case[qi::char_("_a-z")]
                | nonascii
                | escape;

            name.name("name");
            name = +nmchar;

            ident.name("ident");
            ident = -qi::char_('-')
                >> nmstart
                >> *nmchar;

            quoted_string %= (qi::char_("'\"")[_a = _1]
                >> *(
                    +(qi::char_ - qi::char_(_a) - qi::char_("\n\r\f\\"))
                    | qi::as_string[(qi::char_('\\') >> nl)]
                    | escape
                    )
                >> qi::char_(_a));

            hash.name("hash");
            hash = qi::as_string[
                qi::lit('#') >> name];

            class_.name("class_");
            class_ = qi::as_string[
                qi::lit('.') >> ident];

            {
                using attr_signature = boost::variant<string, char>;
                auto handler = [](attr_signature const& element) -> css3::BasicSelector {
                    if (auto *s = boost::get<char>(&element)) {
                        return css3::Universal();
                    }
                    else {
                        return css3::Type(boost::get<string>(element));
                    }
                };
                element_name.name("element name");
                element_name = (ident | qi::char_('*'))[qi::_val = phx::bind(handler, qi::_1)];
            }

            {
                using value_signature = optional<fusion::vector2<string, string>>;
                auto handler = [](string const& name, value_signature const& value) -> css3::Attribute {
                    css3::Attribute output;
                    optional<std::tuple<string, string>> attr_value;
                    if (value) {
                        attr_value = std::make_tuple(
                            fusion::at_c<0>(*value),
                            fusion::at_c<1>(*value));
                    }
                    return output.value(std::make_tuple(name, attr_value));
                };
                attrib.name("attribute");
                attrib = (qi::lit('[')
                    >> ident
                    >> -(
                        (qi::string("=") | qi::string("~=") | qi::string("|="))
                        >> (ident | quoted_string))
                    >> ']')[qi::_val = phx::bind(handler, qi::_1, qi::_2)];
            }

            // Relaxed grammar for pseudo class function arguments
            // see: http://www.w3.org/TR/css3-selectors/#nth-child-pseudo
            extended_ident.name("extended ident for pseudo class function");
            extended_ident = -qi::char_("-+") >> qi::no_case[+qi::char_("_a-z0-9-+")];

            function.name("function");
            function = ident >> qi::char_('(') >> -extended_ident >> qi::char_(')');

            pseudo.name("pseudo attribute");
            pseudo = qi::as_string[
                qi::lit(':')
                >> (qi::hold[qi::skip(qi::space)[function]] | ident)];

            _explicit_simple_selector = (
                element_name
                >> *(hash | class_ | attrib | pseudo)
            )[qi::_val = phx::construct<css3::SimpleSelector>(qi::_1, qi::_2)];

            _implicit_simple_selector = (
                +(hash | class_ | attrib | pseudo)
            )[qi::_val = phx::construct<css3::SimpleSelector>(css3::Universal(), qi::_1)];

            start.name("simple selector");
            start = _explicit_simple_selector | _implicit_simple_selector;
        }
    };

    template <typename Iterator>
    struct Combinator : qi::grammar<Iterator, css3::Combinator()> {
        qi::rule<Iterator, css3::Combinator()> start;

        Combinator() : Combinator::base_type(start, "combinator") {
            auto handler = [](char c) {
                css3::Combinator output;
                switch(c) {
                    case ' ':
                        output = css3::Combinator::whitespace;
                        break;
                    case '>':
                        output = css3::Combinator::greaterThan;
                        break;
                    case '+':
                        output = css3::Combinator::plus;
                        break;
                    case '~':
                        output = css3::Combinator::tilde;
                        break;
                };
                return output;
            };
            start = (
                (*qi::lit(' ') >> qi::char_("+>~"))
                | qi::char_(' '))[qi::_val = phx::bind(handler, qi::_1)];
        }
    };

    template <typename Iterator, typename Skipper=qi::space_type>
    struct Selector : qi::grammar<Iterator, css3::Selector(), Skipper> {
        Combinator<Iterator> combinator;
        SimpleSelector<Iterator, Skipper> simple_selector;
        qi::rule<Iterator, css3::Selector(), Skipper> selector;
        qi::rule<Iterator, css3::Selector(), Skipper> start;

        Selector() : Selector::base_type(start, "Selector") {
            using attr_signature = boost::optional<fusion::vector2<css3::Combinator, css3::Selector>>;
            auto handler = [](css3::SimpleSelector sel, attr_signature elements) {
                css3::Selector output{std::move(sel)};
                if (elements) {
                    output.push_back(
                        fusion::at_c<0>(*elements),
                        std::move(fusion::at_c<1>(*elements)));
                }
                return output;
            };
            selector.name("selector");
            selector = (
                simple_selector
                >> -(qi::no_skip[combinator] >> selector)
            )[qi::_val = phx::bind(handler, qi::_1, qi::_2)];

            start = selector.alias();
        }
    };
}}}
