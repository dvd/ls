#pragma once
#include "base.hpp"
#include "block.hpp"

namespace ls { namespace sass { namespace grammars {

    // This is the grammar needed to parse an SCSS.
    // Basically it lists the sub-grammars that can be found at the top level.
    template <typename Iterator, typename Skipper=qi::space_type>
    struct SCSS : qi::grammar<Iterator, void(d::Block*), Skipper> {
        qi::rule<Iterator, void(d::Block*), Skipper> start;
        Block<Iterator, Skipper> block;

        SCSS() : SCSS::base_type(start, "scss") {
            start = *(block(qi::_r1));
        }
    };

}}}
