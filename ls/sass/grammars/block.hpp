#pragma once
#include <vector>

#include "base.hpp"
#include "css3.hpp"
#include "extend.hpp"
#include "property.hpp"

namespace ls { namespace sass { namespace grammars {

    template <typename Iterator, typename Skipper=qi::space_type>
    struct Block : qi::grammar<Iterator, d::Block*(d::Block*), Skipper> {
        qi::rule<Iterator, d::Block*(d::Block*), Skipper> start;
        Selector<Iterator, Skipper> selector;
        Extend<Iterator, Skipper> extend;
        Property<Iterator, Skipper> property;

        Block() : Block::base_type(start, "block") {
            auto construct = [](d::Block* parent, std::vector<css3::Selector> const& s) {
                return parent->emplace_back<d::Block>(s);
            };

            start = (
                (selector % ',')[qi::_val = phx::bind(construct, qi::_r1, qi::_1)]
                >> '{'
                >> *(
                    extend(qi::_val)
                    | property(qi::_val)
                    | start(qi::_val))
                >> '}');
        }
    };
}}}
