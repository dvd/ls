#include "simple_selector.hpp"
#include <sstream>

namespace ls { namespace sass { namespace css3 {

using std::string;
using std::vector;

SimpleSelector::SimpleSelector()
    : SimpleSelector{Universal()} {}

SimpleSelector::SimpleSelector(BasicSelector b)
    : SimpleSelector{b, {}} {}

SimpleSelector::SimpleSelector(BasicSelector b, vector<AuxSelector> const& v)
    : head{std::move(b)} {
    tail = std::move(v);
}

bool SimpleSelector::operator==(SimpleSelector const& other) const {
    if (!(head == other.head) || tail.size() != other.tail.size()) {
        return false;
    }
    return tail && other.tail;
}

bool SimpleSelector::operator!=(SimpleSelector const& other) const {
    return !(*this == other);
}

string SimpleSelector::to_string() const {
    std::ostringstream out;
    out << head;
    for (auto& e : tail) {
        out << e;
    }
    return out.str();
}

SimpleSelector operator-(SimpleSelector const& a, SimpleSelector const& b) {
    SimpleSelector output{};
    output.head = a.head - b.head;
    output.tail = a.tail - b.tail;
    return output;
}

SimpleSelector operator|(SimpleSelector const& a, SimpleSelector const& b) {
    SimpleSelector output{};
    output.head = a.head | b.head;
    output.tail = a.tail | b.tail;
    return output;
}

bool operator&&(SimpleSelector const& a, SimpleSelector const& b) {
    if (!(a.head && b.head)) {
        return false;
    }
    return a.tail && b.tail;
}

}}}
