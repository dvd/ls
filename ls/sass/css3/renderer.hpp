#pragma once
#include <string>

namespace ls { namespace sass { namespace css3 {
    /*
     * Utility class to render a Selector into a string
     */
    class Selector;

    class Renderer {
        public:
            Renderer(Selector const*);
            Renderer(Selector const*, bool);

            std::string to_string() const;

            /*
             * If set to true wraps every SimpleSelector inside the Selector
             * between { and }
             */
            bool selector_separator{false};
        private:
            Selector const* selector;
    };
}}};
