#pragma once
#include <stdexcept>
#include <string>
#include "base.hpp"

namespace ls { namespace sass { namespace css3 {
    struct selector_conflict : std::logic_error {
        selector_conflict(std::string a, std::string b)
            : std::logic_error{"selectors conflicting"}, a{std::move(a)}, b{std::move(b)} {}

        template <typename T>
        selector_conflict(T const& a, T const& b)
            : logic_error{"selectors conflicting"}, a{to_string(a)}, b{to_string(b)} {}

        std::string a;
        std::string b;
    };
}}}

