#include "basic.hpp"
#include "../exceptions.hpp"

namespace ls { namespace sass { namespace css3 {

using std::string;

Type::Type() {}
Type::Type(string n) : _value{std::move(n)} {}

bool Type::operator==(Type const& other) const {
    return _value == other._value;
}

bool Type::operator!=(Type const& other) const {
    return !(*this == other);
}

string Type::value() const {
    return _value;
}

Type& Type::value(string v) {
    _value = std::move(v);
    return *this;
}

string Type::to_string() const {
    return _value;
}

Universal::Universal() {}

bool Universal::operator==(Universal const&) const {
    return true;
}

bool Universal::operator!=(Universal const&) const {
    return false;
}

string Universal::to_string() const {
    return "*";
}

BasicSelector operator-(BasicSelector const& a, BasicSelector const& b) {
    if (!(a && b)) {
        throw selector_conflict{a, b};
    }
    return Universal();
}

BasicSelector operator|(BasicSelector const& a, BasicSelector const& b) {
    auto compatible = (a && b) || (boost::get<Universal>(&a) != nullptr);
    if (!compatible) {
        throw selector_conflict{a, b};
    }
    return b;
}

namespace {
    struct BasicSelectorVisitor : boost::static_visitor<bool> {
        bool operator()(Universal const&, Universal const&) const {
            return true;
        }
        bool operator()(Universal const&, Type const&) const {
            return false;
        }
        bool operator()(Type const&, Universal const&) const {
            return true;
        }
        bool operator()(Type const& reference, Type const& sel) const {
            return reference == sel;
        }
    };
}

bool operator&&(BasicSelector const& a, BasicSelector const& b) {
    return boost::apply_visitor(BasicSelectorVisitor{}, a, b);
}

}}}
