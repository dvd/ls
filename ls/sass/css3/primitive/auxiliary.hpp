#pragma once
#include <string>
#include <tuple>
#include <boost/optional.hpp>
#include <boost/variant.hpp>

namespace ls { namespace sass { namespace css3 {
    using std::tuple;

    class Attribute  {
        public:
            using type = std::tuple<
                std::string, boost::optional<std::tuple<std::string, std::string>>>;

            Attribute();
            Attribute(std::string);
            Attribute(std::string, std::tuple<std::string, std::string>);

            type value() const;
            Attribute& value(type);

            bool operator==(Attribute const&) const;
            bool operator!=(Attribute const&) const;

            std::string to_string() const;
        private:
            type _value;
    };

    class Class  {
        public:
            using type = std::string;

            Class();
            Class(std::string);

            std::string value() const;
            Class& value(std::string);

            bool operator==(Class const&) const;
            bool operator!=(Class const&) const;

            std::string to_string() const;
        private:
            std::string _value = "";
    };

    class ID {
        public:
            using type = std::string;

            ID();
            ID(std::string);

            std::string value() const;
            ID& value(std::string);

            bool operator==(ID const&) const;
            bool operator!=(ID const&) const;

            std::string to_string() const;
        private:
            std::string _value = "";
    };

    class PseudoClass {
        public:
            using type = std::string;

            PseudoClass();
            PseudoClass(std::string);

            std::string value() const;
            PseudoClass& value(std::string);

            bool operator==(PseudoClass const&) const;
            bool operator!=(PseudoClass const&) const;

            std::string to_string() const;
        private:
            std::string _value = "";
    };

    using AuxSelector = boost::variant<ID, Class, Attribute, PseudoClass>;

    /*
     * Checks if the first vector of AuxSelector "matches" the second one.
     *
     * Returns true if every element of the second vector is found in the first
     * one.
     */
    bool operator&&(std::vector<AuxSelector> const&, std::vector<AuxSelector> const&);

    /*
     * Computes the difference between the two vectors.
     *
     * The returned vector contains all the elements from the first vector that
     * doesn't appear in the second one.
     *
     * [a,b] - [a,c] -> [b]
     */
    std::vector<AuxSelector> operator-(std::vector<AuxSelector>, std::vector<AuxSelector> const&);

    /*
     * Combines the two vectors.
     *
     * Every elements found in the second vector but not in the first one is
     * added to the first vector.
     *
     * [a,b] | [a,c] -> [a,b,c]
     */
    std::vector<AuxSelector> operator|(std::vector<AuxSelector>, std::vector<AuxSelector> const&);

}}}
