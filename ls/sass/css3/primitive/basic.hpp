#pragma once
#include <string>
#include <boost/variant.hpp>

namespace ls { namespace sass { namespace css3 {
    class Type {
        public:
            using type = std::string;

            Type();
            Type(std::string);

            std::string value() const;
            Type& value(std::string);

            bool operator==(Type const&) const;
            bool operator!=(Type const&) const;

            std::string to_string() const;
        private:
            std::string _value = "";
    };

    class Universal {
        public:
            using type = void;

            Universal();

            bool operator==(Universal const&) const;
            bool operator!=(Universal const&) const;

            std::string to_string() const;
    };

    using BasicSelector = boost::variant<Universal, Type>;

    /*
     * Checks if the the first selector "matches" the second one.
     *
     * An Universal selector matches only another Universal selector.
     * A Type selector matches an Universal selector or another Type selector
     * with the same value.
     */
    bool operator&&(BasicSelector const&, BasicSelector const&);

    /*
     * "Removes" the second selector from the first one, but only if the two
     * "match", otherwise the selector_conflict exception is thrown.
     *
     * Universal - Universal -> Universal
     * Type      - Type      -> Universal
     */
    BasicSelector operator-(BasicSelector const&, BasicSelector const&);

    /*
     * "Combine" the first selector with the second one, but only if the two
     * "match", otherwise the selector_conflict exception is thrown.
     *
     * Universal | *    -> *
     * Type      | Type -> Type
     */
    BasicSelector operator|(BasicSelector const&, BasicSelector const&);

}}}
