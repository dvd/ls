#include "auxiliary.hpp"
#include <algorithm>
#include <cctype>
#include <sstream>
#include <ls/quoted_string.hpp>
#include <ls/strings.hpp>

namespace ls { namespace sass { namespace css3 {

using std::move;
using std::string;
using std::tuple;
using std::vector;

Attribute::Attribute() {}

Attribute::Attribute(string n) : _value{move(n), {}} {}

Attribute::Attribute(string n, tuple<string, string> v) {
    value(std::make_tuple(n, v));
}

Attribute::type Attribute::value() const {
    return _value;
}

Attribute& Attribute::value(Attribute::type v) {
    auto& name = std::get<0>(v);
    auto& value = std::get<1>(v);
    if (value) {
        string& data = std::get<1>(*value);
        if (data[0] == '"' || data[0] == '\'') {
            data = static_cast<string>(ls::quotes_normalized<'"'>{data});
            std::get<1>(*value) = data;
        }
    }
    _value = Attribute::type{name, value};
    return *this;
}

bool Attribute::operator==(Attribute const& other) const {
    return _value == other._value;
}

bool Attribute::operator!=(Attribute const& other) const {
    return !(*this == other);
}

string Attribute::to_string() const {
    std::ostringstream out;
    auto& name = std::get<0>(_value);
    auto& value = std::get<1>(_value);

    out << '[' << name;
    if (value) {
        out << std::get<0>(*value) << std::get<1>(*value);
    }
    out << ']';
    return out.str();
}

Class::Class() {}

Class::Class(string n) : _value{move(n)} {}

string Class::value() const {
    return _value;
}

Class& Class::value(string v) {
    _value = move(v);
    return *this;
}

bool Class::operator==(Class const& other) const {
    return _value == other._value;
}

bool Class::operator!=(Class const& other) const {
    return !(*this == other);
}

string Class::to_string() const {
    return supplant(".%", _value);
}

ID::ID() {}

ID::ID(string n) : _value{move(n)} {}

string ID::value() const {
    return _value;
}

ID& ID::value(string v) {
    _value = move(v);
    return *this;
}

bool ID::operator==(ID const& other) const {
    return _value == other._value;
}

bool ID::operator!=(ID const& other) const {
    return !(*this == other);
}

string ID::to_string() const {
    return supplant("#%", _value);
}

PseudoClass::PseudoClass() {}

PseudoClass::PseudoClass(string n) : _value{move(n)} {
    std::transform(_value.begin(), _value.end(), _value.begin(), ::tolower);

    if (_value == "nth-child(2n)" || _value == "nth-child(2n+0)") {
        _value = "nth-child(even)";
    }
    else if (_value == "nth-child(2n+1)") {
        _value = "nth-child(odd)";
    }
}

string PseudoClass::value() const {
    return _value;
}

PseudoClass& PseudoClass::value(string v) {
    _value = move(v);
    return *this;
}

bool PseudoClass::operator==(PseudoClass const& other) const {
    return _value == other._value;
}

bool PseudoClass::operator!=(PseudoClass const& other) const {
    return !(*this == other);
}

string PseudoClass::to_string() const {
    return supplant(":%", _value);
}

namespace {
    // variant visitor that return true if the element visited is found
    // in the reference vector
    struct AuxVectorsEquality : boost::static_visitor<bool> {
        vector<AuxSelector> const* reference;

        AuxVectorsEquality(vector<AuxSelector> const* r) : reference{r} {}

        template <typename S>
        bool operator()(S const& sel) const {
            auto same_type = filter<S>();
            auto predicate = [&sel](S const* ref) -> bool { return sel == *ref; };
            return std::any_of(same_type.begin(), same_type.end(), predicate);
        }

        template <typename T>
        vector<T const*> filter() const {
            vector<T const*> output;
            for (auto& aux : *reference) {
                if (auto p = boost::get<T>(&aux)) {
                    output.push_back(p);
                }
            }
            return output;
        }
    };
}

vector<AuxSelector> operator-(vector<AuxSelector> a, vector<AuxSelector> const& b) {
    AuxVectorsEquality visitor{&b};
    auto predicate = [&visitor](AuxSelector const& e) -> bool {
        return boost::apply_visitor(visitor, e); };

    a.erase(
        std::remove_if(a.begin(), a.end(), predicate),
        a.end());
    return a;
}

vector<AuxSelector> operator|(vector<AuxSelector> a, vector<AuxSelector> const& b) {
    AuxVectorsEquality visitor{&a};
    auto predicate = [&visitor](AuxSelector const& e) -> bool {
        return !boost::apply_visitor(visitor, e); };
    std::copy_if(b.begin(), b.end(), std::back_inserter(a), predicate);
    return a;
}

bool operator&&(vector<AuxSelector> const& a, vector<AuxSelector> const& b) {
    AuxVectorsEquality visitor{&a};
    auto predicate = [&visitor](AuxSelector const& e) -> bool {
        return boost::apply_visitor(visitor, e); };
    return std::all_of(b.begin(), b.end(), predicate);
}

}}}
