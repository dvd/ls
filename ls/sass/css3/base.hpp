#pragma once
#include <ostream>
#include <string>
#include <type_traits>
#include <boost/variant.hpp>

namespace ls { namespace sass { namespace css3 {
    template <typename T>
    struct has_to_string {
        template <typename U>
        static constexpr std::true_type f(decltype(std::declval<U>().to_string())*);

        template <typename>
        static constexpr std::false_type f(...);

        static constexpr bool value = decltype(f<T>(nullptr))::value;
    };

    /*
     * makes every type with a `.to_string()` method stremable to an ostream
     */
    template<typename T, typename std::enable_if<has_to_string<T>::value>::type* = nullptr>
    std::ostream& operator<<(std::ostream& out, T const& x) {
        out << x.to_string();
        return out;
    }

    /*
     * calls `to_string()` on the wrapped type of the passed variant
     */
    namespace details {
        struct _V : boost::static_visitor<std::string> {
            template <typename S>
            std::string operator()(S const& x) const {
                return x.to_string();
            }
        };
    }

    template<typename T>
    std::string to_string(T const &x) {
        return boost::apply_visitor(details::_V{}, x);
    }
}}}
