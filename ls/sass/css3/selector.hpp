#pragma once
#include <ostream>
#include <string>
#include <vector>
#include <boost/variant.hpp>
#include <boost/optional.hpp>
#include "simple_selector.hpp"

namespace ls { namespace sass { namespace css3 {
    enum class Combinator {
        whitespace,
        greaterThan,
        plus,
        tilde
    };
    std::ostream& operator<<(std::ostream&, Combinator const&);

    /*
     * A selector is a chain of one or more sequences of simple selectors
     * separated by combinators.
     */
    using SelectorElement = boost::variant<Combinator, SimpleSelector>;

    class Selector {
        public:
            // Shortcut to build an Universal Selector
            Selector();
            Selector(SimpleSelector);

            void push_back(Combinator, SimpleSelector);
            /*
             * Adds every element of the selector
             *
             * [a].push_back(+, [b c]) -> [a+b c]
             */
            void push_back(Combinator, Selector const&);

            /*
             * Gives a read-only access to the underlying storage.
             */
            std::vector<SelectorElement> const& elements() const;
            /*
             * Read-only access to the simple selectors.
             */
            std::vector<SimpleSelector const*> selectors() const;

            /*
             * Returns a reference to the nth simple selector.
             */
            SimpleSelector& selector(int);
            SimpleSelector const& selector(int) const;

            /*
             * Replaces the nth SimpleSelector with a copy of the passed one
             */
            void replace_selector(int, SimpleSelector const&);
            /*
             * Replaces the first SimpleSelector with a copy of the passed one.
             * If the first SimpleSelector is not found a std::out_of_range
             * exception is thrown.
             */
            void replace_selector(SimpleSelector const&, SimpleSelector const&);
            /*
             * Replaces the nth SimpleSelector with a copy of the passed Selector.
             * Every element in the passed selector is inserted in the position
             * of the nth SimpleSelector.
             */
            void replace_selector(int, Selector const&);
            /*
             * Replaces the first SimpleSelector with a copy of the passed Selector.
             * If the first SimpleSelector is not found a std::out_of_range
             * exception is thrown.
             */
            void replace_selector(SimpleSelector const&, Selector const&);

            /*
            * Builds a Selector from the input string, throw a
            * ls::parsing_error if the input is malformed
            */
            static Selector fromString(std::string const&);

            std::string to_string() const;

            bool operator==(Selector const&) const;
            bool operator!=(Selector const&) const;
        private:
            template <typename F>
            std::vector<SelectorElement>::const_iterator find_selector(F) const;

            std::vector<SelectorElement> _elements;
    };
    /*
     * Checks if the the two selector are compatible.
     *
     * Two selectors are compatible when, traversed together, have all  the
     * selectors compatible and the combinators equal.
     */
    bool operator&&(Selector const&, Selector const&);

    /*
     * Splits a Selector at the specified Combinator
     *
     * split({div.foo span+b}, +) -> [div.foo span, b]
     */
    std::vector<Selector> split(Selector const&, Combinator);

    /*
     * Joins a vector of Selector with the specified Combinator
     *
     * join([div.foo span, b], +) -> {div.foo span+b}
     */
    Selector join(std::vector<Selector> const&, Combinator);

    /*
     * Combines the two selector in a new one, the two selectors are spearated
     * by the combinator.
     *
     * combine({div}, {p}, +) -> {div+p}
     */
    Selector combine(Selector const&, Selector const&, Combinator);

    /*
     * Combines every selector in the first vector with every selector in the
     * second vector.
     *
     * combine([{div}, {span}], [{b}, {em}], +)
     *  -> [{div+b}, {div+em}, {span+b}, {span+em}]
     */
    std::vector<Selector> combine(std::vector<Selector> const&, std::vector<Selector> const&, Combinator);

    /*
     * Search a SimpleSelector compatible with the passed one, if found a
     * reference is returned.
     */
    boost::optional<SimpleSelector const&> find_match(Selector const&, SimpleSelector const&);

    std::ostream& operator<<(std::ostream& out, std::vector<Selector> const& selectors);
}}}
