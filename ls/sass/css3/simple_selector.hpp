#pragma once
#include <string>
#include <vector>
#include "base.hpp"
#include "primitive/auxiliary.hpp"
#include "primitive/basic.hpp"

namespace ls { namespace sass { namespace css3 {
    /*
     * A SimpleSelector (called by the w3c `selector`) is an aggregation of one
     * or more primitive selectors (called by the w3c `simple selectors`)
     *
     * Quoting from the w3c:
     * > A sequence of simple selectors (SimpleSelector) is a chain of simple
     * > selectors (primitive selectors) [...]. It always begins with a type
     * > selector or a universal selector. No other type selector or universal
     * > selector is allowed in the sequence.
     *
     * SimpleSelector::head holds the type selector or the universal selector
     * SimpleSelector::tails contains the other selectors (AuxSelector)
     */
    struct SimpleSelector {
        // Shortcut to build an Universal SimpleSelector
        SimpleSelector();
        SimpleSelector(BasicSelector);
        SimpleSelector(BasicSelector, std::vector<AuxSelector> const&);

        BasicSelector head;
        std::vector<AuxSelector> tail;

        /*
         * Checks if the two SimpleSelector are "equals"; equals means that the
         * two heads are equal and the two tails are "compatible".
         */
        bool operator==(SimpleSelector const&) const;
        bool operator!=(SimpleSelector const&) const;

        std::string to_string() const;

        /*
         * Builds a SimpleSelector from the input string, throw a
         * ls::parsing_error if the input is malformed
         */
        static SimpleSelector fromString(std::string const&);
    };

    /*
     * Checks if the the first selector "matches" the second one.
     *
     * Returns true if both the head and the tail are compatible.
     */
    bool operator&&(SimpleSelector const&, SimpleSelector const&);

    /*
     * Computes the difference between the two selectors.
     *
     * The difference between two selectors is a selector with the head equals
     * to the difference between the heads and the tail equals to the
     * difference between the tails.
     */
    SimpleSelector operator-(SimpleSelector const&, SimpleSelector const&);

    /*
     * Combines the the two selectors.
     *
     * The union between two selectors is a selector with the head equals to
     * the union of the heads and the tail equals to the union of the tails.
     */
    SimpleSelector operator|(SimpleSelector const&, SimpleSelector const&);
}}}
