#include "renderer.hpp"
#include "selector.hpp"
#include <sstream>

namespace ls { namespace sass { namespace css3 {

Renderer::Renderer(Selector const* sel)
    : Renderer{sel, false} {}

Renderer::Renderer(Selector const* sel, bool sel_sep)
    : selector_separator{sel_sep}, selector{sel} {}

std::string Renderer::to_string() const {
    std::ostringstream out;
    for (auto& e : selector->elements()) {
        if (selector_separator && boost::get<SimpleSelector>(&e)) {
            out << "{" << e << "}";
        }
        else {
            out << e;
        }
    }
    return out.str();
}

}}};
