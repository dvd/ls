#include "selector.hpp"
#include "renderer.hpp"
#include <ls/goodies/remove_constness.hpp>
#include <ls/goodies/infix_ostream_iterator.hpp>
#include <algorithm>
#include <stdexcept>

namespace ls { namespace sass { namespace css3 {

using std::string;
using std::vector;
using boost::optional;

std::ostream& operator<<(std::ostream& out, Combinator const& c) {
    switch(c) {
        case Combinator::whitespace:
            out << ' ';
            break;
        case Combinator::greaterThan:
            out << '>';
            break;
        case Combinator::plus:
            out << '+';
            break;
        case Combinator::tilde:
            out << '~';
            break;
    }
    return out;
}

template <typename F>
std::vector<SelectorElement>::const_iterator Selector::find_selector(F predicate) const {
    auto it = std::begin(_elements);
    auto end = std::end(_elements);
    while (it < end) {
        if (predicate(boost::get<SimpleSelector>(*it))) {
            return it;
        }
        std::advance(it, 2);
    }
    return end;
}

template <>
inline std::vector<SelectorElement>::const_iterator
    Selector::find_selector<SimpleSelector>(SimpleSelector needle) const {

    auto predicate = [&needle](SimpleSelector const& el) -> bool {
        return needle == el;
    };
    return find_selector(predicate);
}

Selector::Selector() : Selector{SimpleSelector{}} {}

Selector::Selector(SimpleSelector sel) {
    _elements.push_back(sel);
}

void Selector::push_back(Combinator c, SimpleSelector sel) {
    _elements.push_back(c);
    _elements.push_back(std::move(sel));
}

void Selector::push_back(Combinator c, Selector const& sel) {
    _elements.push_back(c);
    for (auto& e: sel._elements) {
        _elements.push_back(e);
    }
}

vector<SelectorElement> const& Selector::elements() const {
    return _elements;
}

vector<SimpleSelector const*> Selector::selectors() const {
    vector<SimpleSelector const*> output;
    find_selector(
        [&](SimpleSelector const& el) -> bool {
            output.push_back(&el);
            return false;
        });
    return output;
}

SimpleSelector const& Selector::selector(int index) const {
    size_t position = index >= 0 ? 2*index : _elements.size() - 2*(index+1) - 1;
    return boost::get<SimpleSelector>(_elements.at(position));
}

SimpleSelector& Selector::selector(int index) {
    return const_cast<SimpleSelector&>(
        static_cast<const Selector &>(*this).selector(index));
}

void Selector::replace_selector(int index, SimpleSelector const& sel) {
    selector(index) = sel;
}

void Selector::replace_selector(int index, Selector const& sel) {
    replace_selector(selector(index), sel);
}

void Selector::replace_selector(SimpleSelector const& index, SimpleSelector const& sel) {
    auto pos = find_selector(index);
    if (pos == _elements.end()) {
        throw std::out_of_range("element not found");
    }
    else {
        auto it = ls::remove_constness(_elements, pos);
        *it = sel;
    }
}

void Selector::replace_selector(SimpleSelector const& index, Selector const& sel) {
    auto match = find_selector(index);
    if (match == _elements.end()) {
        throw std::out_of_range("element not found");
    }
    auto pos = _elements.insert(match, sel._elements.begin(), sel._elements.end());
    std::advance(pos, sel._elements.size());
    _elements.erase(pos);
}

string Selector::to_string() const {
    return Renderer(this).to_string();
}

bool Selector::operator==(Selector const& other) const {
    if (_elements.size() != other._elements.size()) {
        return false;
    }
    return _elements == other._elements;
}

bool Selector::operator!=(Selector const& other) const {
    return !(*this == other);
}

vector<Selector> split(Selector const& sel, Combinator sep) {
    vector<Selector> output;
    if (sel.elements().size() == 0) {
        return output;
    }

    auto& elements = sel.elements();
    auto it = std::begin(elements);
    auto end = std::end(elements);

    Selector s = Selector{boost::get<SimpleSelector>(*it++)};

    while (it != end) {
        auto c = boost::get<Combinator>(*it++);
        if (c == sep) {
            output.push_back(s);
            s = Selector{boost::get<SimpleSelector>(*it++)};
        }
        else {
            s.push_back(c, boost::get<SimpleSelector>(*it++));
        }
    }
    output.push_back(s);

    return output;
}

Selector join(vector<Selector> const& selectors, Combinator sep) {
    if (selectors.size() == 0) {
        return {};
    }
    auto it = std::begin(selectors);
    auto end = std::end(selectors);

    Selector output{*it++};
    while (it != end) {
        output.push_back(sep, *it++);
    }
    return output;
}

Selector combine(Selector const& a, Selector const& b, Combinator sep) {
    Selector output{a};
    output.push_back(sep, b);
    return output;
}

vector<Selector> combine(vector<Selector> const& a, vector<Selector> const& b, Combinator sep) {
    if (a.size() == 0) {
        return b;
    }
    else if (b.size() == 0) {
        return a;
    }

    vector<Selector> output;
    output.reserve(a.size() * b.size());

    for (auto& head : a) {
        for (auto& tail : b) {
            output.push_back(combine(head, tail, sep));
        }
    }
    return output;
}

optional<SimpleSelector const&> find_match(Selector const& a, SimpleSelector const& b) {
    optional<SimpleSelector const&> output;
    for (auto child : a.selectors()) {
        if (*child && b) {
            output = *child;
        }
    }
    return output;
}

bool operator&&(Selector const& a, Selector const& b) {
    auto a_elements = a.elements();
    auto b_elements = b.elements();

    auto it_a = std::begin(a_elements);
    auto end_a = std::end(a_elements);
    auto it_b = std::begin(b_elements);
    auto end_b = std::end(b_elements);

    while (it_a != end_a && it_b != end_b) {
        auto& sel_a = boost::get<SimpleSelector>(*it_a++);
        auto& sel_b = boost::get<SimpleSelector>(*it_b++);
        if (!(sel_a && sel_b)) {
            return false;
        }
        if (it_a == end_a || it_b == end_b) {
            return false;
        }
        auto com_a = boost::get<Combinator>(*it_a++);
        auto com_b = boost::get<Combinator>(*it_b++);
        if (com_a != com_b) {
            return false;
        }
    }
    return true;
}

std::ostream& operator<<(std::ostream& out, vector<Selector> const& selectors) {
    auto it = ls::infix_ostream_iterator<string>(out, ", ");
    for (auto& s : selectors) {
        *it = s.to_string();
    }
    return out;
}

}}}
