#include "exception.hpp"

namespace ls {

parsing_error::parsing_error(long int pos)
    : std::runtime_error{"parsing error"}, error_position{pos} {}
}
