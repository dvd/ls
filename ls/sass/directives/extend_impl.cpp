#include <algorithm>
#include "extend_impl.hpp"

namespace ls { namespace sass { namespace directives { namespace details {
using std::vector;
using css3::Selector;
using css3::SimpleSelector;
using css3::Combinator;

boost::optional<ExtendImplementation::SelectorMatch>
    ExtendImplementation::find_selector(vector<Selector>& groups, SimpleSelector const& sel) {

    int index = 0;
    for (auto& g : groups) {
        auto result = css3::find_match(g, sel);
        if (result) {
            return SelectorMatch{g, *result, index};
        }
        index++;
    }
    return {};
}

Selector ExtendImplementation::extend_selector(
    Selector const& S1, SimpleSelector const& E1,
    Selector const& S2, SimpleSelector const& E2,
    SimpleSelector const& EX) {

    SimpleSelector Em = (E1 - EX) | E2;

    Selector Sd = S2;
    Sd.replace_selector(E2, Em);

    Selector Sf = S1;
    Sf.replace_selector(E1, Sd);

    return Sf;
}

vector<Selector> ExtendImplementation::extend(
    Selector const& base, Selector const& derived, SimpleSelector const& EX) {

    using v = vector<Selector>;

    v G1 = css3::split(base, Combinator::whitespace);
    v G2 = css3::split(derived, Combinator::whitespace);

    auto x = find_selector(G1, EX);
    if (!x) {
        throw std::runtime_error("extension not found");
    }
    Selector& S1m = x->selector;
    Selector& S2m = G2.back();

    SimpleSelector const& E1 = x->match;
    SimpleSelector const& E2 = S2m.selector(-1);

    Selector SX = extend_selector(S1m, E1, S2m, E2, EX);

    v G1r = G1;
    G1r[x->index] = SX;

    v G2r = G2;
    G2r.back() = SX;

    G1[x->index] = css3::join(G2r, Combinator::whitespace);
    G2.back() = css3::join(G1r, Combinator::whitespace);

    if (G1 == G2) {
        return {
            base,
            css3::join(G1, Combinator::whitespace)
        };
    }
    else {
        return {
            base,
            css3::join(G1, Combinator::whitespace),
            css3::join(G2, Combinator::whitespace)
        };
    }
}

}}}}
