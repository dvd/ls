#pragma once
#include "base.hpp"
#include <ls/sass/css3/simple_selector.hpp>

namespace ls { namespace sass {
    namespace compiler {
        class Feedback;
    }

    namespace directives {
        class Block;

        class Extend : public Directive {
            css3::SimpleSelector _selector;

            public:
                Extend();
                Extend(css3::SimpleSelector);

                css3::SimpleSelector const& selector() const;

                void apply(compiler::Feedback*);

            private:
                bool apply_block(Block&);
        };

    }
}};
