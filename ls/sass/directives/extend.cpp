#include "extend.hpp"
#include "extend_impl.hpp"
#include "block.hpp"
#include <ls/strings.hpp>
#include <ls/sass/compiler/feedback.hpp>

namespace ls { namespace sass { namespace directives {

Extend::Extend() : Extend(css3::SimpleSelector{}) {}

Extend::Extend(css3::SimpleSelector sel) : Directive{}, _selector{std::move(sel)} {
}

css3::SimpleSelector const& Extend::selector() const {
    return _selector;
}

void Extend::apply(compiler::Feedback* f) {
    auto tree = root();
    auto it = std::next(tree->begin<Block>());
    auto end = tree->end<Block>();

    bool found = false;
    for (; it != end; it++) {
        auto& block = *it;

        if (&block == parent()) {
            continue;
        }

        found |= apply_block(block);
    }

    if (!found) {
        f->warn(supplant(
            R"("%" failed to @extend "%".)",
            parent()->selectors(),
            _selector));
    }
}

bool Extend::apply_block(Block& block) {
    auto impl = details::ExtendImplementation();
    std::vector<css3::Selector> to_add;

    for (auto& block_selector : block.selectors()) {
        for (auto& parent_selector : parent()->selectors()) {
            std::vector<css3::Selector> e;
            try {
                e = impl.extend(block_selector, parent_selector, _selector);
            }
            catch(std::runtime_error&) {
                continue;
            }
            to_add.push_back(e[1]);
            if (e.size() == 3) {
                to_add.push_back(e[2]);
            }
        }
    }

    block.add_selectors(to_add);
    return to_add.size() > 0;
}

}}}
