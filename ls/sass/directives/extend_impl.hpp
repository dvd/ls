#pragma once
#include "../css3.hpp"
#include <vector>
#include <boost/optional.hpp>

/*
extend algorithm derived from the observation of the output of sass
===================================================================

S* represents a selector (ls::css3::Selector)

G* represents a vector of selectors  obtained splitting a selector on the white
spaces

E* represents a sub-selector (ls::css3::SimpleSelector)

GIVEN:
    S1 the base selector
    S2 the derived selector
    EX the extend sub-selector
    E2 the sub-selector of S2 that will be extended
    extend a function (see below):
        (
            Selector, SimpleSelector,
            Selector, SimpleSelector,
            SimpleSelector
        ) -> Selector

G1 <- obtained by splitting S1
G2 <- obtained by splitting S2

E1 <- the sub-selector of S1 that meets EX
E2 <- last sub-selector of S2

S1m <- selector of G1 that contains E1
S2m <- selector of G2 that contains E2

SX <- selector obtained by calling extend(S1m, E1, S2m, E2, EX)

G1r <- obtained by copying G1 but replacing S1m with SX
G2r <- obtained by copying G2 but replacing S2m with SX

G1f <- obtained by copying G1 but replacing S1m with G2r
G2f <- obtained by copying G2 but replacing S1m with G1r

S1f <- obtained by joining back G1f
S2f <- obtained by joining back G2f

if S1f == S2f
    RETURN S1, S1f
else
    RETURN S1, S1f, S2f

The extend function
-------------------

extend(S1, E1, S2, E2, EX) -> Sf

    E1 is a sub-selector of S1
    E2 is a sub-selector of S2
    EX is a sub-selector that match E1

    Em = (E1 - EX) | E2

    Sd <- obtained by copying S2 but replacing E2 with Em
    Sf <- obtained by copying S1 but replacing E1 with Sd

    RETURN Sf

Example 1
=========

.error { color: red; }
div > p { @extend: .error }

S1 = .error
S2 = div > p
EX = .error

G1 = [.error]
G2 = [div > p]
E1 = .error
E2 = p

S1m = .error
S2m = div > p

SX = div > p

G1r = [div > p]
G2r = [div > p]

G1f = [div > p]
G2f = [div > p]

S1f = div > p
S2f = div > p

RETURN .error, div > p

Example 2
=========

g h i ~ j.foo.bar q { color: red; }
k l + m { @extend j.foo; }

S1 = g h i ~ j.foo.bar q
S2 = k l + m
EX = j.foo

G1 = [g, h, i ~ j.foo.bar, q]
G2 = [k, l + m]
E1 = j.foo.bar
E2 = m

S1m = i ~ j.foo.bar
S2m = l + m

SX = i ~ l + m.bar

G1r = [g, h, i ~ l + m.bar, q]
G2r = [k, i ~ l + m.bar]

G1f = [g, h, k, i ~ l + m.bar, q]
G2f = [k, g, h, i ~ l + m.bar, q]

S1f = g h k i ~ l + m.bar q
S2f = k g h i ~ l + m.bar q

RETURN g h i~j.foo.bar q, g h k i~l+m.bar q, k g h i~l+m.bar q
*/
namespace ls { namespace sass { namespace directives { namespace details {
    class ExtendImplementation {
        public:
            struct SelectorMatch {
                css3::Selector& selector;
                css3::SimpleSelector const& match;
                int index;
            };

            boost::optional<SelectorMatch>
                find_selector(std::vector<css3::Selector>&, css3::SimpleSelector const&);

            css3::Selector extend_selector(
                css3::Selector const&, css3::SimpleSelector const&,
                css3::Selector const&, css3::SimpleSelector const&,
                css3::SimpleSelector const&);

            std::vector<css3::Selector>
                extend(css3::Selector const&, css3::Selector const&, css3::SimpleSelector const&);
    };
}}}}
