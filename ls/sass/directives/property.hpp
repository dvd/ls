#pragma once
#include <string>
#include "base.hpp"

namespace ls { namespace sass { namespace directives {
    class Block;

    class Property : public Directive {
        public:
            Property() = default;
            Property(std::string, std::string);

            std::string name{""};
            std::string value{""};
    };
}}}
