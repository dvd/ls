#pragma once

namespace ls { namespace sass { namespace directives {

    class Block;

    class Directive {
        friend class Block;
        Block* _parent{nullptr};

        public:
            Directive();
            virtual ~Directive();

            Block* parent() const;
            Block const* root() const;
            Block* root();

            Directive& operator=(Directive const&) = delete;
    };

}}}
