#include "property.hpp"

namespace ls { namespace sass { namespace directives {

using std::string;

Property::Property(string name, string value)
    : Directive{}, name{std::move(name)}, value{std::move(value)} {
}

}}}
