#pragma once
#include "block.hpp"

namespace ls { namespace sass { namespace directives {

    template <typename UserVisitor>
    class BlockTreeTraversal {
        UserVisitor& user_visitor;

        public:
            BlockTreeTraversal(UserVisitor& v) : user_visitor{v} {}

            template<typename P>
            bool operator()(P const& d) const {
                if (user_visitor.enter(d)) {
                    return true;
                }
                return user_visitor.leave(d);
            }

            bool operator()(Block const& d) const {
                auto sub = BlockTreeTraversal<UserVisitor>(user_visitor);
                return sub.run(d);
            }

            bool run(Block const& root) const {
                if (user_visitor.enter(root)) {
                    return true;
                }
                if (root.directives(*this)) {
                    return true;
                }
                return user_visitor.leave(root);
            }
    };

    // Helper class to write a visitor of a block tree.
    // Derive from TreeVisitor and reimplement enter() or levea() only for
    // the types you are interested in::
    //
    // struct W : struct TreeVisitor {
    //      using TreeVisitor::enter;
    //      bool enter(ls::Block const&) {
    //          return false;
    //      }
    // };
    //
    // to abort the traversal both enter() or leave() can return true.
    class TreeVisitor {
        public:
            template <typename T>
            bool enter(T const&) const {
                return false;
            }
            template <typename T>
            bool leave(T const&) const {
                return false;
            }
    };

    template<typename T>
    bool traverse_tree(Block const& block, T& visitor) {
        return BlockTreeTraversal<T>(visitor).run(block);
    }
}}}
