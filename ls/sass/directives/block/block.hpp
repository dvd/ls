#pragma once
#include <memory>
#include <vector>
#include <ls/sass/css3/selector.hpp>
#include "../base.hpp"
#include "../extend.hpp"
#include "../property.hpp"

namespace ls { namespace sass { namespace directives {
    /*
     * Block
     * -----
     * a, p {
     *  color: red;
     * }
     *
     * a Block is defined by:
     *  - a pointer to a parent Block
     *      A parent block is needed by numerous features of SCSS, from
     *      variables scope to the parent selector (&)
     *  - one or more selectors
     *  - a collection of directives
     *
     * A directive can be a number of things most notably a unique pointer to
     * another block
     */
    template <typename T>
    class Iterator;

    class Block : public Directive {
        private:
            template <typename T>
            friend class Iterator;

            std::vector<css3::Selector> _selectors;
            std::vector<std::unique_ptr<Directive>> _directives;

            void _move(Directive&);
        public:
            Block();
            Block(std::vector<css3::Selector>);

            std::vector<css3::Selector> const& selectors() const;

            void add_selector(css3::Selector);
            void add_selectors(std::vector<css3::Selector>);
            void replace_selectors(std::vector<css3::Selector>);

            /*
             * Computes the selectors of this block up to parent (the root if
             * parent=null).
             *
             * If parent is not found an exception is raised.
             */
            std::vector<css3::Selector> computed_selectors(Block* parent=nullptr) const;

            /*
             * Add the the directive to the Block.
             *
             * If the directive already has a parent an exception is thrown.
             *
             * To move a directive between parents use the `move` method
             */
            void push_back(std::unique_ptr<Directive>);

            // shortcut to build and append a new directive
            template <typename T, typename... Args>
            T* emplace_back(Args&&... args) {
                T* raw = new T{std::forward<Args>(args)...};
                push_back(std::unique_ptr<Directive>{raw});
                return raw;
            }

            /*
             * Remove a directive from this block.
             *
             * If the directive is not found on this Block an exception is
             * thrown.
             *
             * The parent of the directive is set to nullptr before returning.
             *
             * An unique_ptr to the removed directive is returned.
             */
            std::unique_ptr<Directive> remove(Directive&);

            /*
             * Move a directive between blocks.
             *
             * The directive must be assigned to another Block otherwise an
             * exception is thrown.
             *
             * This Block become the parent of the directive and starts to
             * manage its lifetime.
             */
            void move(Directive&);

            /*
             * Replace the first directive with the second
             *
             * If the first directive is not found on this Block an exception
             * is thrown.
             *
             * The second directive is moved into this block.
             *
             * An unique_ptr to the first directive is returned.
             */
            std::unique_ptr<Directive> replace(Directive&, Directive&);

            template <typename Visitor>
            bool directives(Visitor const&) const;

            template <typename T>
            std::vector<T*> directives() const;

            /*
             * Returns an iterator that starts from this block and descends the tree
             * returning only the directives of a certain type
             */
            template <typename T>
            Iterator<T> begin() {
                return Iterator<T>{this};
            }

            template <typename T>
            Iterator<T> end() {
                return Iterator<T>{nullptr};
            }
    };

    template <typename Visitor>
    bool Block::directives(Visitor const& v) const {
        for(auto& d : _directives) {
            Directive* base = d.get();
            if (auto s = dynamic_cast<Extend*>(base)) {
                if (v(*s)) {
                    return true;
                }
            }
            else if (auto s = dynamic_cast<Property*>(base)) {
                if (v(*s)) {
                    return true;
                }
            }
            else if (auto s = dynamic_cast<Block*>(base)) {
                if (v(*s)) {
                    return true;
                }
            }
        }
        return false;
    }

    template <typename T>
    std::vector<T*> Block::directives() const {
        std::vector<T*> output{};
        for(auto& d : _directives) {
            T* p = dynamic_cast<T*>(d.get());
            if (p) {
                output.push_back(p);
            }
        }
        return output;
    }

    /*
     * Checks if one of the selectors of the block "matches" the passed
     * selector.
     */
    bool operator&&(Block const&, css3::Selector const&);

    /*
     * transplant a block, and all of its children, as a child of the root It's
     * ok to transplant a block that already is a direct child of the root.
     */
    void transplant(Block&);
}}}
