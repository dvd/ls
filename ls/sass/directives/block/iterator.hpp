#pragma once
#include "../base.hpp"
#include "block.hpp"
#include <iterator>
#include <tuple>
#include <vector>

namespace ls { namespace sass { namespace directives {
    /*
     * An iterator that traverse a Block tree returning only the directives of
     * the specified type.
     *
     * Iterator<Block> it{root} // returns only the Block found in the tree
     * Iterator<Extend> it{root} // only the Extend
     */
    template <typename T>
    class Iterator : public std::iterator<std::forward_iterator_tag, T> {
        using StackEntry = std::tuple<Block*, size_t>;

        Block* root;
        T* value;
        std::vector<StackEntry> stack;

        Iterator<T>& advance() {
            T* value = nullptr;
            while (stack.size() && value == nullptr) {
                size_t stack_index = stack.size() - 1;

                StackEntry& tail = stack.back();
                Block* node = std::get<0>(tail);
                size_t index = std::get<1>(tail);

                for (index++; index < node->_directives.size(); index++) {
                    Directive* base = node->_directives[index].get();
                    if (auto s = dynamic_cast<Block*>(base)) {
                        stack.push_back(StackEntry{s, -1});
                        if (std::is_same<T, Block>::value) {
                            value = reinterpret_cast<T*>(s);
                        }
                        break;
                    }
                    else if(auto s = dynamic_cast<T*>(base)) {
                        value = s;
                        break;
                    }
                }

                if (index == node->_directives.size()) {
                    stack.erase(stack.begin() + stack_index);
                }
                else {
                    std::get<1>(stack[stack_index]) = index;
                }
            }
            this->value = value;
            return *this;
        }

        public:
            Iterator(Block* root) : root{root}, value{nullptr} {
                if (root != nullptr) {
                    stack.push_back(StackEntry{root, -1});
                    if (std::is_same<T*, Block*>::value) {
                        value = reinterpret_cast<T*>(root);
                    }
                    else {
                        advance();
                    }
                }
            }

            bool operator==(Iterator<T> const& other) {
                return value == other.value;
            }

            bool operator!=(Iterator<T> const& other) {
                return !(*this == other);
            }

            Iterator<T>& operator++() {
                return advance();
            }

            Iterator<T> operator++(int) {
                Iterator<T> tmp(*this);
                advance();
                return tmp;
            }

            T& operator*() {
                return *value;
            }

            T* operator->() {
                return &(*value);
            }
    };

}}}
