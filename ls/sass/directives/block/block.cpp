#include "block.hpp"
#include <algorithm>
#include <ls/sass/exception.hpp>

namespace ls { namespace sass { namespace directives {
using std::vector;

Block::Block()
    : Block{vector<css3::Selector>{}} {}

Block::Block(vector<css3::Selector> sel)
    : Directive{}, _selectors{std::move(sel)} {}

vector<css3::Selector> const& Block::selectors() const {
    return _selectors;
}

void Block::add_selector(css3::Selector s) {
    _selectors.push_back(std::move(s));
}

void Block::add_selectors(vector<css3::Selector> selectors) {
    for (auto& s : selectors) {
        _selectors.push_back(std::move(s));
    }
}

void Block::replace_selectors(vector<css3::Selector> selectors) {
    _selectors = std::move(selectors);
}

void Block::push_back(std::unique_ptr<Directive> p) {
    if (p->parent()) {
        throw ls::owner_error{"directive already bound"};
    }
    p->_parent = this;
    _directives.push_back(std::move(p));
}

std::unique_ptr<Directive> Block::remove(Directive& p) {
    Block* parent = p.parent();
    if (parent != this) {
        throw ls::owner_error{"parent mismatch"};
    }

    auto found = std::find_if(
        _directives.begin(),
        _directives.end(),
        [&p](std::unique_ptr<Directive> const& d) -> bool {
            return d.get() == &p;
        });

    if (found == _directives.end()) {
        throw ls::owner_error{"directive address not found"};
    }
    auto alive = std::move(*found);
    _directives.erase(found);
    alive->_parent = nullptr;
    return std::move(alive);
}

void Block::_move(Directive& p) {
    Block* parent = p.parent();
    std::unique_ptr<Directive> alive;
    if (parent) {
        alive = parent->remove(p);
    }
    else {
        alive.reset(&p);
    }
    push_back(std::move(alive));
}

void Block::move(Directive& p) {
    Block* parent = p.parent();
    if (parent == nullptr) {
        throw ls::owner_error{"cannot move an unbound directive"};
    }
    if (parent == this) {
        throw ls::owner_error{"source and destination parents overlap"};
    }
    _move(p);
}

std::unique_ptr<Directive> Block::replace(Directive& to_remove, Directive& to_insert) {
    if (to_insert.parent() == this) {
        throw ls::owner_error{"source and destination parents overlap"};
    }
    auto alive = remove(to_remove);
    _move(to_insert);
    return std::move(alive);
}

vector<css3::Selector> Block::computed_selectors(Block* parent) const {
    vector<css3::Selector> output;

    Block const* node = this;
    bool found = node == parent;
    while (node && !found) {
        output = css3::combine(node->selectors(), output, css3::Combinator::whitespace);
        node = node->parent();
        found = node == parent;
    }
    if (!found) {
        throw ls::owner_error{"parent not found"};
    }
    return output;
}

bool operator&&(Block const& b, css3::Selector const& s) {
    for(auto& selector : b.selectors()) {
        if (selector && s) {
            return true;
        }
    }
    return false;
}

void transplant(Block& block) {
    if (block.parent() == nullptr) {
        throw ls::owner_error{"the root cannot be transplanted"};
    }
    Block* root = block.root();
    if (block.parent() == root) {
        return;
    }
    block.replace_selectors(block.computed_selectors());
    root->push_back(
        block.parent()->remove(block));
}

}}}
