#include "base.hpp"
#include <ls/sass/directives/block/block.hpp>
#include <ls/sass/exception.hpp>

namespace ls { namespace sass{ namespace directives {

Directive::Directive() {}

Directive::~Directive() {}

Block* Directive::parent() const {
    return _parent;
}

Block const* Directive::root() const {
    if (_parent == nullptr) {
        auto output = dynamic_cast<Block const*>(this);
        if (!output) {
            throw ls::owner_error{"out of three directive"};
        }
        return output;
    }
    Block const* r = parent();
    while (r->parent()) {
        r = r->parent();
    }
    return r;
}

Block* Directive::root() {
    return const_cast<Block*>(
        static_cast<Directive const&>(*this).root());
}

}}}
