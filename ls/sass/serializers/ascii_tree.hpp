#pragma once
#include <string>
#include <ls/sass/directives.hpp>
#include <ls/strings.hpp>

namespace ls { namespace sass { namespace serializers {
    template <typename T>
    class AsciiTree : public directives::TreeVisitor {
        T& stream;
        std::string indent = "";

        template <typename U>
        T& write(U o) {
            stream << indent << o << std::endl;
            return stream;
        }

        public:
            AsciiTree(T& stream) : stream{stream} {};

            void dump(directives::Block const& block) {
                directives::traverse_tree(block, *this);
            }

        public:
            using directives::TreeVisitor::enter;
            using directives::TreeVisitor::leave;

            bool enter(directives::Block const& x) {
                write(supplant("+ block [%]", x.selectors()));
                indent.append("  ");
                return false;
            }
            bool enter(directives::Extend const& x) {
                write(supplant("- extend {%}", x.selector()));
                return false;
            }
            bool enter(directives::Property const& x) {
                write(supplant("- property {%}", x.name));
                return false;
            }
            bool leave(directives::Block const&) {
                indent.resize(indent.size() - 2);
                return false;
            }
    };
}}}
