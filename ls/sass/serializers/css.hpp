#pragma once
#include <ls/sass/directives.hpp>

namespace ls { namespace sass { namespace serializers {
    template <typename T>
    class CSS : public directives::TreeVisitor {
        T& stream;
        directives::Block const* root;

        public:
            CSS(T& stream) : stream{stream} {};

            void dump(directives::Block const& r) {
                root = &r;
                directives::traverse_tree(r, *this);
                root = nullptr;
            }

        public:
            using directives::TreeVisitor::enter;
            using directives::TreeVisitor::leave;

            bool enter(directives::Block const& x) {
                if (&x == root) {
                    return false;
                }
                if (x.parent() != root) {
                    throw ls::owner_error{"nested blocks are not supported"};
                }
                stream << x.selectors() << " {" << std::endl;
                return false;
            }
            bool enter(directives::Property const& x) {
                stream << "  " << x.name << ':' << x.value << ';' << std::endl;
                return false;
            }
            bool leave(directives::Block const& x) {
                if (&x != root) {
                    stream << "}" << std::endl;
                }
                return false;
            }
    };
}}}
