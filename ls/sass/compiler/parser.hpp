#pragma once
#include <istream>
#include <ls/sass_script/compiler/streams.hpp>
#include <ls/sass/grammars/scss.hpp>
#include <ls/sass/exception.hpp>

namespace ls { namespace sass {
    namespace directives {
        class Block;
    }

    namespace compiler {

    template <typename Parser, typename  Iterator>
    void parse(Parser& parser, directives::Block *root, Iterator start, Iterator end) {
        namespace qi = boost::spirit::qi;
        Iterator it{start};

        if (!qi::phrase_parse(it, end, parser(root), qi::space) || it != end) {
            throw ls::parsing_error{std::distance(start, it)};
        }
    }

    template <typename  Iterator>
    void parse(directives::Block *root, Iterator begin, Iterator end) {
        grammars::SCSS<Iterator> parser;
        parse(parser, root, begin, end);
    }

    void parse(directives::Block*, std::string const&);
    void parse(directives::Block*, std::istream&);
    void parse(directives::Block*, sass_script::compiler::CharStream);

    }
}}
