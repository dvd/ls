#pragma once
#include <string>
#include <ostream>

namespace ls { namespace sass { namespace compiler {
    enum class FeedbackLevel {
        info, warn, error
    };

    class Feedback {
        protected:
            virtual void write(FeedbackLevel, std::string const&) = 0;
        public:
            virtual ~Feedback();

            void info(std::string const&);
            void warn(std::string const&);
            void error(std::string const&);
    };

    template <typename T>
    class StreamFeedback : public Feedback {
        T& stream;

        protected:
            void write(FeedbackLevel, std::string const& msg) override {
                stream << msg << std::endl;
            }

        public:
            StreamFeedback(T& stream) : Feedback{}, stream{stream} {}
    };

}}}
