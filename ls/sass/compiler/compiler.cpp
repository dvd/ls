#include "compiler.hpp"
#include "feedback.hpp"
#include <ls/sass/directives.hpp>
#include <algorithm>
#include <ls/strings.hpp>

namespace ls { namespace sass { namespace compiler {

using directives::Block;

Compiler::Compiler(Feedback* f) : feedback{f} {}

void Compiler::compile(Block& root) {
    feedback->info("start compiling");

    process_extend(root);
    flatten_tree(root);
}

void Compiler::process_extend(Block& root) const {
    using directives::Extend;

    auto it = root.begin<Extend>();
    auto end = root.end<Extend>();
    int count = 0;
    for (; it != end; it++, count++) {
        it->apply(feedback);
    }

    feedback->info(supplant("% extends processed", count));
}

void Compiler::flatten_tree(Block& root) const {
    auto it = std::next(root.begin<Block>());
    auto end = root.end<Block>();

    std::vector<Block*> collected;
    int count = 0;
    for (; it != end; it++, count++) {
        if (it->parent() != &root) {
            collected.push_back(&(*it));
        }
    }
    for (auto p : collected) {
        transplant(*p);
    }
    feedback->info(supplant("% blocks transplanted out of %", collected.size(), count));
}

}}}
