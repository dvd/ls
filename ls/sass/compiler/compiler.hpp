#pragma once
namespace ls { namespace sass {
    namespace directives {
        class Block;
    }
    namespace compiler {
        class Feedback;

        class Compiler {
            Feedback* feedback;

            void process_extend(directives::Block&) const;
            void flatten_tree(directives::Block&) const;

            public:
                Compiler(Feedback* f);
                void compile(directives::Block&);
        };
    }
}}
