#include "parser.hpp"
#include <string>
#include <ls/sass/exception.hpp>
#include <ls/sass/grammars/scss.hpp>
#include <ls/sass/css3/selector.hpp>
#include <boost/spirit/include/support_istream_iterator.hpp>

namespace ls { namespace sass {
    namespace css3 {

    using std::string;

    SimpleSelector SimpleSelector::fromString(string const& input) {
        namespace qi = boost::spirit::qi;
        grammars::SimpleSelector<string::const_iterator> parser;

        SimpleSelector result;

        auto start = std::begin(input);
        auto end = std::end(input);
        if (!qi::phrase_parse(start, end, parser, qi::space, result) || start != end) {
            throw ls::parsing_error(std::distance(std::begin(input), start));
        }
        return result;
    }

    Selector Selector::fromString(string const& input) {
        namespace qi = boost::spirit::qi;
        grammars::Selector<string::const_iterator> parser;

        Selector result;

        auto start = std::begin(input);
        auto end = std::end(input);
        if (!qi::phrase_parse(start, end, parser, qi::space, result) || start != end) {
            throw ls::parsing_error(std::distance(std::begin(input), start));
        }
        return result;
    }

    }

    namespace compiler {

    using directives::Block;
    using grammars::SCSS;
    using sass_script::compiler::CharStream;
    using sass_script::compiler::CharStreamMultipass;

    void parse(Block *root, std::string const& input) {
        SCSS<std::string::const_iterator> parser;
        parse(parser, root, std::begin(input), std::end(input));
    }

    void parse(Block *root, std::istream& input) {
        using namespace boost::spirit;
        SCSS<basic_istream_iterator<char>> parser;
        parse(parser, root, istream_iterator{input}, istream_iterator{});
    }

    void parse(Block *root, CharStream input) {
        SCSS<CharStreamMultipass> parser;
        parse(parser, root, CharStreamMultipass{input}, CharStreamMultipass{});
    }

    }
}}
