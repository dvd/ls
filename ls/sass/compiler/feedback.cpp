#include "feedback.hpp"

namespace ls { namespace sass { namespace compiler {

using std::string;

Feedback::~Feedback() {}

void Feedback::info(string const& msg) {
    write(FeedbackLevel::info, msg);
}

void Feedback::warn(string const& msg) {
    write(FeedbackLevel::warn, msg);
}

void Feedback::error(string const& msg) {
    write(FeedbackLevel::error, msg);
}

}}}
