#include "types.hpp"
#include <cxxabi.h>
#include <memory>

namespace ls {

std::string demangle(char const* mangled) {
    int status;
    std::unique_ptr<char[], void (*)(void*)> result{
        abi::__cxa_demangle(mangled, 0, 0, &status), std::free};
    return result ? std::move(std::string(result.get())) : "";
}

}
