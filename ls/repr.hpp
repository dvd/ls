#pragma once
#include <string>

namespace ls {
    class Repr {
        public:
            virtual ~Repr() = default;
            virtual std::string repr() const = 0;
    };

    template <typename T, typename enable=std::enable_if_t<not std::is_pointer<T>::value>>
    std::string repr(T const& x) {
        return std::move(x.repr());
    }

    template <typename T, typename enable=std::enable_if_t<std::is_pointer<T>::value>>
    std::string repr(T x) {
        return std::move(x->repr());
    }
}
