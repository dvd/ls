#pragma once

namespace ls {
    template <typename Container, typename ConstIterator>
    typename Container::iterator remove_constness(Container& c, ConstIterator it) {
        // https://twitter.com/_JonKalb/status/202815932089896960
        return c.erase(it, it);
    }
}

