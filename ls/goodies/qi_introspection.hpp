#pragma once
#include <cxxabi.h>
#include <stdlib.h>
#include <string>
#include <iostream>

namespace ls {

// wizardy from http://stackoverflow.com/questions/9404189/detecting-the-parameter-types-in-a-spirit-semantic-action
template <typename T> std::string nameofType(const T& v) {
    int status;
    char *realname = abi::__cxa_demangle(typeid(v).name(), 0, 0, &status);
    std::string name(realname ? realname : "????");
    free(realname);
    return name;
}

struct what_is_the_attr {
    template <typename> struct result { typedef bool type; };

    template <typename T> bool operator()(T& attr) const {
        std::cerr << "what_is_the_attr: " << nameofType(attr) << std::endl;
        return true;
    }
};

struct what_are_the_arguments {
    template <typename...> struct result { typedef bool type; };

    template <typename... T> bool operator()(const T&... attr) const {
        std::vector<std::string> names { nameofType(attr)... };
        std::cerr << "what_are_the_arguments:\n\t";
        std::copy(names.begin(), names.end(), std::ostream_iterator<std::string>(std::cerr, "\n\t"));
        std::cerr << '\n';
        return true;
    }
};
}
