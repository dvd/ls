#include <iostream>
#include <ls/sass_script/sass_script.hpp>
#include <ls/sass/sass.hpp>

int main() {
    using namespace std;
    namespace script = ls::sass_script;
    namespace sass = ls::sass;

    cin.unsetf(std::ios::skipws);

    cout << "== SASS SCRIPT ==" << endl;

    script::data::Scope root_scope;
    auto context = script::compiler::default_context();
    auto program = script::compiler::compile(context, cin);

    {
        cout << "\n+ In memory Tree" << endl;
        script::serializers::AsciiTree<decltype(cout)> d{cout};
        d.dump(*program);

        cout << "\n+ Intermediate output" << endl;

        auto runner = program->runner(root_scope);
        while (auto s = runner->step()) {
            cout << *s;
        }

        cout << endl;
    }

    cout << "\n== SASS ==" << endl;
    {
        auto runner = program->runner(root_scope);
        auto root_block = unique_ptr<sass::directives::Block>{new sass::directives::Block{}};

        sass::compiler::parse(root_block.get(), script::compiler::CharStream{runner.get()});

        cout << "\n+ In memory Tree" << endl;
        sass::serializers::AsciiTree<decltype(cout)> d{cout};
        d.dump(*root_block);

        cout << "\n+ Compiler output" << endl;
        sass::compiler::StreamFeedback<ostream> f{cout};
        sass::compiler::Compiler c{&f};
        c.compile(*root_block);

        cout << "\n+ Final output" << endl;
        sass::serializers::CSS<ostream> a{cout};
        a.dump(*root_block);
    }
}

