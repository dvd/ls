#include <iostream>
#include "ls/css3/selector.hpp"
#include "ls/css3/grammar.hpp"
#include <boost/version.hpp>
#include "utils.hpp"

int main() {
    using namespace std;
    cout << BOOST_VERSION << endl;
    using namespace ls::css3;

    auto sel = Selector{SimpleSelector{"h1"}};
    sel.push_back(Combinator::tilde, SimpleSelector{"p"});
    /*
    auto x = SimpleSelector{Type{"h1"}, {Class{".foo"}, Attribute{"[name]"}}};
    cout << x << endl;

    auto y = Selector{Type{"h1"}};
    auto z = Combinator::tilde;
    */
    cout << sel << endl;

    cin.unsetf(std::ios::skipws);

    using Grammar = ls::css3::grammars::Selector<boost::spirit::basic_istream_iterator<char>>;
    Grammar g;
    ls::css3::Selector result{};
    test_phrase_parser_attr(cin, g, result);
    cout << "r -> {" << result << "}" << endl;
}
